<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- mobile metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="viewport" content="initial-scale=1, maximum-scale=1">
      <!-- site metas -->
      <title>Lienzanier</title>
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- bootstrap css -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
      <!-- style css -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style_theme.css">
      <!-- Responsive-->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/responsive.css">
      <!-- fevicon -->
      <link rel="icon" href="${pageContext.request.contextPath}/resources/img/fevicon.png" type="image/gif" />
      <!-- Scrollbar Custom CSS -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery.mCustomScrollbar.min.css">
      
      
      <style>
      
      	.card-container {
			background-color: #fafafa;
			border-radius: 5px;
			box-shadow: 0px 10px 20px -10px rgba(0,0,0,0.75);
			color: #B3B8CD;
			padding-top: 30px;
			position: relative;
			width: 350px;
			max-width: 100%;
			text-align: center;
			height: 600px;
		}
		
		
		.card-container .round {
			border: 1px solid #03BFCB;
			border-radius: 50%;
			padding: 7px;
		}
		
		button.primary {
			background-color: #03BFCB;
			border: 1px solid #03BFCB;
			border-radius: 3px;
			color: #231E39;
			font-family: Montserrat, sans-serif;
			font-weight: 500;
			padding: 10px 25px;
		}
		
		button.primary.ghost {
			background-color: transparent;
			color: #02899C;
		}
		
		
		h3 {
			margin: 10px 0 !important;
			font-family: 'Jost', sans-serif !important;;
		    font-size: 1.8rem !important;;
		    line-height: 1.5 !important;;
		}
		
		h6 {
			margin: 5px 0;
			text-transform: uppercase;
		}
		
		h1 {
			font-family: 'Jost', sans-serif !important;
		    font-size: 4.6rem !important;
		    line-height: 1.1 !important;
		    margin: auto !important;
   			 width: fit-content !important;
   			 text-align: center !important;
		}
		
		p {
			font-size: 14px;
			line-height: 21px;
		}
		
		.container-img{
			margin: 0 340px;
		}
		
		.contenido{
			font-size: 24px;
    		line-height: 1.6;
    		font-family: open-sans, Open Sans, Helvetica Neue, Helvetica, Arial, sans-serif;
/*     		color: #555; */
		}
		
		.div-img{
			margin: 25px 0;
			text-align: center;
		}
		
		.underline-bottom-brand-red {
		    padding-bottom: .5rem!important;
		    border-bottom: .3rem solid #ff352a;
		   
		}
		
		ol{
			color: #000;
		}
		
		.contenido ol{
			color: #212222;
		}
      
      </style>
      
      
</head>
 <body >
     
     <header>
		<jsp:include page='header-inicio.jsp'/>
	</header>
	
	
	<div>
		<h1>KPIs escenciales para tu Startup</h1>
	
	</div>
	
	<article id="top">
		<div class="row" style="margin-left: 30%;">
	
			<div class="card-container">
				<img class="round" src="${pageContext.request.contextPath}/resources/img/foco_blog.jpg" alt="user" />
				<h3><strong>¿De que se trata?</strong></h3>
				<p>
					Los KPI’s son los indicadores claves de desempeño, es un valor cuantificable que muestra qué tan efectivamente tu empresa está alcanzando sus 
					objetivos de negocio, además reflejan el comportamiento futuro sobre el progreso de tu startup y ayudan a analizar cómo han evolucionado tus ventas; 
					es decir, te muestran el adecuado camino para ir logrando tus objetivos.
				</p>	
				<div class="buttons">
					<a  href="#definicion">
						<button class="primary ghost">
							Ver más
						</button>
					</a>
					
				</div>				
			</div>
			
			<div class="card-container">
				<img style="height:172px" class="round" src="${pageContext.request.contextPath}/resources/img/pasos_blog.png" alt="user" />
				<h3><strong>Importancia</strong></h3>
				<p>
					Los KPIS son de vital importancia para una Startup porque ofrecen una rigurosa visión analítica tanto del estado de la empresa como de sus 
					posibilidades reales. Tiene utilidad tanto para el medir cómo ha funcionado una determinada acción realizada, como para anticiparse a averiguar 
					una línea de progreso que nos lleve a nuestra meta/ objetivo empresarial.

				</p>
				<div class="buttons">	
					<a href="#usar">
						<button  class="primary ghost">
							Ver más
						</button>
					</a>			
					
				</div>
				
			</div>		
			
		
		</div>
	</article>
	
	<div class="container-img">
		<div class="div-img">
			<img src="${pageContext.request.contextPath}/resources/img/kpi.jpg" alt="#" />
		</div>
		
		<section>
			<p class="contenido" id="definicion">
				<strong>Los KPIS son métricas cuantificables y medibles que identifican el rendimiento en una empresa</strong> por medio de la medición de determinadas variables, 
				como puede ser el número de visitas, los ingresos o los gastos, entre otras. A la hora de establecer las variables a cuantificar (KPIS) estas deben 
				estar directamente <strong>relacionadas con los objetivos previamente marcados en la estrategia de la Startup</strong> para determinar los resultados de la misma.

				Por tanto, en este momento de tanta competitividad, sobre todo entre Startups (empresas de nueva creación o en pañales) <strong>resulta crucial fijar y, sobre 
				todo, hacer un exhaustivo seguimiento de los KPIS</strong> esenciales. Y serán esos KPIS los que permitan no solo la supervivencia de nuestra recién nacida 
				Startup, sino que con el tiempo serán cauce para el crecimiento y la expansión.
			
			</p>
			<br><br><br>
			<h3 class="large underline-bottom-brand-red space-line-bottom">KPIs indispensables para tu startup!</h3>
			
			<p class="contenido" id="usar">
				<ol>
					<li>
						<strong>Average Order Value</strong>
						<p>
							Se refiere al valor medio del carrito de compra de los clientes de una tienda online. Es decir, informa acerca de la 
							cantidad de dinero que gastan los usuarios como promedio en nuestro ecommerce.
						</p>
						
						
						<br>
					</li>
					
					<li>
						<strong>Customer Acquisition Cost (CAC)</strong>
						<p>
							Se refiere a la <strong>inversión que se realiza para obtener un nuevo cliente</strong>. Se trata de una métrica unitaria que se basa en la media por 
							cada nuevo cliente, ayudando a realizar extrapolaciones que nos servirán para comprender mejor el funcionamiento del negocio.
        					Para calcular el monto invertido se debe <strong>considerar únicamente aquellos involucrados directamente en la adquisición de clientes</strong>, 
        					generalmente las áreas de Marketing y Ventas. No se considera costos de administración, SAC o desarrollo del producto.
        					De igual forma, para calcular el número de clientes solo considerar aquellos que hayan sido por la estrategia tomada. Por ejemplo, 
        					si en un blog mencionan a tu producto y obtienes algún cliente por este medio, no se debería considerar porque no invertiste ningún 
        					esfuerzo o dinero en ello.
						</p>
						<br>
					</li>
					
					<li>
						<strong>LifeTime Value</strong>
						<p>
							Es el término que se utiliza para determinar el valor que un cliente aporta a un negocio durante toda la vida útil de la empresa.
        					Es el valor neto de los ingresos que nos genera un cliente durante el tiempo que es nuestro cliente.

        					El CAC debe ser menor que el LTV, de lo contrario se estaría inviertiendo más en captar un cliente que en valor que este tiene una vez captado
						</p>
						<br>
					</li>
					
					<li>
						<strong>Tasa de retención</strong>
						<p>
							La tasa de retención de clientes es el índice que mide la fidelidad de los clientes hacia un negocio durante un plazo de tiempo concreto, expresado en un porcentaje.
					        Es decir, que indica la <strong>capacidad de un negocio para retener a sus clientes</strong> durante un tiempo determinado.
					
					        Si la tasa de retención es alta: los clientes son fieles y compran repetidamente en el mismo e-commerce.
					        Si la tasa es baja: suelen entrar clientes nuevos, pero muy pocos repiten compra.
						</p>
						<br>
					</li>
					
					<li>
						<strong>Churn Rate</strong>
						<p>
							Es la tasa de cancelación o pérdida de usuarios durante un período determinado de tiempo. Se refiere a la <strong>cantidad de clientes o 
							suscriptores que un negocio o base de datos deja de tener durante el período de tiempo que se quiera estudiar</strong>, y se expresa en porcentaje.

        					El churn rate es una métrica que a cualquier negocio basado en la suscripción (por ejemplo, una compañía de seguros o la mencionada 
        					herramienta online mediante suscripción) o con bases de datos de clientes o listas de correo (por ejemplo, aquellas a las que enviamos 
        					nuestras campañas de email marketing) le conviene controlar, porque indica el porcentaje de clientes que han perdido el interés en aquello 
        					que les ofrecemos, ya sea un producto, un servicio o un contenido.
						</p>
						<br>
					</li>
					
					
					
				</ol>
			
			</p>
			
		
		
		</section>
		
		<br><br>
		
         <div class="row margin_top_30">
            <div class="col-md-12">
               <div class="button_section full center margin_top_30">
                  <a style="margin:0;" href="#top">Subir</a>
               </div>
            </div>
         </div>
         <br><br>
	</div>
	
	

     
     
     
      <!-- end footer -->
      <!-- Javascript files-->
      <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/plugin.js"></script>
      <!-- Scrollbar Js Files -->
      <script src="${pageContext.request.contextPath}/resources/js/jquery.mCustomScrollbar.concat.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/custom.js"></script>
      
     
   </body>
</html>