<%@ taglib prefix="c" uri="http://www.hdiv.org/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Lienzanier</title>
	<link href='http://fonts.googleapis.com/css?family=Headland+One' rel='stylesheet' type='text/css'>
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/canvas.css">
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/menu.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/sticky.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">    
    <link href="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.css" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/deltoss/d3-mitch-tree@1.0.2/dist/css/d3-mitch-tree.min.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/d3-mitch-tree-default.min.css">
    
    <style>
    
    	p > a{
    		color: white;
   			font-size: 0.8rem;
    	}
    	
    	.hipo{
    		width: 100px !important;
		    height: auto !important;
		    border-radius: 15px;
		    padding: 3px;
    	}
    	
    	.speech-bubble {
			position: absolute;
			background: #8d89ff;
			border-radius: .4em;
			width: 150px;
			height: 80px;
			left: 666px;
			top: 150px;
			z-index: 10;
/* 			opacity: 1 !important; */
		}
		
		.speech-bubble:after {
			content: '';
			position: absolute;
			bottom: 0;
			left: 50%;
			width: 0;
			height: 0;
			border: 53px solid transparent;
			border-top-color: #8d89ff;
			border-bottom: 0;
			border-left: 0;
			margin-left: -26.5px;
			margin-bottom: -53px;
/* 			opacity: 1 !important; */
		}
		
		.close{
			float: right;
    		margin-right: 5px;
		}
		
		#main2{
/* 			opacity: .6;		 */
		}
		
		p.hipo > a{
			font-weight: bold;
			text-decoration: none;
		}
		
		p.hipo > a:hover{
			font-weight: bold;
			color: white;
			font-size: 0.9rem;
		}

		.modal-header{
			color: #fff;
			font-size: 3em;
		}
		
		img:not(.head){
			width: 80px;
			height: auto;
		}
		
		figure{
			text-align: center;
		}
		
		figcaption{
			font-size: 23px;
		}
		
		figcaption p{
			color: #8b4513;
		}
		
		.list-group-item{
			background-color: #ece9d6 !important;
		}
		
		.list-group-item.active{
			background-color: #6db9c6 !important;
    		border-color: #6db9c6 !important;
		}
		
		figure.arbol{
			margin-right: 65px;
		}
		
		
		.search {
		  width: 100%;
		  position: relative;
		  display: flex;
		}
		
		.searchTerm {
		  width: 100%;
		  border: 3px solid #00B4CC;
		  border-right: none;
		  padding: 5px;
		  height: 20px;
		  border-radius: 5px 0 0 5px;
		  outline: none;
		  color: #9DBFAF;
		}
		
		.searchTerm:focus{
		  color: #00B4CC;
		}
		
		.searchButton {
		  width: 40px;
		  height: 36px;
		  border: 1px solid #00B4CC;
		  background: #00B4CC;
		  text-align: center;
		  color: #fff;
		  border-radius: 0 5px 5px 0;
		  cursor: pointer;
		  font-size: 20px;
		}
		
		strong{
			font-weight: bold;
		}

.float{
	position:fixed;
	width:150px;
	height:80px;
	bottom:10px;
	right:40px;
	background-color:#0C9;
	color:#FFF;
	border-radius:50px;
	text-align:center;
	box-shadow: 2px 2px 3px #999;
}

.my-float{
	margin-top:22px;
}

.comentario{
	max-width: 1000px !important;
    max-height: 800px !important;
    height: 800px !important;
}

.scrollbar-comentario button{
	    height: fit-content;
    margin-left: 20px;
}

.scrollbar-comentario .item{
	    display: block;
}
	.scrollbar-comentario a{
    margin-left: 20px;
}

.btn-filtro.active{
	background-color: #597D7B;
}

.modal-content.arbol {
    width: 900px;
    padding: 20px;
}


#contentArbol.scrollbar {
	margin-left: 0px;
    height: 800px;
    width: 100%;
    background: #FFF;
    overflow-y: scroll;
    margin-bottom: 25px;
}
    </style>
    
</head>
<body>

	<header>
		<jsp:include page='header.jsp'/>
	</header>
	
	<div id="main2">
	<c:out value="${globillo}" escapeXml="false"/>
		<div class="options-home">
			<div>
	
					
					<figure onClick="open_modal_newHipotesis()">
						<img src="${pageContext.request.contextPath}/resources/img/bombilla.png">
						<figcaption><p>¿Que piensas?</p></figcaption>
					</figure>

			</div>
			<div class="flex-vertical">
				<h3>Hipotesis</h3>
				<div class="btn-group">
<%-- 					<button type="button" class="btn btn-primary btn-filtro"><a href="/home?id=${idProyecto}">Sin experimento</a></button> --%>
						<c:out value="${filtroSinExperimento}" escapeXml="false"/>
				</div>
				
			</div>
			<div class="flex-vertical">
				<h3>Experimentos</h3>
				<div class="btn-group">
				<c:out value="${filtroExperimento}" escapeXml="false"/>
				</div>
			</div>
			
			<figure class="arbol" onClick="open_modal_arbol()">
				<img src="${pageContext.request.contextPath}/resources/img/arbol4.png">
				<figcaption><p>¿En que punto estamos?</p></figcaption>
			</figure>
			
			<div class="flex-vertical" onClick="open_modal_share();">
				<label>
					<i class="fas fa-share fa-4x"></i>
				</label>
				<label>Compartir</label>
			</div>
			
		</div>

				
		<div class="container-canvas">
	      <!-- Canvas -->
	      <table id="bizcanvas" class="table-lienzo" cellspacing="0" border="1">
	        <!-- Upper part -->
	        <tr>
	          <td class="sociosclaves" colspan="2" rowspan="2">

	            	<c:out value="${sociosClave}" escapeXml="false"/>
	          </td>
	          <td class="actividadesClave" colspan="2">
	            	
	            	<c:out value="${actividadesClave}" escapeXml="false"/>
	          </td>
	          <td class="propuestaValor" colspan="2" rowspan="2">
	          		
	           		<c:out value="${propuestaValor}" escapeXml="false"/>
	          </td>
	          <td class="relacionClientes" colspan="2">
	          		
	            <c:out value="${relacionClientes}" escapeXml="false"/>
	          </td>
	          <td class="segmentosClientes" colspan="2" rowspan="2">
	          
	          		
	            <c:out value="${segmentosClientes}" escapeXml="false"/>
	          </td>
	        </tr>
	
	        <!-- Lower part -->
	        <tr>
	          <td class="recursosClave" colspan="2">
	          		
	            <c:out value="${recursosClave}" escapeXml="false"/>
	          </td>
	          <td class="canales" colspan="2">
	          		
	            <c:out value="${canales}" escapeXml="false"/>
	          </td>
	        </tr>
	        <tr>
	          <td class="estructuraCostos" colspan="5">
	          		
	            <c:out value="${estructuraCostos}" escapeXml="false"/>
	          </td>
	          <td class="fuentesIngreso" colspan="5">
	          		
	            <c:out value="${fuentesIngreso}" escapeXml="false"/>
	          </td>
	        </tr>
	      </table>
	      <!-- /Canvas -->
	    </div>
	    <br>
	    <br>
	    
	    
	    
	     <!--Modal crear hipotesis-->
         <div class="modal fade" id="modal-crear-hipotesis" role="dialog" style="height: auto;">
             <div class="modal-dialog">
                 <div class="modal-content hipotesis">
                     <div class="modal-header">
                         <div>
                             <label>¿Que estás pensando...?</label>
                         </div>
                     </div>
                     <div class="modal-body pop-inner">
                         <label id="idProyecto" style="display:none;">id</label>
                         <textarea id="txtNewHipotesis" class="border-1 form-control" name="" style="width: 100%;"></textarea>	
                         <br>
                         <br>
                         <label class="color-white">Áreas del lienzo (seleccione por lo menos una)</label>
						        <div class="table-wrapper-scroll-y my-custom-scrollbar">
						      		<table id="tableAreas2" class="table table-bordered table-striped mb-0" cellspacing="0" width="100%">
								        <thead>
								            <tr>
								                <th>#</th>
								                <th>Áreas</th>
								            </tr>
								        </thead>
								        <tbody>								            
								            <c:out value="${tablaAreas}" escapeXml="false"/>								            
								        </tbody>
								    </table>
						        	
						      	</div>		
						
                     </div>
                     <div id="msgErrorCrear" class="alert alert-danger" style="display:none">
				    	Por favor, validar los campos
			  		 </div>
                     <div style="margin:auto;">
                             <button type="button" onClick="confirmEditarProyecto()" class="swal2-confirm swal2-styled button-principal">Guardar</button>
                             <button type="button" onClick="" class="swal2-cancel swal2-styled" style="background-color:#bdbdbd;">Cancelar</button>                                  
                     </div>
                 </div>
             </div>
         </div>
   
      <div class="float" onClick="open_modal_comments();">
		<i class="far fa-comment my-float fa-4x"></i>
	  </div>
       
       <!--Modal comentarios-->
       <div id="modalComment" class="modal fade" role="dialog">
		    <div class="modal-dialog comentario">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                 <h4 class="modal-title"></h4>  
		
		            </div>
		            <div class="modal-body">
		            	
<!-- 		                <h3>Aun no hay comentarios</h3> -->
<!-- 		                <p>Para comentar una hipótesis, ingresa al detalle</p> -->
		                
		                <div class="flex-column scrollbar-comentario" id="style-2">
		                
		                	<ul id="lista_comentario" class="list">
		                		<c:out value="${comentariosHtml}" escapeXml="false"/>
		                		
		                	</ul>
		                </div>
		               
		            </div>
		            <div class="modal-footer">
		            	<textarea id="txtComentario" class="form-control" placeholder="Escribe tu comentario..."></textarea> 
		            	<button id="loadpage" onClick="saveComentario();" type="button" class="btn btn-primary">Enviar</button>
		            </div>
		        </div>
		        <!-- /.modal-content -->
		    </div>
		    <!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
       
         
         
         <!--Modal crear hipotesis-->
         <div class="modal fade" id="modal-info-lienzo" role="dialog" style="height: auto;">
             <div class="modal-dialog">
                 <div class="modal-content hipotesis">
                 
                 
                 
                     	<div class="row">
							  <div class="col-4">
							    <div class="list-group" id="list-info" role="tablist">
							      <a class="list-group-item list-group-item-action active" id="list-segmentos-clientes" data-toggle="list" href="#segmentacionClientes" role="tab" aria-controls="segmentacionClientes">Segmentación de clientes</a>
							      <a class="list-group-item list-group-item-action" id="list-propuesta-valor" data-toggle="list" href="#propuestaValor" role="tab" aria-controls="propuestaValor">${tituloPropuestaValor}</a>
							      <a class="list-group-item list-group-item-action" id="list-canales" data-toggle="list" href="#canales" role="tab" aria-controls="canales">Canales</a>
							      <a class="list-group-item list-group-item-action" id="list-relacion-clientes" data-toggle="list" href="#relacionClientes" role="tab" aria-controls="relacionClientes">${tituloRelacionClientes}</a>
							      <a class="list-group-item list-group-item-action" id="list-fuentes-ingreso" data-toggle="list" href="#fuentesIngreso" role="tab" aria-controls="home">Fuentes de ingreso</a>
							      <a class="list-group-item list-group-item-action" id="list-recursos-clave" data-toggle="list" href="#recursosClave" role="tab" aria-controls="profile">${tituloRecursosClave}</a>
							      <a class="list-group-item list-group-item-action" id="list-actividades-clave" data-toggle="list" href="#actividadesClave" role="tab" aria-controls="messages">${tituloActividadesClave}</a>
							      <a class="list-group-item list-group-item-action" id="list-socios-clave" data-toggle="list" href="#sociosClave" role="tab" aria-controls="settings">${tituloSociosClave}</a>
							      <a class="list-group-item list-group-item-action" id="list-estructura-costos" data-toggle="list" href="#estructuraCostos" role="tab" aria-controls="settings">Estructura de costos</a>
							    </div>
							  </div>
							  <div class="col-8">
							    <div class="tab-content" id="nav-tabContent">
							      <div class="tab-pane fade show active" id="segmentacionClientes" role="tabpanel" aria-labelledby="list-segmentos-clientes">
							      		<c:out value="${infoSegmentosClientes}" escapeXml="false"/>
							      		
				   				 </div>
							      <div class="tab-pane fade" id="propuestaValor" role="tabpanel" aria-labelledby="list-propuesta-valor">
							      		<c:out value="${infoPropuestaValor}" escapeXml="false"/>
							      		
							      </div>
							      <div class="tab-pane fade" id="canales" role="tabpanel" aria-labelledby="list-canales">
							      		<c:out value="${infoCanales}" escapeXml="false"/>
							      	 	 
							      
							      </div>
							      <div class="tab-pane fade" id="relacionClientes" role="tabpanel" aria-labelledby="list-relacion-clientes">
							      		 <c:out value="${infoRelacionClientes}" escapeXml="false"/>
						      			 

							      </div>
							      <div class="tab-pane fade" id="fuentesIngreso" role="tabpanel" aria-labelledby="list-fuentes-ingreso">
							      		<c:out value="${infoFuentesIngreso}" escapeXml="false"/>
							      	 
							      
							      
							      </div>
							      <div class="tab-pane fade" id="recursosClave" role="tabpanel" aria-labelledby="list-recursos-clave">
							      	<c:out value="${infoRecursosClave}" escapeXml="false"/>
							      	 
							      
							      </div>
							      <div class="tab-pane fade" id="actividadesClave" role="tabpanel" aria-labelledby="list-actividades-clave">
							     		 <c:out value="${infoActividadesClave}" escapeXml="false"/>
							      		
							      
							      </div>
							      <div class="tab-pane fade" id="sociosClave" role="tabpanel" aria-labelledby="list-socios-clave">
							      
							      		<c:out value="${infoSociosClave}" escapeXml="false"/>
							      		
							      
							      </div>
							      <div class="tab-pane fade" id="estructuraCostos" role="tabpanel" aria-labelledby="list-estructura-costos">
							      
							      		<c:out value="${infoEstructuraCostos}" escapeXml="false"/>
							      		 
							      
							      </div>
							    </div>
							  </div>
						</div>
                     
                     
                 </div>
             </div>
         </div>
         
<%-- 		<label style="display:none" id="nombreProyecto">${nombreProyecto}</label> --%>
		<label style="display:none" id="idProyecto2">${idProyecto}</label>
       
       	  <!--Modal share-->
         <div class="modal fade" id="modal-share" role="dialog" style="height: auto;">
             <div class="modal-dialog">
                 <div class="modal-content hipotesis">
                     <div class="modal-header">
                         <div>
                             <label>Comparte</label>
                         </div>
                     </div>
                     <div class="modal-body pop-inner">
                         	<div class="wrap">
							<!-- 							   <div class="search"> -->
														      <input style="display:none" id="txtUrl" name="txtUrl" type="text" class="searchTerm">
							<!-- 							      <button onclick="copyUrl()" type="submit" class="searchButton"> -->
							<!-- 							        <i class="far fa-copy"></i> -->
							<!-- 							     </button> -->
							<!-- 							   </div> -->
							<div class="search">
							      <input id="txtEmail" name="txtEmail" type="email" class="searchTerm" placeholder="Escribe el correo del revisor...">							      
							   </div>
							   <button onclick="enviarInvitacion()" type="submit" style="width:100%" class="searchButton">
							        Enviar invitación
							     </button>
							</div>
						
                     </div>                                    
                 </div>
             </div>
         </div>
         
         
         
         
         
         <div class="modal fade" id="modalArbol" role="dialog" style="height: auto;">
             <div class="modal-dialog">
                 <div class="modal-content arbol">
                     <div id="contentArbol" class="modal-body pop-inner scrollbar">
                     
<!--                          	<section id="visualisation" style="border: 1px black solid; max-width: 1100px;"> -->
<!-- 							</section> -->
						
                     </div>                                    
                 </div>
             </div>
         </div>
         
         
         
         
         
         
       
         
	</div>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.js"></script>

	<!-- SweetAlert2 --> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ws/endpoint.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/loginService.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/proyectoService.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ws/endpoint.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/canvas.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/menu.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
	<script src="https://cdn.jsdelivr.net/gh/deltoss/d3-mitch-tree@1.0.2/dist/js/d3-mitch-tree.min.js"></script>
	<script>
          $(document).on( 'click', '.salir', function(){
            sessionStorage.removeItem('user');
            window.location.href = '/login';
          })
        </script>
        
	<script>

	 $(document).ready(function() {
	      $("#milienzo").addClass("active");
	});
	
		window.onload = function() {      

			   
				var tableAreas = $('#tableAreas2').DataTable({
					searching: false,
					ordering: false,
					bInfo: false,
				    paging: false,
				    "columnDefs": [
						{
							"targets": [0],
							"visible": false
							}
					    ]
				} );
				  
			    $('#tableAreas2 tbody').on( 'click', 'tr', function () {
			       $(this).toggleClass('selected');			       
			    });

			    
		 }

		
		
		function open_modal_newHipotesis(){
			
			$("#modal-crear-hipotesis").modal();
		}

		function confirmEditarProyecto(){
			var tableAreas = $('#tableAreas2').DataTable();
			var rows = tableAreas.rows('.selected').data();
			var hipotesis = $('#txtNewHipotesis').val();
			var publico = $('#txtNewPublico').val();
			var tiempo = $('#txtNewTiempo').val();
			 
			var proyectoAreas = []
		    
			var i;
			for (i = 0; i < rows.length; i++) {
			  proyectoAreas.push({"id": + rows[i][0]});
			}

			if(proyectoAreas.length==0 || hipotesis == ""){
				$("#msgErrorCrear").show();
			}else{
				var nuevaHipotesis = {
						suposicion: hipotesis,
						publico: publico,
						duracion: tiempo,
						proyectoAreas: proyectoAreas
				}

				
				registrarHipotesisWS(nuevaHipotesis);
			}

			
		}

		function segmentosClientes(){
			$('#list-info a:nth-child(1)').tab('show') // Select first tab
			$("#modal-info-lienzo").modal();
		}

		function propuestaValor(){
			
			$('#list-info a:nth-child(2)').tab('show') // Select first tab
			$("#modal-info-lienzo").modal();
		}

		function canales(){
			$('#list-info a:nth-child(3)').tab('show') // Select first tab
			$("#modal-info-lienzo").modal();
		}

		function reclacionClientes(){
			$('#list-info a:nth-child(4)').tab('show') // Select first tab
			$("#modal-info-lienzo").modal();
		}

		function fuentesIngreso(){
			$('#list-info a:nth-child(5)').tab('show') // Select first tab
			$("#modal-info-lienzo").modal();
		}

		function recursosClave(){
			$('#list-info a:nth-child(6)').tab('show') // Select first tab
			$("#modal-info-lienzo").modal();
		}

		function actividadesClave(){
			$('#list-info a:nth-child(7)').tab('show') // Select first tab
			$("#modal-info-lienzo").modal();
		}
		

		function sociosClave(){
			$('#list-info a:nth-child(8)').tab('show') // Select first tab
			$("#modal-info-lienzo").modal();
		}


		function estructuraCostos(){
			$('#list-info a:nth-child(9)').tab('show') // Select first tab
			$("#modal-info-lienzo").modal();
		}

		

		function closeGlobo(){
			$("#globillo").hide();
			$("#main2").css("opacity", "1");
		}

		$('#list-info a').on('click', function (e) {
			  e.preventDefault()
			  $(this).tab('show')
			})
			
		function open_modal_share(){
			$("#txtUrl").val('http://lienzanier-env.eba-ac8jtntc.us-east-2.elasticbeanstalk.com/login');
			$("#modal-share").modal();
		}

	     function copyUrl(){
		     
	    	 var copyText = $("#txtUrl");
	    	  copyText.select();
	    	  document.execCommand("copy");
		 }

		 function open_modal_comments(){
			 console.log('etntra opren_modal');
			$("#modalComment").modal();
		 }

		 function open_modal_arbol(){
			 $("#modalArbol").modal();
			 
			}

		 function enviarInvitacion(){
			 var copyText = $("#txtUrl").val();
			 var emailShare = $('#txtEmail').val();
			 var idProyecto = $('#idProyecto2').text();
			 registerShare(idProyecto, emailShare, emailShare, copyText);
			 $("#modal-share").modal('toggle');
		 }

		 $(document).on('click', '#btnResuelta', function() {
			 var liCurrent = $(this).closest('li');
			 var idComentario = $(liCurrent).attr('idComentario');
			 updateEstadoComentarioWs(idComentario, 2);
			 console.log(idComentario);
			 $(this).closest('li').remove();
		 });

		 function saveComentario(){
				var comentario = $("#txtComentario").val();
				console.log(comentario);
				var idProyecto = $("#idProyecto2").text();
				var payload = {comentario, idProyecto};
				saveComentarioProyecto(payload);
				var newComentario = "";
				newComentario = newComentario.concat('"<li class="item"><span>'+comentario+'</span></li>');

				$("#lista_comentario").append(newComentario);
		     }
	</script>
	
	
	
	
	<script>
			function getData() {

				var arbolJson = '${arbolObject}';
	        	var nodos = JSON.parse(arbolJson);
				
				console.log(nodos);
	        	var data = nodos;
// 		        	{
// 					"id": 1,
// 					"name": "Animals",
// 					"description": "A living organism that feeds on organic matter",
// 					"children": [
// 						{
// 							"id": 2,
// 							"name": "Carnivores",
// 							"description": "Diet consists solely of animal materials",
// 							"children": [
// 								{
// 									"id": 3,
// 									"name": "Felidae",
// 									"description": "Also known as cats",
// 									"children": [
// 										{
// 											"id": 4,
// 											"name": "Siamese Cat",
// 											"description": "A breed of Asian cat. it has white fur",
// 											"children": []
// 										},
// 										{
// 											"id": 5,
// 											"name": "Javanese Cat",
// 											"description": "Domestic breed of cats, of oriental origin",
// 											"children": []
// 										}
// 									]
// 								},
// 								{
// 									"id": 6,
// 									"name": "Ursidae",
// 									"description": "Also known as bears",
// 									"children": [
// 										{
// 											"id": 7,
// 											"name": "Polar Bear",
// 											"description": "White bear native to the Arctic Circle",
// 											"children": []
// 										},
// 										{
// 											"id": 8,
// 											"name": "Panda Bear",
// 											"description": "Spotted bear native to South Central China",
// 											"children": []
// 										}
// 									]
// 								},
// 								{
// 									"id": 9,
// 									"name": "Canidae",
// 									"description": "Also known as dogs",
// 									"children": [
// 										{
// 											"id": 10,
// 											"name": "Labradore Retriever",
// 											"description": "One of the most popular dog breeds in Canada, UK and USA",
// 											"children": []
// 										},
// 										{
// 											"id": 11,
// 											"name": "Golden Retriever",
// 											"description": "Generally friendly, loyal and intelligent breed of dogs",
// 											"children": []
// 										}
// 									]
// 								}
// 							]
// 						},
// 						{
// 							"id": 12,
// 							"name": "Herbivores",
// 							"description": "Diet consists solely of plant matter",
// 							"children": [
// 								{
// 									"id": 13,
// 									"name": "Bovidae",
// 									"description": "Also known as cattle or cows",
// 									"children": [
// 										{
// 											"id": 14,
// 											"name": "Angus Cattle",
// 											"description": "Scottish breed of black cattle",
// 											"children": []
// 										},
// 										{
// 											"id": 15,
// 											"name": "Holstein Friesian Cattle",
// 											"description": "Known as the highest-production dairy animals",
// 											"children": []
// 										}
// 									]
// 								},
// 								{
// 									"id": 16,
// 									"name": "Equidae",
// 									"description": "Also known as horses",
// 									"children": [
// 										{
// 											"id": 17,
// 											"name": "Barb Horse",
// 											"description": "A breed of Northern African horses with high stamina and hardiness. Their generally hot temperament makes it harder to tame.",
// 											"children": []
// 										},
// 										{
// 											"id": 18,
// 											"name": "Holsteiner Horse",
// 											"description": "One of the oldest of warmblood breeds, tracing back to the 13th century. Originates from Germany",
// 											"children": []
// 										}
// 									]
// 								},
// 								{
// 									"id": 19,
// 									"name": "Leporidae",
// 									"description": "Also known as rabbits",
// 									"children": [
// 										{
// 											"id": 20,
// 											"name": "Lionhead rabbit",
// 											"description": "Unusual rabbit with a wool mane circling it's head, similar to a lion, hence it's name.",
// 											"children": []
// 										},
// 										{
// 											"id": 21,
// 											"name": "Silver Fox Rabbit",
// 											"description": "Bred for their meat and unique fur.",
// 											"children": []
// 										}
// 									]
// 								}
// 							]
// 						}
// 					]
// 				};

				return data;
			}

			contentArbol;
			
			
			var datas = getData();
			var index=0;
			datas.forEach(function(data) {  
				console.log(data);
				var idNuevo = "visualisation"+index;
				$("#contentArbol").append("<section id='"+idNuevo+"' style='border: 1px black solid; max-width: 1100px;'> </section>");
				
				var treePlugin = new d3.mitchTree.boxedTree()
				.setData(data)
				.setAllowFocus(false)
				.setElement(document.getElementById(idNuevo))
				.setIdAccessor(function(data) {
					return data.id;
				})
				.setChildrenAccessor(function(data) {
					return data.children;
				})
				.setBodyDisplayTextAccessor(function(data) {
					return data.description;
				})
				.setTitleDisplayTextAccessor(function(data) {
					return data.name;
				})
				.initialize();
			
				// Expand all nodes
				var nodes = treePlugin.getNodes();
				nodes.forEach(function(node, index, arr) {
					treePlugin.expand(node);
				});
				treePlugin.update(treePlugin.getRoot());

				index = index+1;
			});
			
		</script>
	
	
	
	
	
	
</body>
</html>