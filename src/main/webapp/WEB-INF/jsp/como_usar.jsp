<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- mobile metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="viewport" content="initial-scale=1, maximum-scale=1">
      <!-- site metas -->
      <title>Lienzanier</title>
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- bootstrap css -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
      <!-- style css -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style_theme.css">
      <!-- Responsive-->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/responsive.css">
      <!-- fevicon -->
      <link rel="icon" href="${pageContext.request.contextPath}/resources/img/fevicon.png" type="image/gif" />
      <!-- Scrollbar Custom CSS -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery.mCustomScrollbar.min.css">
      
      
      <style> 
		
		h3 {
			margin: 10px 0 !important;
			font-family: 'Jost', sans-serif !important;;
		    font-size: 1.8rem !important;;
		    line-height: 1.5 !important;;
		}
		
		h6 {
			margin: 5px 0;
			text-transform: uppercase;
		}
		
		h1 {
			font-family: 'Jost', sans-serif !important;
		    font-size: 2.6rem !important;
		    line-height: 1.1 !important;
		    margin: auto !important;
   			 width: fit-content !important;
   			 text-align: center !important;
		}
		
		p {
			font-size: 14px;
			line-height: 21px;
		}
		
		#heading {
    text-transform: uppercase;
    color: #673AB7;
    font-weight: normal
}

#msform {
    text-align: center;
    position: relative;
    margin-top: 20px
}

#msform fieldset {
    background: white;
    border: 0 none;
    border-radius: 0.5rem;
    box-sizing: border-box;
    width: 100%;
    margin: 0;
    padding-bottom: 20px;
    position: relative
}

.form-card {
    text-align: left
}

#msform fieldset:not(:first-of-type) {
    display: none
}

#msform input,
#msform textarea {
    padding: 8px 15px 8px 15px;
    border: 1px solid #ccc;
    border-radius: 0px;
    margin-bottom: 25px;
    margin-top: 2px;
    width: 100%;
    box-sizing: border-box;
    font-family: montserrat;
    color: #2C3E50;
    background-color: #ECEFF1;
    font-size: 16px;
    letter-spacing: 1px
}

#msform input:focus,
#msform textarea:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    border: 1px solid #673AB7;
    outline-width: 0
}

#msform .action-button {
    width: 100px;
    background: #673AB7;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 0px 10px 5px;
    float: right
}

#msform .action-button:hover,
#msform .action-button:focus {
    background-color: #311B92
}

#msform .action-button-previous {
    width: 100px;
    background: #616161;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px 10px 0px;
    float: right
}

#msform .action-button-previous:hover,
#msform .action-button-previous:focus {
    background-color: #000000
}

.card {
    z-index: 0;
    border: none;
    position: relative
}

.fs-title {
    font-size: 25px;
    color: #673AB7;
    margin-bottom: 15px;
    font-weight: normal;
    text-align: left
}

.purple-text {
    color: #673AB7;
    font-weight: normal
}

.steps {
    font-size: 25px;
    color: gray;
    margin-bottom: 10px;
    font-weight: normal;
    text-align: right
}

.fieldlabels {
    color: gray;
    text-align: left
}

#progressbar {
    margin-bottom: 30px;
    overflow: hidden;
    color: lightgrey
}

#progressbar .active {
    color: #673AB7
}

#progressbar li {
    list-style-type: none;
    font-size: 15px;
    width: 25%;
    float: left;
    position: relative;
    font-weight: 400
}

#progressbar #account:before {
    font-family: FontAwesome;
    content: "\f0eb"
}

#progressbar #personal:before {
    font-family: FontAwesome;
    content: "\f0c3"
}

#progressbar #payment:before {
    font-family: FontAwesome;
    content: "\f073"
}

#progressbar #confirm:before {
    font-family: FontAwesome;
    content:  "\f00c"
}

#progressbar li:before {
    width: 50px;
    height: 50px;
    line-height: 45px;
    display: block;
    font-size: 20px;
    color: #ffffff;
    background: lightgray;
    border-radius: 50%;
    margin: 0 auto 10px auto;
    padding: 2px
}

#progressbar li:after {
    content: '';
    width: 100%;
    height: 2px;
    background: lightgray;
    position: absolute;
    left: 0;
    top: 25px;
    z-index: -1
}

#progressbar li.active:before,
#progressbar li.active:after {
    background: #673AB7
}

.progress {
    height: 20px
}

.progress-bar {
    background-color: #673AB7
}

.fit-image {
    width: 100%;
    object-fit: cover
}

.col-xl-5{
	flex: 0 0 70%;
    max-width: 100%;
}    
    
    .contenido{
			font-size: 24px;
    		line-height: 1.6;
    		font-family: open-sans, Open Sans, Helvetica Neue, Helvetica, Arial, sans-serif;
/*     		color: #555; */
		}  
      </style>
      
      
</head>
 <body >
     
     <header>
		<jsp:include page='header-inicio.jsp'/>
	</header>
	
	
	
	
	<div class="container-fluid">
	
		<div class="fs-title">
	    		<h1>Valida tus hipótesis</h1>
	    	</div>
	
	
	    <div class="row justify-content-center">
	        
	        
	        
	        <div class="col-11 col-sm-10 col-md-10 col-lg-6 col-xl-5 text-center p-0 mt-3 mb-2">
	            <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
	                <h2 id="heading">Crea, Mide, Aprende</h2>
	                <p>Crea tus hipótesis, experimenta y toma desiciones</p>
	                <form id="msform">
	                    <!-- progressbar -->
	                    <ul id="progressbar">
	                        <li class="active" id="account"><strong>Tu hipótesis</strong></li>
	                        <li id="personal"><strong>¿Listo para experimentar?</strong></li>
	                        <li id="payment"><strong>Estamos en marcha</strong></li>
	                        <li id="confirm"><strong>Finalizar</strong></li>
	                    </ul>
	                    <div class="progress">
	                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
	                    </div> <br> <!-- fieldsets -->
	                    <fieldset>
	                        <div class="form-card">
	                            <div class="row">
	                                <div class="col-7">
	                                    <h2 class="fs-title">Datos de tu hipótesis</h2>
	                                </div>
	                                <div class="col-5">
	                                    <h2 class="steps">Paso 1 - 4</h2>
	                                </div>
	                            </div> 
	                            <div>
	                            	<p class="contenido">
	                            		Crea tu hipótesis y selecciona a que sector del lienzo está relacionado
	                            	</p>
	                            	
	                            	<div>
	                            		<img src="${pageContext.request.contextPath}/resources/img/area_pertenece.png" alt="" />
	                            	</div>
	                            </div>
	                        </div>
	                            <input type="button" name="next" class="next action-button" value="Siguiente" />
	                    </fieldset>
	                    <fieldset>
	                        <div class="form-card">
	                            <div class="row">
	                                <div class="col-7">
	                                    <h2 class="fs-title">Crea tu experimento</h2>
	                                </div>
	                                <div class="col-5">
	                                    <h2 class="steps">Paso 2 - 4</h2>
	                                </div>
	                            </div> 
	                            <div>
	                           		 <p class="contenido">
	                            		Selecciona que <a href="#">métricas</a> usarás para validar hipótesis
	                            	</p>
	                            	<p>
	                            		Establece tus condiciones
	                            	</p>
	                            	
	                            	<div>
	                            		<img src="${pageContext.request.contextPath}/resources/img/crear_experimento.png" alt="" />
	                            	</div>
                            		
                            	</div>
	                            
	                        </div> 
	                        <input type="button" name="next" class="next action-button" value="Siguiente" /> <input type="button" name="previous" class="previous action-button-previous" value="Anterior" />
	                    </fieldset>
	                    <fieldset>
	                        <div class="form-card">
	                            <div class="row">
	                                <div class="col-7">
	                                    <h2 class="fs-title">Anota tus resultados</h2>
	                                </div>
	                                <div class="col-5">
	                                    <h2 class="steps">Paso 3 - 4</h2>
	                                </div>
	                            </div>
	                            <div>
	                           		 <p class="contenido">
	                            		Anota tus resultados y visualiza el cumplimiento de las condiciones establecidas
	                            	</p>
	                            	
	                            	<div>
	                            		<img src="${pageContext.request.contextPath}/resources/img/agenda_how.png" alt="" />
	                            	</div>
                            		
                            	</div>
	                        </div> 
	                        <input type="button" name="next" class="next action-button" value="Siguiente" /> <input type="button" name="previous" class="previous action-button-previous" value="Anterior" />
	                    </fieldset>
	                    <fieldset>
	                        <div class="form-card">
	                            <div class="row">
	                                <div class="col-7">
	                                    <h2 class="fs-title">Toma desiciones</h2>
	                                </div>
	                                <div class="col-5">
	                                    <h2 class="steps">Paso 4 - 4</h2>
	                                </div>
	                            </div>
	                            <div>
	                           		 <p class="contenido">
	                            		Visualiza la fluctuación de tus resultados de cada día
	                            	</p>
	                            	
	                            	<div>
	                            		<img src="${pageContext.request.contextPath}/resources/img/historial_how.png" alt="" />
	                            	</div>
                            		
                            	</div>
	                        </div>
	                        <input type="button" name="previous" class="previous action-button-previous" value="Anterior" />
	                    </fieldset>
	                </form>
	            </div>
	        </div>
	    	
	    	
	    	
	    </div>
	    
	    
<!-- 	    	<div class="fs-title"> -->
<!-- 	    		<h1>Mide el progreso de tu startup!</h1> -->
<!-- 	    	</div> -->
	    	
<!-- 	    	<div> -->
	    	
<!-- 	    		<h2>Visualiza los indicadores necesarios para medir tu startup</h2> -->
<!-- 	    		<div> -->
<!-- 	    			<p>En este apartado podrás visualizar los indicadores que estés evaluando en este momento. Se deberá crear las métricas que fuesen necesarias para  -->
<!-- 	    			  medir el indicador,</p> -->
<%--                		<img src="${pageContext.request.contextPath}/resources/img/indicadores.png" alt="" /> --%>
<!--                	</div> -->
<!-- 	    	</div> -->
	</div>
	

     
      <!-- end footer -->
      <!-- Javascript files-->
      <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/plugin.js"></script>
      <!-- Scrollbar Js Files -->
      <script src="${pageContext.request.contextPath}/resources/js/jquery.mCustomScrollbar.concat.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/custom.js"></script>
      

      <script>
      $(document).ready(function(){

    	  $("#comoUsar").addClass("active");
    	  
    	  var current_fs, next_fs, previous_fs; //fieldsets
    	  var opacity;
    	  var current = 1;
    	  var steps = $("fieldset").length;

    	  setProgressBar(current);

    	  $(".next").click(function(){

    	  current_fs = $(this).parent();
    	  next_fs = $(this).parent().next();

    	  //Add Class Active
    	  $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

    	  //show the next fieldset
    	  next_fs.show();
    	  //hide the current fieldset with style
    	  current_fs.animate({opacity: 0}, {
    	  step: function(now) {
    	  // for making fielset appear animation
    	  opacity = 1 - now;

    	  current_fs.css({
    	  'display': 'none',
    	  'position': 'relative'
    	  });
    	  next_fs.css({'opacity': opacity});
    	  },
    	  duration: 500
    	  });
    	  setProgressBar(++current);
    	  });

    	  $(".previous").click(function(){

    	  current_fs = $(this).parent();
    	  previous_fs = $(this).parent().prev();

    	  //Remove class active
    	  $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    	  //show the previous fieldset
    	  previous_fs.show();

    	  //hide the current fieldset with style
    	  current_fs.animate({opacity: 0}, {
    	  step: function(now) {
    	  // for making fielset appear animation
    	  opacity = 1 - now;

    	  current_fs.css({
    	  'display': 'none',
    	  'position': 'relative'
    	  });
    	  previous_fs.css({'opacity': opacity});
    	  },
    	  duration: 500
    	  });
    	  setProgressBar(--current);
    	  });

    	  function setProgressBar(curStep){
    	  var percent = parseFloat(100 / steps) * curStep;
    	  percent = percent.toFixed();
    	  $(".progress-bar")
    	  .css("width",percent+"%")
    	  }

    	  $(".submit").click(function(){
    	  return false;
    	  })

    	  });


      </script>
      
      
   </body>
</html>