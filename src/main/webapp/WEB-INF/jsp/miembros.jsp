<%@ taglib prefix="c" uri="http://www.hdiv.org/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Lienzanier</title>
	<link href='http://fonts.googleapis.com/css?family=Headland+One' rel='stylesheet' type='text/css'>
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/canvas.css">
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/menu.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/sticky.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/steps.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">    
    <link href="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.css" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    
    <style>
    
    	p > a{
/*     		color: white; */
   			font-size: 1.2rem;
    	}
    	
    	#main2{
    		margin: 80px 150px;
    	}
    	
    	.mrg-bot-16{
    		margin: 0 250px;
    	}
   
   		#msform{
   			text-align: left;
   			padding: 10px;
   		}
   		
   		.descripcion{
   			padding: 8px 15px 8px 15px;
		    border: 1px solid #ccc;
		    border-radius: 0px;
		    margin-bottom: 25px;
		    margin-top: 2px;
		    width: 100%;
		    box-sizing: border-box;
		    font-family: montserrat;
		    color: #2C3E50;
		    background-color: #ECEFF1;
		    font-size: 16px;
		    letter-spacing: 1px;
   		}
   		
   		
	
    </style>
    
</head>
<body>

	<header>
		<jsp:include page='header.jsp'/>
	</header>
	
	<div id="main2">
		<form id="msform">
			<div class="col-10">
				<div style="display:flex;">
					<h2 class="fs-title">Proyecto</h2><label onClick="openModalEditProyecto();" style="margin-left:10px"><i class="fas fa-edit fa-2x"></i></label>
				</div>
               	
               	<textarea id="txtProyecto" disabled>${proyecto.nombre}</textarea>
            </div>
            <div class="col-10">
            	<div style="display:flex;">
            		<h2 class="fs-title">Descripción</h2><label onClick="openModalEditDescripcion();"style="margin-left:10px"><i class="fas fa-edit fa-2x"></i></label>
            	</div>
               	
               	<div class="descripcion" id="descripcionTxt">
               		${proyecto.descripcion}
               	</div>
               	
<%--                	<textarea id="txtEditSuposicion" disabled>${proyecto.descripcion}</textarea> --%>
            </div>
		</form>
												
	
	
		
	
		
		 <div>
			 <table id="listProyectos" class="table table-striped">
                    <thead>  
               	       <tr>
	               	       	<td>#</td>
	               	       	<td>Nombre</td>
	               	       	<td>Email</td>
	               	       	<td>Eliminar</td>
               	       </tr>              
                        
                    </thead>
                    <tbody>
                          <c:out value="${lista}" escapeXml="false"/>
                    </tbody>
                </table>
		 	
		 </div>
		
         
         <div class="mrg-bot-16">
            <span>Buscar integrantes</span>
            <input id="txtSearchUsuario" class="form-control my-0 py-1 lime-border" type="text" placeholder="Ingrese correo electrónico" aria-label="Search">
            <span  id="btnSearchEmail" onclick='searchUsuarioProy()'><i class="fas fa-search"></i></span>
            
        </div>
        <div style="margin: 0 250px;">
            <table id="tblUsuarioBuscados" data-toggle="table">
                <tbody>
                    
                </tbody>
            </table>                                        
        </div>
        
        
        
        <!--Modal edit nombre hipotesis-->
	          <div class="modal fade" id="modal-edit-proyecto" role="dialog" style="height: auto;">
	              <div id="msform" class="modal-dialog">
	                  <div class="modal-content">
	                      <div class="modal-header">
	                          <div>
	                              <label>Editar nombre de proyecto</label>
	                          </div>
	                      </div>
	                      <div class="modal-body pop-inner">
	                          <label>Proyecto</label>
	                          <textarea id="txtEditProyecto"></textarea>                      
	                      </div>
	                      <div style="margin:auto;">
	                              <button type="button" onClick="confirmEditarProyecto()" class="swal2-confirm swal2-styled" style="background-color:#9e1b32;">Guardar</button>
	                              <button type="button" onClick="" class="swal2-cancel swal2-styled" style="background-color:#bdbdbd;">Cancelar</button>                                  
	                      </div>
	                  </div>
	              </div>
	          </div>
         
         
            <!--Modal edit nombre hipotesis-->
	          <div class="modal fade" id="modal-edit-descripcion" role="dialog" style="height: auto;">
	              <div id="msform" class="modal-dialog">
	                  <div class="modal-content">
	                      <div class="modal-header">
	                          <div>
	                              <label>Editar descripción de proyecto</label>
	                          </div>
	                      </div>
	                      <div class="modal-body pop-inner">
	                          <label>Descripción</label>
	                          <textarea type="text" class="form-control" name="txtEditDescripcion" id="txtEditDescripcion" rows="10" cols="80"></textarea>                     
	                      </div>
	                      <div style="margin:auto;">
	                              <button type="button" onClick="confirmEditarDescripcion()" class="swal2-confirm swal2-styled" style="background-color:#9e1b32;">Guardar</button>
	                              <button type="button" onClick="" class="swal2-cancel swal2-styled" style="background-color:#bdbdbd;">Cancelar</button>                                  
	                      </div>
	                  </div>
	              </div>
	          </div>
<%-- 		<label style="display:none" id="nombreProyecto">${nombreProyecto}</label> --%>
		<label style="display:none" id="idProyecto2">${idProyecto}</label>
		
		<button type="button" onclick="eliminarProyecto()" class="swal2-confirm swal2-styled" style="background-color:#9e1b32;float: right;">Eliminar proyecto</button>
       	<button type="button" onclick="confimarEditarProyecto();" class="swal2-confirm swal2-styled" style="background-color:#9e1b32;float: right;">Guardar Cambios</button>
        
	</div>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.js"></script>

	<!-- SweetAlert2 --> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ws/endpoint.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/loginService.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/proyectoService.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ws/endpoint.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/canvas.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/menu.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ckeditor.js"></script>
	<script>
          $(document).on( 'click', '.salir', function(){
            sessionStorage.removeItem('user');
            window.location.href = '/login';
          })
        </script>
        
	<script>
		window.onload = function() {      
			
		}

		$(document).ready(function() {
		      $("#miembros").addClass("active");
		});

		function searchUsuarioProy (){
		    var emailSearch = $("#txtSearchUsuario").val();
		    searchUsarioByEmailWs(emailSearch);
		}

		 $(document).on('click', '#x', function() {
				
			   $(this).closest('li').remove();
			});

		function eliminarProyecto(){
			var idProyecto2 = $("#idProyecto2").text();
			Swal.fire({
				  title: '¿Seguro que desea eliminar el proyecto?',
				  text: "Una vez realizado no podrá recuperar la información",
				  icon: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si, eliminar'
				}).then((result) => {
					console.log('Si elimina e2 ');
				  if (result.value) {
					  console.log('si eliminar');
					  eliminarProyectoWs(idProyecto2);
				    
				  }
				})
		}

		function openModalEditProyecto(){
			$('#txtEditProyecto').val($("#txtProyecto").text());
            $("#modal-edit-proyecto").modal();
		}

		function confirmEditarProyecto(){
			 $("#txtProyecto").text($('#txtEditProyecto').val());
			 $('#modal-edit-proyecto').modal('toggle');
			}
		
		function openModalEditDescripcion(){
			CKEDITOR.instances.txtEditDescripcion.setData($("#descripcionTxt").text());
			$("#modal-edit-descripcion").modal();		
		}

		function confirmEditarDescripcion(){
			var data = CKEDITOR.instances.txtEditDescripcion.getData();
			$("#descripcionTxt").html(data);
			$('#modal-edit-descripcion').modal('toggle');
		}

		function confimarEditarProyecto(){
			var idProyecto = $("#idProyecto2").text();
			var nombre = $("#txtProyecto").text();
			var descripcion = $("#descripcionTxt").html();
			editProyectoWS(nombre, descripcion, idProyecto);
		}
		
	</script>
	
	<script>
          // Replace the <textarea id="editor1"> with a CKEditor 4
          // instance, using default configuration.
          CKEDITOR.replace( 'txtEditDescripcion' );
     </script>
	
</body>
</html>