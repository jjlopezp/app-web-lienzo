<%@ taglib prefix="c" uri="http://www.hdiv.org/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Lienzanier</title>
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/cards.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/menu.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
    <link href="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.css" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    
    <style>
    	td > a {
    		color: #007bff;
    	}
    	
    	td > a:hover {
    		color: #007bff;
    		text-decoration: none;
    	}
    	
    	h2{
    		font-size: 1rem;
    		display: block !important; 
    		color: black !important;
    	}
    	
    	ul{
    	
    		list-style-type: none;
    	}
    	
    	.pro-ul{
    		border: 2px solid darkgray;
   		    background-color: darkgray;
   		    border-radius: 25px;
    	}
    	
    	ul li{
			width: max-content;
			display: inline-block;
			padding: 10px;
    	}
    	
    	img{
    		display: block;
    		margin: auto;
    	}
    	
    	.mr-ri{
    		margin-top: 20px;
    	}
    	
    	li a{
    		width: 100%;
    	}
    	
    	h1{
    		text-align: center;
    	}
    	
    </style>
</head>
<body>
	<header>
		<jsp:include page='header_medio.jsp'/>
	</header>

	<div id="main2">
        <section class="mr-ri mr-lf">
          
            <br>
			<h1>
				Bienvenido, por favor seleccione el proyecto<br>que desea revisar
			</h1>
            
            <section class="cards">
           		 <c:out value="${proyectos}" escapeXml="false"/>

            </section>
            
      
            
            
         
                

        </section>
    </div>
    
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.js"></script>

	<!-- SweetAlert2 --> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ws/endpoint.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/loginService.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/proyectoService.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/menu.js"></script>
<script>
          $(document).on( 'click', '.salir', function(){
            sessionStorage.removeItem('user');
            window.location.href = '/login';
          })
        </script>
 <script>
        window.onload = function() {         
//             reloadTableProyecto();
            clearTablSearchUserSeleccionado();
        }

        $('#tblUsuarioSeleccionados').bootstrapTable({
            formatNoMatches: function () {
                return '';
            }
        });

        $('#tblUsuarioSeleccionados').bootstrapTable({
            formatNoMatches: function () {
                return 'No se encontró ningún miembro con su búsqueda';
            }
        });

        $('#tblUsuarioBuscados tbody').on( 'click', 'tr', function () {
            var idUser = ($(this).attr('value'));
            var tr = '<tr id='+idUser+' value='+idUser+'>';
            tr = tr.concat($(this).html());
            tr = tr.concat('<td><i class="fas fa-user-times"></i></td>');
            tr = tr.concat('</tr>');
            $("#tblUsuarioSeleccionados").find('tbody').append(tr);
            clearTablSearchUser();
        } );

        $('#tblUsuarioSeleccionados tbody').on( 'click', 'tr', function () {
            var value = ($(this).attr('value'));
            $('#'+value).remove();
        } );

        $(document).on( 'click', '.remProy', function(){
            swal.fire(
                {
                    title: 'Eliminar proyecto',
                    text: 'Por favor, confirme su elección',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#9e1b32',
                    cancelButtonColor: '#bdbdbd',
                    confirmButtonText: 'Confirmar',
                    cancelButtonText: 'Cancelar'
                }
            ).then((result) => {
                if (result.value) {
                    var current_row = $(this).parents('tr');
                    var value = current_row.attr('value');
                    deleteProyectoWs(value);                 
                }
            })
        })

        $(document).on( 'click', '.editProy', function(){
            var current_row = $(this).parents('tr');
            var nombre = current_row.find("td:eq(0)").text();
            var descripcion = current_row.find("td:eq(1)").text(); 
            var id = current_row.attr('value');           
            fillModalEditProyecto(nombre, descripcion, id);
            $("#modal-edit-proyecto").modal();
        })
        
        function fillModalEditProyecto(nombre, descripcion, id){
            $('#txtEditNomProy').val('');
            $('#txtEditDescripProy').val('');
            $('#idProyecto').val('');
            $('#txtEditNomProy').val(nombre);
            $('#txtEditDescripProy').val(descripcion);
            $('#idProyecto').val(id);
        }

        function reloadTableProyecto(){
            clearTablProyectos();
            getAllProyectosWs();
        }

        function confirmEditarProyecto(){
            var nombre = $('#txtEditNomProy').val();
            var descripcion = $('#txtEditDescripProy').val();
            var id = $('#idProyecto').val();
            editProyectoWS(nombre, descripcion, id);
            $('#modal-edit-proyecto').modal('toggle');
        }

        function openModalCrearProyecto(){
        	$('#modal-crear-proyecto').modal();
         }
        
    </script>
	
</body>
</html>