<%@ taglib prefix="c" uri="http://www.hdiv.org/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Lienzanier</title>
	<link href='http://fonts.googleapis.com/css?family=Headland+One' rel='stylesheet' type='text/css'>
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/canvas.css">
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/menu.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/steps.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/options.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/tileswrap.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/timeline.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/result.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">    
    <link href="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.css" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    
    
    <style>
    	label{
    		font-size: 1.5rem;
    	}
    	
    	#main2::after{
    		content: "";
/* 			background-image: url('${pageContext.request.contextPath}/resources/img/creaMideAprende.png');    	 */
    		opacity: 0.5;
			  top: 0;
			  left: 0;
			  bottom: 0;
			  right: 0;
			  position: absolute;
			  z-index: -1; 
    	}
    	
    	.edit-hipotesis-container{
    		border: 1px solid black;
		    width: 60%;
		    margin: 30px auto !important;
		    background-color: antiquewhite;
		    border-radius: 50px;
		    padding-right: 10rem;
    	}
    	
    	#tablePuntoMedicion2{
    		padding-left: 0 !important;
    	}
    	
    	.info-left{
    		width: 30%;
    		margin: 0 5%;
    	}
    	
    	.info-derecha{
    		width: 50%;
    		margin: 0 5%;
    	}
    	
    	.align-left{
    		text-align: left;
    	}
    	
    	.first-element-derecha{
    	
    		margin-top: 7rem;
    	}
    	
    	.flex-derecha{
    		display: flex;
			flex-wrap: wrap;
			margin-bottom: 16px;
			word-break: break-word;
    	}
    	
    	.detalle-experimento-container{
    		border: 1px solid black;
		    width: 70%;
		    margin: 30px auto !important;
 		    background-color: antiquewhite; 
		    border-radius: 50px;
		    display:flex
    	}
    	
    	.col-4{
    		max-width: 100%;
    	}
    	
    	.col-8{
    		max-width: 100%;
    	}
    	
    	.col-10{
    		flex: 0 0 53.333333% !important;
    		margin-top: 2rem;
    	}
    	
    	.div-timeline ul li{
    		list-style: none;
    	}
    	
    	#boxMetrica, .liCondicion{
    		width: 33.333333%;
    	}
    	
    	.row li{
    		float: left;
    	}
    	
    	.col-sm-4{
    		max-width: 100% !important;
    	}
    	
    	.width-max{
    		max-width: 280% !important;
    		width: 106% !important;
    	}

		#txtConclusion{
			height: 150px !important;
		}
		
		#list_totalizado{
			list-style-type: none;
		}
		#list_totalizado li{
			display: inline-block;
		}
		
		.tilesWrap p, .tilesWrap h2, .tilesWrap h3{
	text-align: center;
}

#listaCondiciones p{
	color: #000 !important;
	font-size: 22px !important;
}

#listaCondiciones li{
	background: #55686d !important;
}

#listaMetricas p{
	font-size: 22px !important;
}
.ui-widget.ui-widget-content{
	margin: auto;
}

.box-item p{
	width: fit-content;
	margin: auto;
}

.liCondicion .box-tittle{
	color: #fff;
	font-size: 18px;
}

.course-preview h6, .course-preview h2{
	color: #fff !important;
}

.float{
	position:fixed;
	width:150px;
	height:80px;
	bottom:10px;
	right:40px;
	background-color:#0C9;
	color:#FFF;
	border-radius:50px;
	text-align:center;
	box-shadow: 2px 2px 3px #999;
}

.my-float{
	margin-top:22px;
}

.comentario{
	max-width: 1000px !important;
    max-height: 800px !important;
    height: 800px !important;
}

.scrollbar-comentario button{
	    height: fit-content;
    margin-left: 20px;
}

.scrollbar-comentario .item{
	    display: block;
}
	.scrollbar-comentario a{
    margin-left: 20px;
}
    </style>
</head>
<body>
	<header>
		<jsp:include page='header.jsp'/>
	</header>
	<div id="main2">


			<div class="container-fluid">
		        <div class="">
		                <div class="">
		                    <div class="">                      
		
		                    <h2 id="heading">Sal del edificio</h2>
		                    <p style="text-align: center">Apunta tus resultados</p>
		                    <form id="msform">
		                        <!-- progressbar -->
		                        <ul id="progressbar">
		                            <li class="active" id="account"><strong>Tu hipótesis</strong></li>
		                            <li class="active" id="personal"><strong>¿Listo para experimentar?</strong></li>
		                            <li class="active" id="payment"><strong>Estamos en marcha</strong></li>
		                            <li class="active" id="confirm"><strong>Finalizar</strong></li>
		                        </ul>
		                        <div class="progress">
		                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
		                        </div> <br> <!-- fieldsets -->
		                        <fieldset>
		                                
		                            <div class="">
		                                <div class="">
		                                
		                                	<div class="flex-lienzo">
		                                		<div class="col-10">
		                                        	<h2 class="fs-title">¿Que piensas?</h2>
		                                        	<textarea id="txtEditSuposicion" disabled>${suposicion}</textarea>
			                                    </div>
			                                    <div class="col-10">
			                                            <h2 class="fs-title">¿De que trata?</h2>
			                                            <textarea disabled>${descripcion}</textarea>
			                                    </div>
		                                	</div>		                                
		                                	
		                                	<div class="width-max col-10">
	                                        	<h2 class="fs-title">¿Que concluyes?</h2>
	                                        	<textarea id="txtConclusion"></textarea>
		                                    </div>
       
		                                    <div class="col-10">
		                                    	<h2 class="fs-title">¿Cómo validas tu hipótesis?</h2>
		                                    	<div>
		                                    		<h2>${tipoPrueba}</h2>
		                                    	</div>
		                                    	<ul id="listaMetricas" class="tilesWrap">
		                                    		<c:out value="${metricas}" escapeXml="false"/>
		                                    	
												</ul>
												<ul id="listaCondiciones" class="tilesWrap">
		                                    		<c:out value="${condiciones}" escapeXml="false"/>
		                                    	
												</ul>

		                                    </div>
		                                    
		                                    <div class="width-max col-10">
	                                        	<h2 class="fs-title">Historial</h2>
	                                        	<div id="chartContainer" style="height: 370px; max-width: 920px; margin: 0px auto;">
												</div>
		                                    </div>
		                                    
		                                    <div class="width-max col-10">
	                                        	<h2 class="fs-title">Total</h2>
	                                        	<ul id="list_totalizado">
	                                        		
	                                        	</ul>
	                                        	
	                                        	<ul id="list_condiciones" class="tilesWrap">
	                                        		
	                                        	</ul>
		                                    </div>
	
		                
										</div>
		                            </div> 		                            
                          			<label style="display:none" id="idHipotesis">${hipotesisDto.id}</label>
									<label style="display:none" id="idProyecto">${idProyecto}</label>
									<label style="display:none" id="idExperimento">${idExperimento}</label>
									
									 <div class="float" onClick="open_modal_comments();">
		<i class="far fa-comment my-float fa-4x"></i>
	  </div>
		                        </fieldset>
		                        
		                    </form>
		                </div>
		            </div>
		        </div>
		        
		        
		        
		        
       
       <!--Modal comentarios-->
       <div id="modalComment" class="modal fade" role="dialog">
		    <div class="modal-dialog comentario">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                 <h4 class="modal-title"></h4>  
		
		            </div>
		            <div class="modal-body">
		            	
<!-- 		                <h3>Aun no hay comentarios</h3> -->
<!-- 		                <p>Para comentar una hipótesis, ingresa al detalle</p> -->
		                
		                <div class="flex-column scrollbar-comentario" id="style-2">
		                
		                	<ul id="lista_comentario" class="list">
		                		<c:out value="${comentariosHipotesisHtml}" escapeXml="false"/>
		                		
		                	</ul>
		                </div>
		               
		            </div>
		            <div class="modal-footer">
		            	<textarea id="txtComentario" class="form-control" placeholder="Escribe tu comentario..."></textarea> 
		            	<button id="loadpage" onClick="saveComentario();" type="button" class="btn btn-primary">Enviar</button>
<!-- 		                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> -->
		            </div>
		        </div>
		        <!-- /.modal-content -->
		    </div>
		    <!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
		        
		        
		    </div>
		  
        
        <label style="display:none" id="idHipotesis">${hipotesisDto.id}</label>
		<label style="display:none" id="idProyecto">${idProyecto}</label>
		<label style="display:none" id="idExperimento">${idExperimento}</label>

	</div>
	
	

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.js"></script>


	<!-- SweetAlert2 --> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ws/endpoint.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/loginService.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/proyectoService.js"></script> 
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/menu.js"></script>
	<script src="https://unpkg.com/complex-js@5.0.0/dst/complex.min.js"></script>
<!-- 	<script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script> -->
	<script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>

      
      
      <script>
          $(document).on( 'click', '.salir', function(){
            sessionStorage.removeItem('user');
            window.location.href = '/login';
          })
        </script>
      <script>

      
        window.onload = function() {       

         	var medicionesJson = '${mediciones}';
        	var mediciones = JSON.parse(medicionesJson);

        	var metricasJson = '${metricasObject}';
        	var metricas = JSON.parse(metricasJson);

			var colores = new Array(7);
			colores[0] = "#F08080";
			colores[1] = "#054740";
			colores[2] = "#c10037";
			colores[3] = "#4caf50";
			colores[4] = "#f75431";
			colores[5] = "#8B4516";

			var formas= new Array(7);
			formas[0] = "square";
			formas[1] = "circle";
			formas[2] = "triangle";
			formas[3] = "cross";
			formas[4] = "square";
			formas[5] = "circle";


			//list_totalizado
			
			var liCabecera = "";
			var liTotal = "";
			
        	var data = [];
        	var metricasTittle = "";
        	var i = 0;
			metricas.forEach(function(metrica) {  
				var idMetricaActual = metrica.id;
				var dataPoint = [];
				var nombre = metrica.nombreCorto;
				var color = colores[i];
				var linea = formas[i++];


				
				
				metricasTittle = metricasTittle.concat(nombre+' vs ');
				var suma = 0;
				mediciones.forEach(function(obj) {  
	        		
					var mesNro = obj.mes;
					var dias = obj.dias;
					dias.forEach(function(dia){
						var diaNro = dia.dia;

						var mediciones = dia.mediciones;
						
						mediciones.forEach(function(medicion){
							var valor = medicion.valor;
							
							if(medicion.idMetrica == idMetricaActual){
								suma = suma + valor;
								dataPoint.push({ x: new Date(2020, mesNro-1, diaNro), y: valor });
							}
						});
					});

					
	            });

	            var identificador = 'L'+idMetricaActual;

				liTotal = liTotal.concat('<li codigo='+identificador+' suma='+suma+'>');
				liTotal = liTotal.concat('<div class="courses-container">');
				liTotal = liTotal.concat('<div class="course">');
				liTotal = liTotal.concat('<div class="course-preview">');
				liTotal = liTotal.concat('<h6>Métrica</h6>');
				liTotal = liTotal.concat('<h2>'+nombre+'</h2>');
				liTotal = liTotal.concat('<h2>'+identificador+'</h2>');
// 				liTotal = liTotal.concat('<a href="#">Ver detalle <i class="fas fa-chevron-right"></i></a>');
				liTotal = liTotal.concat('</div>');
				liTotal = liTotal.concat('<div class="course-info">');

				liTotal = liTotal.concat('<h2>'+suma+'</h2>');
				liTotal = liTotal.concat('</div></div></div></li>');


				data.push({type: "line", 
					showInLegend: true, 
					name: nombre, 
					markerType: linea,
					xValueFormatString: "DD MMM, YYYY",
					color: color,
					yValueFormatString: "#,##0",
					dataPoints: dataPoint});
				
			});

			
			$('#list_totalizado').append(liTotal);
			
        	var options = {
    				animationEnabled: true,
    				theme: "light2",
    				title:{
    					text: metricasTittle
    				},
    				axisX:{
    					valueFormatString: "DD MMM"
    				},
    				axisY: {
    					title: "Cantidad",
    					suffix: "",
    					minimum: 5
    				},
    				toolTip:{
    					shared:true
    				},  
    				legend:{
    					cursor:"pointer",
    					verticalAlign: "top",
    					horizontalAlign: "left",
    					dockInsidePlotArea: true,
    					itemclick: toogleDataSeries
    				},
    				data: data
    			};
    			$("#chartContainer").CanvasJSChart(options);
    	
    			function toogleDataSeries(e){
    				if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
    					e.dataSeries.visible = false;
    				} else{
    					e.dataSeries.visible = true;
    				}
    				e.chart.render();
    			}

        	setProgressBar(4);


			fillResultado();
        	
        	
        	
			
	 	}

	
		function fillResultado(){

			var listaCondi = $("#listaCondiciones li");
        	

			var resultado2 = "";
	    	listaCondi.each(function( index, li ) {
				console.log('Resultad ' + resultado2);
				var condiTxt = $(li).attr('nombre');
				resultado2 = resultado2.concat('<li>');
				var txtCondicionHtml = condiTxt.replace("<", "&#60;");
				resultado2 = resultado2.concat('<p>'+txtCondicionHtml+'</p>');
								
				var liTotal = $('#list_totalizado li');
				liTotal.each(function( index, li ) {
					var identificador = $(li).attr('codigo');
					var valor = $(li).attr('suma');
					condiTxt = condiTxt.replace(identificador, valor);
				 });
	
			    var operador="";
				var opera = 0;
				var expresion ="";
				var esperado="";
				if (condiTxt.includes("=")) {
					operador="=";
					opera=1;						
				}else if(condiTxt.includes("<")) {
					operador="<";
					opera=2;
				}else if(condiTxt.includes(">")) {
					operador=">";
					opera=3;
				}
				var indexOperador = condiTxt.indexOf(operador);
				expresion = condiTxt.substring(0, indexOperador);
				esperado= condiTxt.substring(indexOperador+1, condiTxt.length);
				expresion = expresion.replace("%", "/100");
			    if(isMathExpression(expresion)){
					var resultado = eval(expresion);
					var bool = false;
					if(opera==1){
						if(resultado == esperado){
							bool = true;
							
					    }
					}else if(opera==2){
						if(resultado < esperado)
							bool = true;
						
					}else if(opera==3){
						if(resultado > esperado)
							bool = true;
						
					}
					
					if(bool){
						resultado2 = resultado2.concat('<p>BIEN</p>');

					}else{
						resultado2 = resultado2.concat('<p>MAL</p>');

					}
				}else{
					resultado2 = resultado2.concat('<p>ERROR</p>');

				}	    	
			    resultado2 = resultado2.concat('</li>');
			});
	
	    	$('#list_condiciones').append(resultado2);

		}

	 	function setProgressBar(curStep){
	        var percent = parseFloat(100 / 4) * curStep;
	        percent = percent.toFixed();
	        $(".progress-bar")
	        .css("width",percent+"%")
    	}

	 	function isMathExpression (str) {
			  try {
			    Complex.compile(str);
			  } catch (error) {
			    return false;
			  }

			  return true;
		}

	 	function open_modal_comments(){
			$("#modalComment").modal();
		 }

	 	$(document).on('click', '#btnResuelta', function() {
			 var liCurrent = $(this).closest('li');
			 var idComentario = $(liCurrent).attr('idComentario');
			 updateEstadoComentarioDetalleWs(idComentario, 2);
			 console.log(idComentario);
			 $(this).closest('li').remove();
		 });

	 	function saveComentario(){
			var comentario = $("#txtComentario").val();
			var idExperimento = $("#idExperimento").text();
			var payload = {comentario, idExperimento};
			saveComentarioExperimento(payload);
			var newComentario = "";
			newComentario = newComentario.concat('"<li class="item"><span>'+comentario+'</span></li>');

			$("#lista_comentario").append(newComentario);
	     }

		
      </script>
</body>
</html>