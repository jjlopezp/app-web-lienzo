<%@ taglib prefix="c" uri="http://www.hdiv.org/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Lienzanier</title>
	<link href='http://fonts.googleapis.com/css?family=Headland+One' rel='stylesheet' type='text/css'>
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/canvas.css">
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/menu.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/steps.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/mobiscroll.jquery.min.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/checkbox.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">    
    <link href="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.css" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/deltoss/d3-mitch-tree@1.0.2/dist/css/d3-mitch-tree.min.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/d3-mitch-tree-default.min.css">
    
    <style>
    	label{
    		font-size: 1.5rem;
    	}
    	
    	#main2::after{
    		content: "";
/* 			background-image: url('${pageContext.request.contextPath}/resources/img/creaMideAprende.png');    	 */
    		opacity: 0.5;
			  top: 0;
			  left: 0;
			  bottom: 0;
			  right: 0;
			  position: absolute;
			  z-index: -1; 
    	}
    	
    	.edit-hipotesis-container{
    		border: 1px solid black;
		    width: 80%;
		    margin: 30px auto !important;
		    background-color: antiquewhite;
		    border-radius: 50px;
		    display: flex;
    	}
    	
    	
    	#tableAreas_wrapper{
    		width: 100% !important;
    	}
    	
    	.info-derecha{
/*     		border-left: 1px solid black; */
		    width: 30% !important;
    		margin: 0 5% !important;
/* 		    border-radius: 50px; */
    	}
    	
    	.first-element{
    		margin-top: 16rem;
    	}
    	
    	.info-left{
    		width: 50% !important;
    		margin: 0 5% !important;
    	}
    	
    	.flex-derecha{
    		display: flex;
			flex-wrap: wrap;
			margin-bottom: 16px;
			word-break: break-word;
    	}
    	
/*     	h2{ */
/* /*     		color: black !important; */ */
/* 			font-size: 2rem !important; */
/* 			text-align: center !important; */
/* 			display: block !important; */
/*     	} */
    	
    	
    	
    	
    	.col-7{
            margin-top: 5px;
        }

        input{
            text-align: center;
        }

        #heading{
            text-align: center;
        }
        #heading + p{
            text-align: center;
        }
        .col-10 p{
            font-size: 24px;
            text-align: center !important;
        }

        .lienzo{
            border: 2px solid #4852a0;
            margin: 1px;
        }
        
        .modal-content.arbol {
    width: 900px;
    padding: 20px;
}


#contentArbol.scrollbar {
	margin-left: 0px;
    height: 800px;
    width: 100%;
    background: #FFF;
    overflow-y: scroll;
    margin-bottom: 25px;
}
    	
    </style>
</head>
<body>
	<header>
		<jsp:include page='header.jsp'/>
	</header>
	<div id="main2">
	
		
		<div class="container-fluid">
        <!--<div class="row justify-content-center">
            <div class="col-11 col-sm-10 col-md-10 col-lg-6 col-xl-5 text-center p-0 mt-3 mb-2">
                <div class="card px-0 pt-4 pb-0 mt-3 mb-3">-->
        <div class="">
                <div class="">
                    <div class="">                      

                    <h2 id="heading">¿Estás listo para experimentar?</h2>
                    <p>Da el siguiente paso</p>
                    <form id="msform">
                        <!-- progressbar -->
                        <ul id="progressbar">
                            <li class="active" id="account"><strong>Tu hipótesis</strong></li>
                            <li id="personal"><strong>¿Listo para experimentar?</strong></li>
                            <li id="payment"><strong>Estamos en marcha</strong></li>
                            <li id="confirm"><strong>Finalizar</strong></li>
                        </ul>
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                        </div> <br> <!-- fieldsets -->
                        <fieldset>
                                
                            <div class="form-card">
                                <div class="row">
                                    <div class="col-10">
                                        <h2 class="fs-title">¿Que piensas?</h2>
                                        <textarea id="txtEditSuposicion">${hipotesisDto.suposicion}</textarea>
                                    </div>
                                    <div class="col-10">
                                            <h2 class="fs-title">¿De que trata?</h2>
                                            <textarea id="txtDescripcionEdit">${hipotesisDto.descripcion}</textarea>
                                    </div>

									<div class="col-10">
                                            <h2 class="fs-title">Seleccione hipótesis antecesor</h2>
                                            <button type="button" onClick="open_modal_arbol()" class="swal2-confirm swal2-styled" style="background-color:#9e1b32;float: right;"><span id="btnTxtPadre">No ha seleccionado ninguna hipótesis aún</span></button>                       
                                    </div>


                                    <div class="col-10">
                                            <h2 class="fs-title">Fecha creación</h2>
                                            <p class="fieldlabels">${fechaRegistro}</p>
                                    </div>
                                    <div class="col-10">
                                            <h2 class="fs-title">¿Que área pertenece?</h2>
                                            <div class="flex-lienzo">
                                                <div id="sociosClave" class="lienzo">
                                                    Socios clave
                                                    
                                                    <div style="padding-top: 65px;padding-left: 15px;">
                                                    	<c:out value="${sociosClave}" escapeXml="false"/>
                                                    	   
                                                    </div>
                                                    
                                                </div>
                                                <div id="col-two">
                                                    <div id="actividadesClave" class="lienzo">
                                                            Actividades clave  
                                                            <div style="padding-top: 50px; padding-left: 50px;">
	                                                    	    <c:out value="${actividadesClave}" escapeXml="false"/>
		                                                    </div>  
                                                    </div>
                                                    <div id="recursosClave" class="lienzo">
                                                            Recursos clave   
                                                            <div style="padding-top: 50px; padding-left: 50px;">
		                                                    	 <c:out value="${recursosClave}" escapeXml="false"/>   
		                                                    </div> 
                                                    </div>
                                                </div>
                                                <div id="propuestaValor" class="lienzo">
                                                        Propuesta valor
                                                        <div style="padding-top: 110px; padding-left: 145px;">
	                                                    	<c:out value="${propuestaValor}" escapeXml="false"/>     
	                                                    </div>    
                                                </div>
                                                <div id="col-two">
                                                    <div id="relacionClientes" class="lienzo">
                                                            Relación clientes
                                                            <div style="padding-top: 50px; padding-left: 50px;">
		                                                    	<c:out value="${relacionClientes}" escapeXml="false"/>   
		                                                    </div>    
                                                    </div>
                                                    <div id="canales" class="lienzo">
                                                            Canales
                                                            <div style="padding-top: 50px; padding-left: 50px;">
		                                                    	<c:out value="${canales}" escapeXml="false"/>  
		                                                    </div>    
                                                    </div>
                                                </div>
                                                <div id="segmentacionClientes" class="lienzo">
                                                        Segmentación clientes    
                                                        <div style="padding-top: 50px; padding-left: 20px;">
	                                                    	<c:out value="${segmentosClientes}" escapeXml="false"/>    
	                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="flex-lienzo">
                                            	<div id="estructuraCostos" class="lienzo">
                                            		Estructura de costos
                                            		<div style="padding-top: 80px; padding-left: 190px;">
                                                    	<c:out value="${estructuraCostos}" escapeXml="false"/>    
                                                    </div>
                                            	</div>
                                            	<div id="fuentesIngreso" class="lienzo">
                                            		Fuentes ingreso
                                            		<div style="padding-top: 80px; padding-left: 190px;">
                                                    	<c:out value="${fuentesIngreso}" escapeXml="false"/>    
                                                    </div>
                                            	</div>
                                            </div>
                                    </div>
                                </div> 
                                                       
                

                            </div> 
                            <button type="button" onClick="cofirmarEditHipotesis()" class="swal2-confirm swal2-styled" style="background-color:#9e1b32;float: right;">Crear Experimento</button>
                        </fieldset>
            			<label style="display:none" id="idHipotesis">${hipotesisDto.id}</label>
						<label style="display:none" id="idProyecto">${idProyecto}</label>
                    </form>
                </div>
            </div>
        </div>
    </div>
		
		<div class="modal fade" id="modalArbol" role="dialog" style="height: auto;">
             <div class="modal-dialog">
                 <div class="modal-content arbol">
                 	<div class="modal-header">
                         <div>
                             <label>Hipótesis seleccionada: </label>
                             <label id="hipotesisPadreTxt">No se ha seleccionado</label>
                             <label id="idHipoPadre" style="display:none">0</label>                            
                        	<button type="button" onClick="limpiarPadre()" class="swal2-confirm swal2-styled" style="background-color:#9e1b32;float: right;">Limpiar</button>
                        	<button type="button" onClick="actualizarPadre()" class="swal2-confirm swal2-styled" style="background-color:#9e1b32;float: right;">Aceptar</button>
                         </div>
                     </div>
                     <div id="contentArbol" class="modal-body pop-inner scrollbar">
                     

						
                     </div>                                    
                 </div>
             </div>
         </div>
		
		
		
	</div>
	
	
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.js"></script>

	<!-- SweetAlert2 --> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ws/endpoint.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/loginService.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/proyectoService.js"></script> 
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/menu.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/steps.js"></script>
      <script src="https://cdn.jsdelivr.net/gh/deltoss/d3-mitch-tree@1.0.2/dist/js/d3-mitch-tree.min.js"></script>
<%--       <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/mobiscroll.jquery.min.js"></script> --%>
      <script>
          $(document).on( 'click', '.salir', function(){
            sessionStorage.removeItem('user');
            window.location.href = '/login';
          });
          
          mobiscroll.settings = {
			    theme: 'ios',
			    themeVariant: 'light'
			};


          $(function () {       	    


        	    $('#demo-autocomplete').mobiscroll().select({
        	        display: 'center',
        	        //data: names,
        	        filter: true,
        	        placeholder: 'Selecciona al responsable',
        	        filterPlaceHolderText: 'Filtra aquí'
        	    });

        	   

        	});


			
        </script>
      <script>
        window.onload = function() {     

        	setProgressBar(1);
                
        	var tableAreas = $('#tableAreas').DataTable({
				searching: false,
				ordering: false,
				bInfo: false,
			    paging: false
			} );
			  
		    $('#tableAreas tbody').on( 'click', 'tr', function () {
		       $(this).toggleClass('selected');
		    });
	 	}

	 	function cofirmarEditHipotesis(){
		 	var idHipotesis = $("#idHipotesis").text();
		 	var idProyecto = $("#idProyecto").text();

			var descripcion = $("#txtDescripcionEdit").val();
			var suposicion = $("#txtEditSuposicion").text();
			var duracion = $("#txtEditTiempo").val();

			
			var proyectoAreas = []
		    
			var i;

			if($('#check_sociosClave').is(':checked')){
				proyectoAreas.push({"id": + $('#check_sociosClave').attr('idproyectoarea')});
			}
			if($('#check_actividadesClave').is(':checked')){
				proyectoAreas.push({"id": + $('#check_actividadesClave').attr('idproyectoarea')});
			}
			if($('#check_recursosClave').is(':checked')){
				proyectoAreas.push({"id": + $('#check_recursosClave').attr('idproyectoarea')});
			}
			if($('#check_propuestaValor').is(':checked')){
				proyectoAreas.push({"id": + $('#check_propuestaValor').attr('idproyectoarea')});
			}
			if($('#check_relacionClientes').is(':checked')){
				proyectoAreas.push({"id": + $('#check_relacionClientes').attr('idproyectoarea')});
			}
			if($('#check_canales').is(':checked')){
				proyectoAreas.push({"id": + $('#check_canales').attr('idproyectoarea')});
			}
			if($('#check_segmentacionClientes').is(':checked')){
				proyectoAreas.push({"id": + $('#check_segmentacionClientes').attr('idproyectoarea')});
			}
			if($('#check_estructuraCostos').is(':checked')){
				proyectoAreas.push({"id": + $('#check_estructuraCostos').attr('idproyectoarea')});
			}
			if($('#check_fuentesIngreso').is(':checked')){
				proyectoAreas.push({"id": + $('#check_fuentesIngreso').attr('idproyectoarea')});
			}

			var editHipotesis = {
				id: idHipotesis,
				idProyecto: idProyecto,
				suposicion: suposicion,
				descripcion: descripcion,
//	 					publico: publico,
				duracion: duracion,
				proyectoAreas: proyectoAreas,
				idHipotesisPadre: $('#idHipoPadre').text()
			}
// 			console.log(editHipotesis);
			editarHipotesisWS(editHipotesis);



			
		 }

		 function confirmEditarSuposicion(){
			 $("#txtEditSuposicion").text($('#txtEditSupo').val());
			 $('#modal-edit-suposicion').modal('toggle');
			}

		function crearExperimento(){
			var idHipotesis = $("#idHipotesis").text();
			var idProyecto = $("#idProyecto").text();
			window.location.href="/experimento?id="+idProyecto+"&idHipotesis="+idHipotesis;
		}


		function sociosClave(){
			$("#modal-socios-clave").modal();
		}

		function actividadesClave(){
			$("#modal-actividades-clave").modal();
		}

		function recursosClave(){
			$("#modal-recursos-clave").modal();
		}

		function propuestaValor(){
			$("#modal-propuesta-valor").modal();
		}

		function reclacionClientes(){
			$("#modal-relacion-clientes").modal();
		}

		function canales(){
			$("#modal-canales").modal();
		}

		function segmentosClientes(){
			$("#modal-segmentos-clientes").modal();
		}

		function estructuraCostos(){
			$("#modal-estructura-costos").modal();
		}

		function fuentesIngreso(){
			$("#modal-fuentes-ingreso").modal();
		}

		function openModalInfo(){
			$('#list-info a:nth-child(1)').tab('show') // Select first tab
			$("#modal-info-lienzo").modal();
		}

		$('#list-info a').on('click', function (e) {
			  e.preventDefault()
			  $(this).tab('show')
			})
		 
	 	$(document).on( 'click', '.editSupo', function(){
              
	 		$('#txtEditSupo').val($("#txtEditSuposicion").text());
            $("#modal-edit-suposicion").modal();
        })
        
        function setProgressBar(curStep){
	        console.log(curStep);
	        var percent = parseFloat(100 / 4) * curStep;
	        percent = percent.toFixed();
	        $(".progress-bar")
	        .css("width",percent+"%")
    	}

		function open_modal_arbol(){
			 $("#modalArbol").modal();
			 
			}

		function actualizarPadre(){
			$('#modalArbol').modal('toggle');
				$('#btnTxtPadre').text($('#hipotesisPadreTxt').text());
		}

		function limpiarPadre(){
			$('#modalArbol').modal('toggle');
			$('#btnTxtPadre').text('No ha seleccionado ninguna hipótesis aún');
			$('#idHipoPadre').text('0');
		}
      </script>
      
      <script>
			function getData() {

				var arbolJson = '${arbolObject}';
	        	var nodos = JSON.parse(arbolJson);
				
				console.log(nodos);
	        	var data = nodos;


				return data;
			}

			
			var datas = getData();
			var index=0;
			datas.forEach(function(data) {  
				console.log(data);
				var idNuevo = "visualisation"+index;
				$("#contentArbol").append("<section id='"+idNuevo+"' style='border: 1px black solid; max-width: 1100px;'> </section>");
				
				var treePlugin = new d3.mitchTree.boxedTree()
				.setData(data)
				.setAllowFocus(true)
				.setElement(document.getElementById(idNuevo))
				.setIdAccessor(function(data) {
					return data.id;
				})
				.setChildrenAccessor(function(data) {
					return data.children;
				})
				.setBodyDisplayTextAccessor(function(data) {
					return data.description;
				})
				.setTitleDisplayTextAccessor(function(data) {
					return data.name;
				})
				.on("nodeClick", function(event) {
					console.log('The event object:')
					console.log(event);
					console.log("Click event was triggered!");
					console.log(event.data.description);
					var idHipotesisPadre = event.data.id;
					var hipotesis = event.data.description;
					$("#hipotesisPadreTxt").text(hipotesis);
					$("#idHipoPadre").text(idHipotesisPadre);
					// Note for 'collapse' or 'expand' event type
					// to trigger, you'll need to disable focus mode.
					// You can do this by passing in false for the
					// allowFocus setting.
					if (event.type == 'focus')
						console.log("Node is being focused");
					else if (event.type == 'collapse')
						console.log("Node is collapsing");
					else if (event.type == 'expand')
						console.log("Node is expanding");
					// You use the below line to cancel the
					// focus/expand/collapse event
// 					  event.preventDefault();
				})
				.initialize();
			
				// Expand all nodes
				var nodes = treePlugin.getNodes();
				nodes.forEach(function(node, index, arr) {
					treePlugin.expand(node);
				});
				treePlugin.update(treePlugin.getRoot());

				index = index+1;
			});
			
		</script>
</body>
</html>