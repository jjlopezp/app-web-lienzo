<%@ taglib prefix="c" uri="http://www.hdiv.org/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Lienzanier</title>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/menu.css">
    <!-- bootstrap css -->
<%--       <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"> --%>
      <!-- style css -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style_theme.css">
      <!-- Responsive-->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/responsive.css">
      <!-- fevicon -->
      <link rel="icon" href="${pageContext.request.contextPath}/resources/img/fevicon.png" type="image/gif" />
      <!-- Scrollbar Custom CSS -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery.mCustomScrollbar.min.css">
    <style>
    	
    </style>
</head>
<body>


	<header>
         <!-- header inner -->
         <div class="container-fluid">
            <div id="rowHeader" class="row">
               <div class="col-lg-3 logo_section">
                  <div class="full">
                     <div class="center-desk">
                        <div class="logo"> <a href="/home?id=${idProyecto}"><img class="head" src="${pageContext.request.contextPath}/resources/img/logo.png" alt="#"></a> </div>
                     </div>
                  </div>
               </div>
               <div class="col-lg-9">
                  <div class="menu-area">
                     <div class="limit-box">
                        <nav class="main-menu">
                           <ul class="menu-area-main">
                              <li id="milienzo">
                                 <a href="/home?id=${idProyecto}">Mi Lienzo</a>
                              </li>
                              <li id="metricas">
                                 <a href="/metrica?id=${idProyecto}">Métricas</a>
                              </li>
                              <li id="indicadores">
                                 <a href="/indicador/listar?id=${idProyecto}">Indicadores</a>
                              </li>
                              <li id="historial">
                              	<a href="/historial?id=${idProyecto}">Historial</a>
                              </li>
                              <li id="miembros">
                                 <a href="/lienzo/miembros?id=${idProyecto}">Mi proyecto</a>
                              </li>                             
                              <li id="miperfil">
                                 <a href="/miperfil?id=${idProyecto}">Mi perfil</a>
                              </li>                              
                              <li id="">
                                 <a href="/proyecto/listar">Regresar a proyectos</a>
                              </li>
                              <li>
	                          	<i class="salir fas fa-sign-out-alt fa-2x"></i>
	                          </li>
                           </ul>
                        </nav>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- end header inner -->
      </header>

	
		

</body>
</html>