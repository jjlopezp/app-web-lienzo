<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Lienzanier</title>

<!-- bootstrap css -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
      <!-- style css -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style_theme.css">
      <!-- Responsive-->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/responsive.css">
      <!-- fevicon -->
      <link rel="icon" href="${pageContext.request.contextPath}/resources/img/fevicon.png" type="image/gif" />
      <!-- Scrollbar Custom CSS -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery.mCustomScrollbar.min.css">


</head>
<body>
	<header>
         <!-- header inner -->
         <div class="container-fluid">
            <div class="row">
               <div class="col-lg-3 logo_section">
                  <div class="full">
                     <div class="center-desk">
                        <div class="logo"> <a href="/login"><img src="${pageContext.request.contextPath}/resources/img/logo.png" alt="#"></a> </div>
                     </div>
                  </div>
               </div>
               <div class="col-lg-9">
                  <div class="menu-area">
                     <div class="limit-box">
                        <nav class="main-menu">
                           <ul class="menu-area-main">
                              <li id="inicio">
                                 <a href="/login">Inicio</a>
                              </li>
                              <li id="articulos">
                                 <a href="#articulos">Artículos</a>
                              </li>
                              <li id="comoUsar">
                                 <a href="/tutorial">Como usar</a>
                              </li>                             
                              <li id="login">
                                 <a href="/inicia">Login</a>
                              </li>
                              <li id="registrarse">
                                 <a href="/registro/usuario">Registrarse</a>
                              </li>
<!--                               <li> -->
<%--                                  <a href="#"><img src="${pageContext.request.contextPath}/resources/img/search_icon.png" alt="#" /></a> --%>
<!--                               </li> -->
                           </ul>
                        </nav>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- end header inner -->
      </header>
</body>
</html>