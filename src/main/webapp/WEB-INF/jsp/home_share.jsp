<%@ taglib prefix="c" uri="http://www.hdiv.org/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Lienzanier</title>
	<link href='http://fonts.googleapis.com/css?family=Headland+One' rel='stylesheet' type='text/css'>
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/canvas.css">
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/menu.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/sticky.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">    
    <link href="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.css" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
 <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <style>
    
    	.options-home{
    		margin-bottom: 15px;
    	}
    
    	p > a{
    		color: white;
   			font-size: 0.8rem;
    	}
    	
    	.hipo{
    		width: 100px !important;
		    height: auto !important;
		    border-radius: 15px;
		    padding: 3px;
    	}
    	
		
		.search {
		  width: 100%;
		  position: relative;
		  display: flex;
		}
		
		.searchTerm {
		  width: 100%;
		  border: 3px solid #00B4CC;
		  border-right: none;
		  padding: 5px;
		  height: 20px;
		  border-radius: 5px 0 0 5px;
		  outline: none;
		  color: #9DBFAF;
		}
		
		.searchTerm:focus{
		  color: #00B4CC;
		}
		
		.searchButton {
		  width: 40px;
		  height: 36px;
		  border: 1px solid #00B4CC;
		  background: #00B4CC;
		  text-align: center;
		  color: #fff;
		  border-radius: 0 5px 5px 0;
		  cursor: pointer;
		  font-size: 20px;
		}
		
		.float{
	position:fixed;
	width:150px;
	height:80px;
	bottom:10px;
	right:40px;
	background-color:#0C9;
	color:#FFF;
	border-radius:50px;
	text-align:center;
	box-shadow: 2px 2px 3px #999;
}

.my-float{
	margin-top:22px;
}

.comentario{
	max-width: 1000px !important;
    max-height: 800px !important;
    height: 800px !important;
}

    </style>
    
</head>
<body>

	<header>
		<jsp:include page='header_share.jsp'/>
	</header>
	
	<div id="main2">

		<div class="options-home">
			
			<div class="flex-vertical">
				<h3>Hipotesis</h3>
				<div class="btn-group">
					<button type="button" class="btn btn-primary btn-filtro"><a href="/share/hipotesis/${idProyecto}">Sin experimento</a></button>
				</div>
			</div>
			<div class="flex-vertical">
				<h3>Experimentos</h3>
				<div class="btn-group">
				  <button type="button" class="btn btn-primary btn-filtro"><a href="/share/experimento?id=${idProyecto}&estado=3">Sin iniciar</a></button>
				  <button type="button" class="btn btn-primary btn-filtro"><a href="/share/experimento?id=${idProyecto}&estado=4">En curso</a></button>
				  <button type="button" class="btn btn-primary btn-filtro"><a href="/share/experimento?id=${idProyecto}&estado=5">Finalizado</a></button>
				</div>
				
			</div>

			
		</div>
			
		<div class="container-canvas">
	      <!-- Canvas -->
	      <table id="bizcanvas" class="table-lienzo" cellspacing="0" border="1">
	        <!-- Upper part -->
	        <tr>
	          <td class="sociosclaves" colspan="2" rowspan="2">

	            	<c:out value="${sociosClave}" escapeXml="false"/>
	          </td>
	          <td class="actividadesClave" colspan="2">
	            	
	            	<c:out value="${actividadesClave}" escapeXml="false"/>
	          </td>
	          <td class="propuestaValor" colspan="2" rowspan="2">
	          		
	           		<c:out value="${propuestaValor}" escapeXml="false"/>
	          </td>
	          <td class="relacionClientes" colspan="2">
	          		
	            <c:out value="${relacionClientes}" escapeXml="false"/>
	          </td>
	          <td class="segmentosClientes" colspan="2" rowspan="2">
	          
	          		
	            <c:out value="${segmentosClientes}" escapeXml="false"/>
	          </td>
	        </tr>
	
	        <!-- Lower part -->
	        <tr>
	          <td class="recursosClave" colspan="2">
	          		
	            <c:out value="${recursosClave}" escapeXml="false"/>
	          </td>
	          <td class="canales" colspan="2">
	          		
	            <c:out value="${canales}" escapeXml="false"/>
	          </td>
	        </tr>
	        <tr>
	          <td class="estructuraCostos" colspan="5">
	          		
	            <c:out value="${estructuraCostos}" escapeXml="false"/>
	          </td>
	          <td class="fuentesIngreso" colspan="5">
	          		
	            <c:out value="${fuentesIngreso}" escapeXml="false"/>
	          </td>
	        </tr>
	      </table>
	      <!-- /Canvas -->
	    </div>
	    <br>
	    <br>
	    

         
<%-- 		<label style="display:none" id="nombreProyecto">${nombreProyecto}</label> --%>
		<label style="display:none" id="idProyecto2">${idProyecto}</label>
       
    
    	<div class="float" onClick="open_modal_comments();">
		<i class="far fa-comment my-float fa-4x"></i>
	  </div>
       
       <!--Modal comentarios-->
       <div id="modalComment" class="modal fade" role="dialog">
		    <div class="modal-dialog comentario">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                 <h4 class="modal-title"></h4>  
		
		            </div>
		            <div class="modal-body">

		                
		                <div class="flex-column scrollbar-comentario" id="style-2">
		                
		                	<ul id="lista_comentario" class="list">
		                		<c:out value="${comentariosHtml}" escapeXml="false"/>
		                		
		                	</ul>
		                </div>
		               
		            </div>
		            <div class="modal-footer">
		            	<textarea id="txtComentario" class="form-control" placeholder="Escribe tu comentario..."></textarea> 
		            	<button id="loadpage" onClick="saveComentario();" type="button" class="btn btn-primary">Enviar</button>
		            </div>
		        </div>
		        <!-- /.modal-content -->
		    </div>
		    <!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
		
    
	</div>
	
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.js"></script>

	<!-- SweetAlert2 --> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ws/endpoint.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/loginService.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/proyectoService.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ws/endpoint.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/canvas.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/menu.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
	<script>
          $(document).on( 'click', '.salir', function(){
            sessionStorage.removeItem('user');
            window.location.href = '/login';
          })
        </script>
        
	<script>
		window.onload = function() {      

		 }

		function open_modal_comments(){
			 console.log('etntra opren_modal');
			$("#modalComment").modal();
		 }

		 function saveComentario(){
			var comentario = $("#txtComentario").val();
			console.log(comentario);
			var idProyecto = $("#idProyecto2").text();
			var payload = {comentario, idProyecto};
			saveComentarioProyecto(payload);
			var newComentario = "";
			newComentario = newComentario.concat('"<li class="item"><span>'+comentario+'</span></li>');

			$("#lista_comentario").append(newComentario);
	     }
		
	</script>
	
</body>
</html>