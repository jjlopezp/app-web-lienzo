<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Lienzanier</title>
<meta name="google-signin-client_id" content="947699620439-f0h77pkjt32as9f0ub6f773lfkvfr1ch.apps.googleusercontent.com">

<link href='http://fonts.googleapis.com/css?family=Headland+One' rel='stylesheet' type='text/css'>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/cards.css">
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/canvas.css">
<%-- 	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css"> --%>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/menu.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">    
    <link href="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.css" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    
	
    
    
<%-- <script src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script> --%>
	<style>
		
		.w-100{
			height: 560px !important;
		}
		
		.mg-0{
			margin-bottom: 0px !important;
		}
		
		
	</style>
</head>
<body>
	<header>
		<jsp:include page='header-inicio.jsp'/>
	</header>
	<section>
		
<!-- 		<div class="texto-inicial"> -->
<!-- 			<h1>Crea el mejor modelo de negocio para tu empresa</h1>	 -->
<!-- 			<input onclick="openlogin()" type="submit" class="fadeIn fourth empieza" value="Empieza ahora">		 -->
<!-- 		</div> -->
		

		<div class="banner-slider">
         <div class="container-fluid">
            <div class="row">
            	<div class="col-md-7">
            		<div id="slider_main" class="carousel slide" data-ride="carousel">
                     <!-- The slideshow -->
                     <div class="carousel-inner">
                        <div class="carousel-item active">
                           <img src="${pageContext.request.contextPath}/resources/img/milienzo.png" alt="#" />
                        </div>
                        <div class="carousel-item">
                           <img src="${pageContext.request.contextPath}/resources/img/metricas_condiciones.png" alt="#" />
                        </div>
                        <div class="carousel-item">
                           <img src="${pageContext.request.contextPath}/resources/img/agenda.png" alt="#" />
                        </div>
                     </div>
                     <!-- Left and right controls -->
                     <a class="carousel-control-prev" href="#slider_main" data-slide="prev">
                     <i class="fa fa-angle-left" aria-hidden="true"></i>
                     </a>
                     <a class="carousel-control-next" href="#slider_main" data-slide="next">
                     <i class="fa fa-angle-right" aria-hidden="true"></i>
                     </a>
                  </div>
            	
            	
            	</div>
            
            	 <div class="col-md-5">
            	 	<div class="full slider_cont_section">
	                     <h4>Consigue el éxito con</h4>
	                     <h3>Lienzanier</h3>
	                     <p class="mg-0">Lienzanier es una plataforma web para hacer un seguimiento, tener una trazabilidad de tus hipótesis y tus resultados que obtengas!</p>
	                     <p class="mg-0">Crea tus hipótesis</p>
	                     <p class="mg-0">Define cómo y de que manera medir tu hipótesis</p>
	                     <p>Aprende de los resultados y decide</p>	                    
	                    
	                     <div class="button_section">
	                        <a href="/tutorial">Leer más</a>
	                        <a href="/inicia">Empieza de una vez</a>
	                     </div>
                  	</div>
            	 
            	 </div>
            
            </div> 
         </div>
        </div>

	
<!-- 		<input onclick="openlogin()" type="submit" class="fadeIn fourth empieza" value="Empieza ahora">							 -->
		
		<div id="articulos" class="section layout_padding">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="heading">
                     <h3>Artículos de <span class="orange_color">interés</span></h3>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-6">
                  <img src="${pageContext.request.contextPath}/resources/img/lienzo.webp" alt="#" />
               </div>
               <div class="col-md-6">
                  <div class="full blog_cont">
                     <h4>¿Que es lienzo de modelo de negocio?</h4>
                     <h5>Nov 10 2020 5min</h5>
                     <p>
                     	El <strong>Lienzo de Modelo de Negocio</strong> es un resumen de una página que expone al mismo tiempo lo que haces (o quieres hacer), y lo qué debes hacer para 
						lograrlo. Facilitando así una conversación estructurada alrededor de la dirección y la estrategia, 
						poniendo en evidencia las actividades y desafíos cruciales que se relacionan con tu iniciativa y cómo se relacionan entre ellos.
					</p>
                  </div>
                  <div class="row margin_top_30">
	               <div class="col-md-12">
	                  <div class="button_section full center margin_top_30">
	                     <a style="margin:0;" href="/lienzo">Leer más</a>
	                  </div>
	               </div>
	            </div>
               </div>
                
            </div>
            <div class="row margin_top_30">
               <div class="col-md-6" style="text-align: center;">
                  <img src="${pageContext.request.contextPath}/resources/img/creaMideAprende.png" alt="#" />
               </div>
               <div class="col-md-6">
                  <div class="full blog_cont">
                     <h4>¿Que es Lean Startup y por que usarlo?</h4>
                     <h5>Nov 10 2020 5min</h5>
                     <p>
                     	El método  <strong>lean startup</strong> es una metodología basada en “aprendizaje validado”, es decir, ir verificando poco a poco 
						las hipótesis antes de tener el producto final(la startup definitiva) y comenzar a escalar el negocio.
                     </p>
                  </div>
                  <div class="row margin_top_30">
		               <div class="col-md-12">
		                  <div class="button_section full center margin_top_30">
		                     <a style="margin:0;" href="/leanStartup">Leer más</a>
		                  </div>
		               </div>
		            </div>
               </div>
                
            </div>
            
            <div class="row margin_top_30">
               <div class="col-md-6" style="text-align: center;">
                  <img src="${pageContext.request.contextPath}/resources/img/kpi.jpg" alt="#" />
               </div>
               <div class="col-md-6">
                  <div class="full blog_cont">
                     <h4>¿Que son los KPIs y por que es importante?</h4>
                     <h5>Nov 10 2020 15min</h5>
                     <p>
                     	Los KPIS son métricas cuantificables y medibles que identifican el rendimiento en una empresa por medio de la medición 
                     	de determinadas variables, como puede ser el número de visitas, los ingresos o los gastos, entre otras. A la hora de establecer 
                     	las variables a cuantificar (KPIS) estas deben estar directamente relacionadas con los objetivos previamente marcados en la estrategia 
                     	de la Startup para determinar los resultados de la misma.
                     </p>
                  </div>
                  <div class="row margin_top_30">
		               <div class="col-md-12">
		                  <div class="button_section full center margin_top_30">
		                     <a style="margin:0;" href="/kpis">Leer más</a>
		                  </div>
		               </div>
		            </div>
               </div>
                
            </div>
            
<!--             <div class="row margin_top_30"> -->
<!--                <div class="col-md-12"> -->
<!--                   <div class="button_section full center margin_top_30"> -->
<!--                      <a style="margin:0;" href="about.html">Read More</a> -->
<!--                   </div> -->
<!--                </div> -->
<!--             </div> -->
         </div>
      </div>
			
  </section>		
		
		
		<!--Modal crear condición-->
        <div class="modal fade" id="modal-login" role="dialog" style="height: auto;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <div>
                            <label>Bienvenido</label>
                        </div>
                    </div>
                    <div class="modal-body pop-inner">
                       	<div class="wrapper fadeInDown">
					        <div id="formContent">
					          <!-- Tabs Titles -->
					      
					         <!-- Login Form -->
					          
					          <input type="text" id="usuarioid" class="fadeIn second" name="login" placeholder="login">
					          <input type="password" id="passwordid" class="fadeIn third" name="login" placeholder="password">
					          <input onclick="login()" type="submit" class="fadeIn fourth" value="Ingresar">
					          
					          <div style="margin-left: 36%;" class="g-signin2" data-onsuccess="onSignIn"></div>
					          <br>
					          <div>
					            <p>¿No tienes cuenta?</p>
					            <a class="underlineHover" href="/registro/usuario">Registráte!</a>
					          </div>
					          
					        </div>
					      </div>                                   
                    </div>
                    
                </div>
            </div>
        </div>
		
		
		
								
	
		
	
	</section>
	
	 <!-- footer -->
<!--       <footer> -->
<!--          <div class="container"> -->
<!--             <div class="row"> -->
<!--                <div class="col-lg-4 col-md-6"> -->
<%--                   <a href="#"><img src="${pageContext.request.contextPath}/resources/img/footer_logo.png" alt="#" /></a> --%>
<!--                   <ul class="contact_information"> -->
<%--                      <li><span><img src="${pageContext.request.contextPath}/resources/img/location_icon.png" alt="#" /></span><span class="text_cont">London 145<br>United Kingdom</span></li> --%>
<%--                      <li><span><img src="${pageContext.request.contextPath}/resources/img/phone_icon.png" alt="#" /></span><span class="text_cont">987 654 3210<br>987 654 3210</span></li> --%>
<%--                      <li><span><img src="${pageContext.request.contextPath}/resources/img/mail_icon.png" alt="#" /></span><span class="text_cont">demo@gmail.com<br>support@gmail.com</span></li> --%>
<!--                   </ul> -->
<!--                   <ul class="social_icon"> -->
<!--                      <li><a href="#"><i class="fa fa-facebook"></i></a></li> -->
<!--                      <li><a href="#"><i class="fa fa-twitter"></i></a></li> -->
<!--                      <li><a href="#"><i class="fa fa-linkedin"></i></a></li> -->
<!--                      <li><a href="#"><i class="fa fa-google-plus"></i></a></li> -->
<!--                   </ul> -->
<!--                </div> -->
<!--                <div class="col-lg-2 col-md-6"> -->
<!--                   <div class="footer_links"> -->
<!--                      <h3>Quick link</h3> -->
<!--                      <ul> -->
<!--                         <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Home</a></li> -->
<!--                         <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Features</a></li> -->
<!--                         <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Evens</a></li> -->
<!--                         <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Markrting</a></li> -->
<!--                         <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Blog</a></li> -->
<!--                         <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Testimonial</a></li> -->
<!--                         <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Contact</a></li> -->
<!--                      </ul> -->
<!--                   </div> -->
<!--                </div> -->
<!--                <div class="col-lg-3 col-md-6"> -->
<!--                   <div class="footer_links"> -->
<!--                      <h3>Instagram</h3> -->
<!--                      <ol> -->
<%--                         <li><img class="img-responsive" src="${pageContext.request.contextPath}/resources/img/footer_blog.png" alt="#" /></li> --%>
<%--                         <li><img class="img-responsive" src="${pageContext.request.contextPath}/resources/img/footer_blog.png" alt="#" /></li> --%>
<%--                         <li><img class="img-responsive" src="${pageContext.request.contextPath}/resources/img/footer_blog.png" alt="#" /></li> --%>
<%--                         <li><img class="img-responsive" src="${pageContext.request.contextPath}/resources/img/footer_blog.png" alt="#" /></li> --%>
<!--                      </ol> -->
<!--                   </div> -->
<!--                </div> -->
<!--                <div class="col-lg-3 col-md-6"> -->
<!--                   <div class="footer_links"> -->
<!--                      <h3>Contact us</h3> -->
<!--                      <form action="index.html"> -->
<!--                         <fieldset> -->
<!--                            <div class="field"> -->
<!--                               <input type="text" name="name" placeholder="Your Name" required="" /> -->
<!--                            </div> -->
<!--                            <div class="field"> -->
<!--                               <input type="email" name="email" placeholder="Email" required="" /> -->
<!--                            </div> -->
<!--                            <div class="field"> -->
<!--                               <input type="text" name="subject" placeholder="Subject" required="" /> -->
<!--                            </div> -->
<!--                            <div class="field"> -->
<!--                               <textarea placeholder="Message"></textarea> -->
<!--                            </div> -->
<!--                            <div class="field"> -->
<!--                               <div class="center"> -->
<!--                                  <button class="reply_bt">Send</button> -->
<!--                               </div> -->
<!--                            </div> -->
<!--                         </fieldset> -->
<!--                      </form> -->
<!--                   </div> -->
<!--                </div> -->
<!--             </div> -->
<!--          </div> -->
<!--       </footer> -->
<!--       <div class="cpy"> -->
<!--          <div class="container"> -->
<!--             <div class="row"> -->
<!--                <div class="col-md-12"> -->
<!--                   <p>Copyright © 2019 Design by <a href="https://html.design/">Free Html Templates</a></p> -->
<!--                </div> -->
<!--             </div> -->
<!--          </div> -->
<!--       </div> -->
      <!-- end footer -->
	
      
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.js"></script>


	<!-- SweetAlert2 --> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ws/endpoint.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/loginService.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/proyectoService.js"></script> 
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/menu.js"></script>>
      
      
      <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
<%--       <script src="${pageContext.request.contextPath}/resources/js/jquery-3.0.0.min.js"></script> --%>
      <script src="${pageContext.request.contextPath}/resources/js/plugin.js"></script>
      <!-- Scrollbar Js Files -->
      <script src="${pageContext.request.contextPath}/resources/js/jquery.mCustomScrollbar.concat.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/custom.js"></script>
      
      
 	  <script src="https://apis.google.com/js/platform.js" async defer></script>
 	  
 	   
     <script>
 	  
	 	 $(document).ready(function() {
	 	      $("#inicio").addClass("active");
	 	});

 	  
	 	 function openlogin(){
	 		
	 		$("#modal-login").modal();
		 }
 	  </script>
 	  
</body>
</html>