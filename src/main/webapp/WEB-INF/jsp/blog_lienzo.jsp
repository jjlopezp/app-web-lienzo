<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- mobile metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="viewport" content="initial-scale=1, maximum-scale=1">
      <!-- site metas -->
      <title>Lienzanier</title>
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- bootstrap css -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
      <!-- style css -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style_theme.css">
      <!-- Responsive-->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/responsive.css">
      <!-- fevicon -->
      <link rel="icon" href="${pageContext.request.contextPath}/resources/img/fevicon.png" type="image/gif" />
      <!-- Scrollbar Custom CSS -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery.mCustomScrollbar.min.css">
      
      
      <style>
      
      	.card-container {
			background-color: #fafafa;
			border-radius: 5px;
			box-shadow: 0px 10px 20px -10px rgba(0,0,0,0.75);
			color: #B3B8CD;
			padding-top: 30px;
			position: relative;
			width: 350px;
			max-width: 100%;
			text-align: center;
			height: 650px;
		}
		
		
		.card-container .round {
			border: 1px solid #03BFCB;
			border-radius: 50%;
			padding: 7px;
		}
		
		button.primary {
			background-color: #03BFCB;
			border: 1px solid #03BFCB;
			border-radius: 3px;
			color: #231E39;
			font-family: Montserrat, sans-serif;
			font-weight: 500;
			padding: 10px 25px;
		}
		
		button.primary.ghost {
			background-color: transparent;
			color: #02899C;
		}
		
		
		h3 {
			margin: 10px 0 !important;
			font-family: 'Jost', sans-serif !important;;
		    font-size: 1.8rem !important;;
		    line-height: 1.5 !important;;
		}
		
		h6 {
			margin: 5px 0;
			text-transform: uppercase;
		}
		
		h1 {
			font-family: 'Jost', sans-serif !important;
		    font-size: 4.6rem !important;
		    line-height: 1.1 !important;
		    margin: auto !important;
   			 width: fit-content !important;
   			 text-align: center !important;
		}
		
		p {
			font-size: 14px;
			line-height: 21px;
		}
		
		.container-img{
			margin: 0 340px;
		}
		
		.contenido{
			font-size: 24px;
    		line-height: 1.6;
    		font-family: open-sans, Open Sans, Helvetica Neue, Helvetica, Arial, sans-serif;
/*     		color: #555; */
		}
		
		.div-img{
			margin: 25px 0;
		}
		
		.underline-bottom-brand-red {
		    padding-bottom: .5rem!important;
		    border-bottom: .3rem solid #ff352a;
		   
		}
      
      </style>
      
      
</head>
 <body >
     
     <header>
		<jsp:include page='header-inicio.jsp'/>
	</header>
	
	
	<div>
		<h1>Lienzo de modelo de <br>negocio</h1>
	
	</div>
	
	<article id="top">
		<div class="row" style="margin-left: 30%;">
	
			<div class="card-container">
				<img class="round" src="${pageContext.request.contextPath}/resources/img/question.jpeg" alt="user" />
				<h3><strong>¿De que se trata?</strong></h3>
				<p>El Business Model Canvas (BMC) es una herramienta de gestión estratégica para definir y comunicar rápidamente una idea o concepto 
					empresarial. Es un documento de una página que trabaja a través de los elementos fundamentales de un negocio o producto.
				</p>	
				<div class="buttons">
					<a  href="#definicion">
						<button class="primary ghost">
							Ver más
						</button>
					</a>
					
				</div>				
			</div>
			
			<div class="card-container">
				<img class="round" src="${pageContext.request.contextPath}/resources/img/comofunciona.jpeg" alt="user" />
				<h3><strong>¿Cómo se utiliza?</strong></h3>
				<p>El lienzo de divide en 2 lados, en el lado derecho se enfoca en el cliente y el negocio (factores externos) 
				mientras que el lado izquierdo se enfoca en la empresa (factores internos). En el medio se tiene la
				prosición de valor que representa el intercambio entre el valor de tu negocio y el cliente. 
				</p>
				<div class="buttons">	
					<a href="#usar">
						<button  class="primary ghost">
							Ver más
						</button>
					</a>			
					
				</div>
				
			</div>		
			
		
		</div>
	</article>
	
	<div class="container-img">
		<div class="div-img">
			<img src="${pageContext.request.contextPath}/resources/img/modelo-de-negocio-howto.png" alt="#" />
		</div>
		
		<section>
			<p class="contenido" id="definicion">
				El <strong>Lienzo de Modelo de Negocio</strong> es un resumen de una página que expone al mismo tiempo lo que haces (o quieres hacer), y lo qué debes hacer para 
				lograrlo. Facilitando así una conversación estructurada alrededor de la dirección y la estrategia, 
				poniendo en evidencia las actividades y desafíos cruciales que se relacionan con tu iniciativa y cómo se relacionan entre ellos. 
				Este formato visual, introducido originalmente por Osterwalder y Pigneur, es útil tanto para organizaciones y negocios nuevos, como para los ya 
				existentes. Existen programas que pueden desarrollar nuevas iniciativas e identificar oportunidades mientras se vuelven más eficientes ilustrando 
				posibles compensaciones y alineando actividades. Nuevos programas pueden usarla para planear y calcular cómo hacer su oferta real.
			
			</p>
			<br><br><br>
			<h3 class="large underline-bottom-brand-red space-line-bottom">Cómo se utiliza?</h3>
			<p class="contenido" id="usar">
				Para hacer un Lienzo de Modelo de Negocio, la forma más fácil de empezar es anotar lo que se quiere hacer. Esto ayuda a mantener el 
				enfoque en tu meta principal mientras llenas los otros bloques del lienzo. A partir de ahí puedes construir sobre ese objetivo y ver cómo 
				puede ser logrado añadiendo detalles de otras actividades y recursos que tienes. Empieza desde un lienzo en blanco y agrega notas con 
				palabras clave en cada bloque del lienzo. Si usas post it para esto, puedes cambiar las ideas de lugar mientras llenas cada bloque del 
				lienzo.
			
			</p>
			<p>&nbsp;</p>
			<p class="contenido">
				Puede ser que quieras usar colores distintos para los diferentes elementos relacionados a un segmento específico de clientes.
			
			</p>
			<p>&nbsp;</p>
			<p class="contenido">
				Sin embargo, ten cuidado de no enamorarte de tu primera idea y en su lugar boceta alternativas de modelos de negocio para el mismo producto, 
				servicio o tecnología.
			
			</p>
			<p>&nbsp;</p>
			<p class="contenido">
				Puedes incluso practicar y aprender nuevas formas de hacer las cosas planificando nuevos/innovadores modelos de negocio que encuentres 
				o admires.
			
			</p>
		
		
		</section>
		
		<br><br>
		
         <div class="row margin_top_30">
            <div class="col-md-12">
               <div class="button_section full center margin_top_30">
                  <a style="margin:0;" href="#top">Subir</a>
               </div>
            </div>
         </div>
         <br><br>
	</div>
	
	

     
      <!-- end footer -->
      <!-- Javascript files-->
      <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/plugin.js"></script>
      <!-- Scrollbar Js Files -->
      <script src="${pageContext.request.contextPath}/resources/js/jquery.mCustomScrollbar.concat.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/custom.js"></script>
      
       <script>
	     $(document).ready(function() {
		      $("#articulos").addClass("active");
		});
     </script>
   </body>
</html>