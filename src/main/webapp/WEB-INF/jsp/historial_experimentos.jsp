<%@ taglib prefix="c" uri="http://www.hdiv.org/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Lienzanier</title>
<link href='http://fonts.googleapis.com/css?family=Headland+One' rel='stylesheet' type='text/css'>
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/canvas.css">
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/menu.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">    
    <link href="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.css" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    
    
    <style>
    
    	ul li{
    		display: flex;
    	}
    
    	ul li label{
    		width: 260px;
    		text-align: center;
    	}
    	
    	ul li p{
    		width: 650px;
    	}
    	
    	.cabecera{
    		display: flex;
		    padding-left: 460px;
		    margin-bottom: 60px;
		    margin-top: 50px;
    	}
    	
    	.green{
    		color: green;
    	}
    	
    	.red{
    		color: red;
    	}
    	
    	h2{
    		font-size: 18px;
    	}
    </style>
    
</head>
<body>
	<header>
		<jsp:include page='header.jsp'/>
	</header>

	<div id="main2">
		<section class="mr-ri mr-lf">
			<div class="cabecera">
				
				<h2>Experimento</h2>
				<h2 style="margin-left:70px;">Sin iniciar</h2>
				<h2 style="margin-left:70px;">En curso</h2>
				<h2 style="margin-left:65px;">Finalizado</h2>
			</div>
			
			
			<div class="mr-ri">
				
				<ul>
					<c:out value="${historico}" escapeXml="false"/>

				</ul>
			
                
            </div>
           
		</section>
		
		
		<label id="idProyecto" style="display:none;">${idProyecto}</label>
	</div>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.js"></script>

	<!-- SweetAlert2 --> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ws/endpoint.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/loginService.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/proyectoService.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ws/endpoint.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/canvas.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/menu.js"></script>
	<script>
          $(document).on( 'click', '.salir', function(){
            sessionStorage.removeItem('user');
            window.location.href = '/login';
          })
        </script>
	<script>

	$(document).ready(function() {
	      $("#historial").addClass("active");
	});


	$(document).on( 'click', '.iniciarExperimento', function(){
			var idProyecto = $("#idProyecto").text();
			var current_row = $(this).parents('tr');
	        var id = current_row.attr('value');  
			var response = updateEstadoExperimentoWs(id, 2, idProyecto);
			console.log('Llga el metodo');
			console.log(response);
		})


	

	</script>
	
</body>
</html>