<%@ taglib prefix="c" uri="http://www.hdiv.org/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Lienzanier</title>
	<link href='http://fonts.googleapis.com/css?family=Headland+One' rel='stylesheet' type='text/css'>
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/canvas.css">
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/menu.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/steps.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/options.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/tileswrap.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/timeline.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">    
    <link href="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.css" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
    <link href="${pageContext.request.contextPath}/resources/css/froala_style.min.css" rel="stylesheet" type="text/css" />
    
    
    <style>
    	label{
    		font-size: 1.5rem;
    	}
    	
    	#main2::after{
    		content: "";
/* 			background-image: url('${pageContext.request.contextPath}/resources/img/creaMideAprende.png');    	 */
    		opacity: 0.5;
			  top: 0;
			  left: 0;
			  bottom: 0;
			  right: 0;
			  position: absolute;
			  z-index: -1; 
    	}
    	
    	.edit-hipotesis-container{
    		border: 1px solid black;
		    width: 60%;
		    margin: 30px auto !important;
		    background-color: antiquewhite;
		    border-radius: 50px;
		    padding-right: 10rem;
    	}
    	
    	#tablePuntoMedicion2{
    		padding-left: 0 !important;
    	}
    	
    	.info-left{
    		width: 30%;
    		margin: 0 5%;
    	}
    	
    	.info-derecha{
    		width: 50%;
    		margin: 0 5%;
    	}
    	
    	.align-left{
    		text-align: left;
    	}
    	
    	.first-element-derecha{
    	
    		margin-top: 7rem;
    	}
    	
    	.flex-derecha{
    		display: flex;
			flex-wrap: wrap;
			margin-bottom: 16px;
			word-break: break-word;
    	}
    	
    	.detalle-experimento-container{
    		border: 1px solid black;
		    width: 70%;
		    margin: 30px auto !important;
 		    background-color: antiquewhite; 
		    border-radius: 50px;
		    display:flex
    	}
    	
    	.col-4{
    		max-width: 100%;
    	}
    	
    	.col-8{
    		max-width: 100%;
    	}
    	
    	.col-10{
    		flex: 0 0 53.333333% !important;
    		margin-top: 2rem;
    	}
    	
    	.div-timeline ul li{
    		list-style: none;
    	}
    	
    	#boxMetrica, .liCondicion{
    		width: 33.333333%;
    	}
    	
    	.row li{
    		float: left;
    	}
    	
    	.col-sm-4{
    		max-width: 100% !important;
    	}
    	
    	.scrollbar-comentario button{
	    height: fit-content;
    margin-left: 20px;
}

.scrollbar-comentario .item{
	    display: block;
}
	.scrollbar-comentario a{
    margin-left: 20px;
}

.tilesWrap p, .tilesWrap h2, .tilesWrap h3{
	text-align: center;
}

#listaCondiciones p{
	color: #000 !important;
	font-size: 22px !important;
}

#listaCondiciones li{
	background: #55686d !important;
}

#listaMetricas p{
	font-size: 22px !important;
}
.ui-widget.ui-widget-content{
	margin: auto;
}

.box-item p{
	width: fit-content;
	margin: auto;
}

.liCondicion .box-tittle{
	color: #fff;
	font-size: 18px;
}

.float{
	position:fixed;
	width:150px;
	height:80px;
	bottom:10px;
	right:40px;
	background-color:#0C9;
	color:#FFF;
	border-radius:50px;
	text-align:center;
	box-shadow: 2px 2px 3px #999;
}

.my-float{
	margin-top:22px;
}

.comentario{
	max-width: 1000px !important;
    max-height: 800px !important;
    height: 800px !important;
}

    </style>
</head>
<body>
	<header>
		<jsp:include page='header.jsp'/>
	</header>
	<div id="main2">


			<div class="container-fluid">
        <!--<div class="row justify-content-center">
            <div class="col-11 col-sm-10 col-md-10 col-lg-6 col-xl-5 text-center p-0 mt-3 mb-2">
                <div class="card px-0 pt-4 pb-0 mt-3 mb-3">-->
		        <div class="">
		                <div class="">
		                    <div class="">                      
		
		                    <h2 id="heading">Sal del edificio</h2>
		                    <p style="text-align: center">Apunta tus resultados</p>
		                    <form id="msform">
		                        <!-- progressbar -->
		                        <ul id="progressbar">
		                            <li class="active" id="account"><a href="/editar/hipotesis?id=${idHipotesis}&idPro=${idProyecto}&idExperimento=${idExperimento}"><strong>Tu hipótesis</strong></a></li>
		                            <li class="active" id="personal"><a href="/editar/experimento?id=${idHipotesis}&idPro=${idProyecto}&idExperimento=${idExperimento}"><strong>¿Listo para experimentar?</strong></a></li>

		                            <li class="active" id="payment"><strong>Estamos en marcha</strong></li>
		                            <li id="confirm"><strong>Finalizar</strong></li>
		                        </ul>
		                        <div class="progress">
		                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
		                        </div> <br> <!-- fieldsets -->
		                        <fieldset>
		                                
		                            <div class="">
		                                <div class="">
		                                
		                                	<div class="flex-lienzo">
		                                		<div class="col-10">
		                                        	<h2 class="fs-title">¿Que piensas????</h2>
		                                        	<textarea id="txtEditSuposicion" disabled>${suposicion}</textarea>
			                                    </div>
			                                    <div class="col-10">
			                                            <h2 class="fs-title">¿De que trata?</h2>
			                                            <textarea disabled>${descripcion}</textarea>
			                                    </div>
		                                	</div>		                                
		                                	
       
		                                    <div class="col-10">
		                                    	<h2 class="fs-title">¿Cómo validas tu hipótesis?</h2>
		                                    	<div>
		                                    		<h2>${tipoPrueba}</h2>
		                                    	</div>
		                                    	<ul id="listaMetricas" class="tilesWrap">
		                                    		<c:out value="${metricas}" escapeXml="false"/>
		                                    	
												</ul>
												<ul id="listaCondiciones" class="tilesWrap">
		                                    		<c:out value="${condiciones}" escapeXml="false"/>
		                                    	
												</ul>
		                                    	
		                                    	
		                                    	
		                                    	
		                                    	
		                                    	
		                                    </div>
		                                    <div class="col-10">
		                                            <h2 class="fs-title">Anota tus resultados</h2>
		                                            <button type="button" onClick="openModalPunto()" class="swal2-confirm swal2-styled" style="background-color:#9e1b32;float:right">Nuevo punto de medición</button>
		                                            <div class="" id="">
														  
<!-- 											INICIO TIMELINE			   -->
													  <div class="container div-timeline">
<!-- 													    <div class="timeline"> -->
													        <ul id="lista-tiempo"  class="timeline">													            
													            <c:out value="${mediciones}" escapeXml="false"/>		
													            
													        </ul>
<!-- 														    </div> -->
													  </div>
														  
														<!-- 										FIN TIMELINE			   -->  
								  
													</div>
		                                            
                                       
		                                    </div>
		                                    
		                                    <div class="col-10">
		                                            <h2 class="fs-title">Evidencias</h2>
		                                            
		                                            <div class="" id="">
														  
<!-- 											INICIO TIMELINE			   -->
													  <div id="evidencias" class="container">
													  		<c:out value="${evidencias}" escapeXml="false"/>
													  </div>
													  
													  <div id="preview" class="fr-view">
													  </div>
													 
													</div>
		                                            
                                       
		                                    </div>
		                                    
		                                                       
		                
										</div>
		                            </div> 
		                            <button type="button" onClick="guardarCambiosExperimento()" class="swal2-confirm swal2-styled" style="background-color:#9e1b32;float: right;">Guardar Cambios</button>
		                            <button type="button" onClick="finalizarExperimento()" class="swal2-confirm swal2-styled" style="background-color:#9e1b32;float: right;">Finalizar Experimento</button>
                          			
                          			<div class="float" onClick="open_modal_comments();">
										<i class="far fa-comment my-float fa-4x"></i>
									  </div>
                          			<label style="display:none" id="idHipotesis">${idHipotesis}</label>
									<label style="display:none" id="idProyecto">${idProyecto}</label>
									<label style="display:none" id="idExperimento">${idExperimento}</label>
		
		                        </fieldset>
		                        
		                    </form>
		                </div>
		            </div>
		        </div>
		    </div>
		    
		    <!-- Modal para nuevo punto medicion -->
	        <div class="modal fade" id="modal-nuevo-medicion" role="dialog" style="height: auto;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <div>
                            <label>Seleccione fecha para punto de medición</label>
                        </div>
                    </div>
                    <div id="datepicker"></div>
                    <div style="margin:auto;">
                            <button type="button" onClick="agregarPuntoMedicion()" class="swal2-confirm swal2-styled" style="background-color:#9e1b32;">Guardar</button>                              
                    </div>
                </div>
            </div>
        </div>


		<!--Modal comentarios-->
       <div id="modalComment" class="modal fade" role="dialog">
		    <div class="modal-dialog comentario">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                 <h4 class="modal-title"></h4>  
		
		            </div>
		            <div class="modal-body">
		            	
<!-- 		                <h3>Aun no hay comentarios</h3> -->
<!-- 		                <p>Para comentar una hipótesis, ingresa al detalle</p> -->
		                
		                <div class="flex-column scrollbar-comentario" id="style-2">
		                
		                	<ul id="lista_comentario" class="list">
		                		<c:out value="${comentariosHipotesisHtml}" escapeXml="false"/>
		                		
		                	</ul>
		                </div>
		               
		            </div>
		            <div class="modal-footer">
		            	<textarea id="txtComentario" class="form-control" placeholder="Escribe tu comentario..."></textarea> 
		            	<button id="loadpage" onClick="saveComentario();" type="button" class="btn btn-primary">Enviar</button>
<!-- 		                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> -->
		            </div>
		        </div>
		        <!-- /.modal-content -->
		    </div>
		    <!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->

	</div>
	
	

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/froala_editor.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/file.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/js/froala_editor.pkgd.min.js"></script>


	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.js"></script>


	<!-- SweetAlert2 --> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ws/endpoint.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/loginService.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/proyectoService.js"></script> 
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/menu.js"></script>
<script src="https://unpkg.com/complex-js@5.0.0/dst/complex.min.js"></script>

      
      
      <script>
          $(document).on( 'click', '.salir', function(){
            sessionStorage.removeItem('user');
            window.location.href = '/login';
          })
        </script>
      <script>

      $( function() {
    	  $("#datepicker").datepicker({
    		    dateFormat: 'dd/mm/yy'
//     		    onSelect: function() {
//     		        var dateObject = $('#datepicker').datepicker().val();
//     		        alert(dateObject);
//     		    }
    		   }
   		  );
   		  
	  } );

      var editorInstance;

      
        window.onload = function() {         
        	var tableMetricas = $('#tableMetricas').DataTable({
				searching: false,
				ordering: false,
				bInfo: false,
			    paging: false
			} );

        	var tableCondiciones = $('#tableCondiciones').DataTable({
				searching: false,
				ordering: false,
				bInfo: false,
			    paging: false
			} );

        	var tablePuntoMedicion = $('#tablePuntoMedicion').DataTable({
				searching: false,
				ordering: false,
				bInfo: false,
			    paging: false
			} );

        	//tablePuntoMedicion.clear().draw();
        	setProgressBar(3);

//         	var editor = new FroalaEditor('#evidencias')

//         	(function () {
//         	      const editorInstance = new FroalaEditor('#evidencias', {
//         	        enter: FroalaEditor.ENTER_P,
//         	        events: {
//         	          initialized: function () {
//         	            const editor = this
//         	            document.getElementById('preview').innerHTML = editor.html.get()
//         	          },
//         	          contentChanged: function () {
//         	            const editor = this
//         	            document.getElementById('preview').innerHTML = editor.html.get()
//         	          }
//         	        }
//         	      })
//         	    })();


        	
        	      editorInstance = new FroalaEditor('#evidencias', {
        	        enter: FroalaEditor.ENTER_P,
        	     	// Set the image upload URL.
        	        imageUploadURL: '/editor/upload_image',
        	        imageUploadParams: {
        	          id: 'my_editor'
        	        },
        	        fileUploadURL: '/editor/upload_file',
        	        fileUploadParams: {
        	          id: 'my_editor'
        	        },
        	     	// Set the video upload URL.
        	        videoUploadURL: '/editor/upload_video',
        	        // Set max video size to 50MB.
        	        videoMaxSize: 50 * 1024 * 1024,
        	        videoUploadParams: {
        	          id: 'my_editor'
        	        },




        	        
//         	    	// Set the save param.
//         	        saveParam: 'content',

//         	        // Set the save URL.
//         	        saveURL: '/editor/save',

//         	        // HTTP request type.
//         	        saveMethod: 'POST',

//         	        // Additional save params.
//         	        saveParams: {idExperimento: '45'},

//         	     // Set the file upload parameter.
//         	        fileUploadParam: 'file_param',

//         	        // Set the file upload URL.
//         	        fileUploadURL: '/upload_file',

//         	        // Additional upload params.
//         	        fileUploadParams: {id: 'my_editor'},

//         	        // Set request type.
//         	        fileUploadMethod: 'POST',

//         	        // Set max file size to 20MB.
//         	        fileMaxSize: 20 * 1024 * 1024,

//         	        // Allow to upload any file.
//         	        fileAllowedTypes: ['*'],
        	        
        	        events: {
        	          initialized: function () {
        	            const editor = this
        	            document.getElementById('preview').innerHTML = editor.html.get()
        	          },
        	          contentChanged: function () {
        	            const editor = this
        	            document.getElementById('preview').innerHTML = editor.html.get()
        	          }
//         	          'save.before': function () {
//         	              // Before save request is made.
//         	            },

//         	            'save.after': function () {
//         	              // After successfully save request.
//         	            },

//         	            'save.error': function () {
//         	              // Do something here.
//         	            },
//         	            'file.beforeUpload': function (files) {
//         	                // Return false if you want to stop the file upload.
//         	              },
//         	              'file.uploaded': function (response) {
//         	                // File was uploaded to the server.
//         	              },
//         	              'file.inserted': function ($file, response) {
//         	                // File was inserted in the editor.
//         	              }
        	        }
        	      });

        	
        	
	 	}

	 	function openModalPunto(){
	 		$("#modal-nuevo-medicion").modal();
		 }

		 function agregarPuntoMedicion(){
			 
			 var weekday=new Array(7);
			 weekday[0]="Domingo";
			 weekday[1]="Lunes";
			 weekday[2]="Martes";
			 weekday[3]="Miercoles";
			 weekday[4]="Jueves";
			 weekday[5]="Viernes";
			 weekday[6]="Sábado";

			 var txtMes = new Array(13);
			 txtMes[1]="Enero";
			 txtMes[2]="Febero";
			 txtMes[3]="Marzo";
			 txtMes[4]="Abril";
			 txtMes[5]="Mayo";
			 txtMes[6]="Junio";
			 txtMes[7]="Julio";
			 txtMes[8]="Agosto";
			 txtMes[9]="Setiembre";
			 txtMes[10]="Octubre";
			 txtMes[11]="Noviembre";
			 txtMes[12]="Diciembre";


			 
			var idMetricas = [];



			var fechaTxt = $('#datepicker').datepicker().val();
			var date = $('#datepicker').datepicker('getDate');
			var dia = date.getDate();
			var mes = date.getMonth() + 1;
			var anio = date.getFullYear();
			var diaSemana = date.getUTCDay();
			var dayOfWeek = weekday[date.getUTCDay()];
		
			console.log('Fecha txt ' + fechaTxt);
			console.log('date ' + date);
			console.log('dia ' + dia);
			console.log('mes ' + mes);
			console.log('año ' + anio);
			console.log('Dia semana ' + dayOfWeek);
			

			//Recorrer la lista de meses para ver si el mes que elegí se encuentra o no
			var estaMes = false;
			/*Agregar un nuevo LI (DIA -> 27, Jueves) al MES*/
			var nuevoLi = '<li class="timeline-section li-dia">';
			nuevoLi = nuevoLi.concat('<div semana="'+diaSemana+'" dia="'+dia+'" class="timeline-date">');
			nuevoLi = nuevoLi.concat(dia + ', '+dayOfWeek);
			nuevoLi = nuevoLi.concat('</div>');

			nuevoLi = nuevoLi.concat('<ul class="row">');
						/*Agregar metricas*/
			$( "#listaMetricas li" ).each(function( index ) {
				var idMetrica = $(this).attr('codigo');
				var txtMetrica = $(this).attr('nombre');
			
				nuevoLi = nuevoLi.concat('<li id="boxMetrica">');
				nuevoLi = nuevoLi.concat('<div class="col-sm-4">');
				nuevoLi = nuevoLi.concat('<div class="timeline-box">');
				nuevoLi = nuevoLi.concat('<div class="box-tittle">');
				nuevoLi = nuevoLi.concat('<i class="fa fa-asterisk text-success" aria-hidden="true"></i> Métrica');
				nuevoLi = nuevoLi.concat('</div>');

				nuevoLi = nuevoLi.concat('<div class="box-content">');
				nuevoLi = nuevoLi.concat('<div class="box-item"><strong>'+txtMetrica+' (L'+idMetrica+')</strong></div>');
				nuevoLi = nuevoLi.concat('<div class="box-item"><input idMetrica="'+idMetrica+'" indentificador="L'+idMetrica+'" onkeypress="validarInput(event)" class=""></div>');
				nuevoLi = nuevoLi.concat('</div>');

				nuevoLi = nuevoLi.concat('<div class="box-footer">-</div>');
				nuevoLi = nuevoLi.concat('</div>');
				nuevoLi = nuevoLi.concat('</div>');
				nuevoLi = nuevoLi.concat('</li>');
			});

						/*Agregar condiciones*/
			$( "#listaCondiciones li" ).each(function( index ) {
				var idCondicion = $(this).attr('codigo');
				var txtCondicion = $(this).attr('nombre');
				var txtCondicionHtml = txtCondicion.replace("<", "&#60;");
				nuevoLi = nuevoLi.concat('<li txtCondicion="'+txtCondicion+'" class="liCondicion">');
				nuevoLi = nuevoLi.concat('<div class="col-sm-4">');
				nuevoLi = nuevoLi.concat('<div class="timeline-box box-condicion">');
				nuevoLi = nuevoLi.concat('<div class="box-tittle">');
				nuevoLi = nuevoLi.concat('<i class="fa fa-asterisk text-success" aria-hidden="true"></i> Condición');
				nuevoLi = nuevoLi.concat('</div>');

				nuevoLi = nuevoLi.concat('<div class="box-content">');
				nuevoLi = nuevoLi.concat('<div class="box-item"><strong>'+txtCondicionHtml+'</strong></div>');

				/*Inicio de pulgar*/
				nuevoLi = nuevoLi.concat('<div class="box-item">');
				nuevoLi = nuevoLi.concat('<div id="inheritBox">');
				nuevoLi = nuevoLi.concat('<span><i class="far fa-thumbs-down dislike" style="display:none"></i></span>');
				nuevoLi = nuevoLi.concat('<span><i class="far fa-thumbs-up like" style="display:none"></i></span>');
				nuevoLi = nuevoLi.concat('<span><i class="fas fa-ban stop"></i></span>');
				nuevoLi = nuevoLi.concat('</div>');
				nuevoLi = nuevoLi.concat('</div>');
				/*----Fin de pulgar*/
				
				nuevoLi = nuevoLi.concat('</div>');

				nuevoLi = nuevoLi.concat('<div class="box-footer">-</div>');
				nuevoLi = nuevoLi.concat('</div>');
				nuevoLi = nuevoLi.concat('</div>');
				nuevoLi = nuevoLi.concat('</li>');
			});

			
			nuevoLi = nuevoLi.concat('</ul>');
			nuevoLi = nuevoLi.concat('</li>');
			
			$( "#lista-tiempo > li" ).each(function( index ) {

			      var div = $(this).find('div');
			      var numeroMes = $(div).attr('mes');
			      console.log('NumeroMes: ' + numeroMes + '  -   Mes: ' + mes);
			      if(numeroMes == mes){
				      console.log('ES del mismo mes');
					//Ya se encuentra el mes seleccionado, agregar aqui el nuevo mes
					var listaDias = $(this).find('ul').first();
					$(listaDias).append(nuevoLi);					
		
						var dias = $(listaDias).find('li.li-dia');

						dias.each(function( index, li ) {
							var divDia = $(li).find('div').first();

						});

					estaMes = true;
				  }
			});


			if(!estaMes){
				console.log('Agregar nuevo MES');
				//Agregar al un nuevo LI de MES
				var nuevoMes = 	'<li>';
				nuevoMes = nuevoMes.concat('<div mes="'+mes+'" class="timeline-month">');
				var mesTexto = txtMes[mes];		
				nuevoMes = nuevoMes.concat(mesTexto+', '+anio);
				nuevoMes = nuevoMes.concat('<span>3 Entries</span>');
				nuevoMes = nuevoMes.concat('</div>');
				nuevoMes = nuevoMes.concat('<ul>');
				nuevoMes = nuevoMes.concat(nuevoLi);
				nuevoMes = nuevoMes.concat('</ul>');
				nuevoMes = nuevoMes.concat('</li>');

				$('#lista-tiempo').append(nuevoMes);
			}
			
            
            $("#modal-nuevo-medicion").modal('toggle');	
           
		 }

		 $(document).on('blur', '#boxMetrica :input', function() {
			 	var ul = $(this).closest('ul');

				var liCondiciones = $(ul).find('li.liCondicion');
				liCondiciones.each(function( index, li ) {
					var condiTxt = $(li).attr('txtCondicion');
					var liInput = $(ul).find('input');
				 	liInput.each(function( index, input ) {
						var identificador = $(input).attr('indentificador');
						var valor = $(input).val();
						condiTxt = condiTxt.replace(identificador, valor);
					 });

				    var operador="";
					var opera = 0;
					var expresion ="";
					var esperado="";
					if (condiTxt.includes("=")) {
						operador="=";
						opera=1;						
					}else if(condiTxt.includes("<")) {
						operador="<";
						opera=2;
					}else if(condiTxt.includes(">")) {
						operador=">";
						opera=3;
					}
					var indexOperador = condiTxt.indexOf(operador);
					expresion = condiTxt.substring(0, indexOperador);
					esperado= condiTxt.substring(indexOperador+1, condiTxt.length);
					expresion = expresion.replace("%", "/100");
				    if(isMathExpression(expresion)){
						var resultado = eval(expresion);
						var bool = false;
						if(opera==1){
							if(resultado == esperado){
								bool = true;
								
						    }
						}else if(opera==2){
							if(resultado < esperado)
								bool = true;
							
						}else if(opera==3){
							if(resultado > esperado)
								bool = true;
							
						}
						var ilike = $(li).find('i.like');
						var idislike = $(li).find('i.dislike');
						var istop = $(li).find('i.stop');
						if(bool){
							$(ilike).show();
							$(idislike).hide();
							$(istop).hide();
						}else{
							$(ilike).hide();
							$(idislike).show();
							$(istop).hide();
						}
					}else{
						var ilike = $(li).find('i.like');
						var idislike = $(li).find('i.dislike');
						var istop = $(li).find('i.stop');
						console.log('NO es válido');
						$(ilike).hide();
						$(idislike).hide();
						$(istop).show();
					}	    	

				});
		    
			    
		 });

		 function isMathExpression (str) {
			  try {
			    Complex.compile(str);
			  } catch (error) {
			    return false;
			  }

			  return true;
		}

	 	function guardarCambiosExperimento(){
			var id = $("#idExperimento").text();
			var idProyecto = $("#idProyecto").text();
			var listMeses = $("#lista-tiempo > li");

			var meses = [];

			listMeses.each(function( index, li) {
				var divMes = $(li).find('div').first();
				var mes = $(divMes).attr('mes');
				
				var liDias = $(li).find('.li-dia');
				var dias = [];
				liDias.each(function( index, liDia){
					var divDia = $(liDia).find('div').first();
					var dia = $(divDia).attr('dia');
					var semana = $(divDia).attr('semana');
					var inputs = $(liDia).find('input');
					var mediciones = [];
					inputs.each(function (index, input){
						var idMetrica = $(input).attr('idMetrica');
						var valor = $(input).val();
						mediciones.push({"idMetrica": + idMetrica, "valor": + valor});
					});
					dias.push({dia, semana, mediciones});
				});
				meses.push({mes, dias});
			});
			
			var payload = {
			        "id":id,
			        "idProyecto": idProyecto,
			        "fechas": meses,
			        "evidencias": editorInstance.html.get(true)
			    }

			
// 			console.log('DEL EDITOR');
			
// 			console.log(  editorInstance.html.get(true)  );
// 			editorInstance.save.save();
			updateMedicionesWs(payload);
		 }

	 	function setProgressBar(curStep){
	        console.log(curStep);
	        var percent = parseFloat(100 / 4) * curStep;
	        percent = percent.toFixed();
	        $(".progress-bar")
	        .css("width",percent+"%")
    	}

    	function finalizarExperimento(){

        	/*Cambiar el estado al experimento e hipotesis a 5*/
    		var idExperimento = $("#idExperimento").text();
			var idProyecto = $("#idProyecto").text();
			var idHipotesis = $("#idHipotesis").text();
			finalizarExperimentoWs(idExperimento, idProyecto, idHipotesis);
			
//     		window.location ='experimento/finalizar?id='+idProyecto+'&idExperimento='+id;
        }

    	function open_modal_comments(){
			$("#modalComment").modal();
		 }
		 
    	function saveComentario(){
			var comentario = $("#txtComentario").val();
			var idExperimento = $("#idExperimento").text();
			var payload = {comentario, idExperimento};
			saveComentarioExperimento(payload);
			var newComentario = "";
			newComentario = newComentario.concat('"<li class="item"><span>'+comentario+'</span></li>');

			$("#lista_comentario").append(newComentario);
	     }

    	$(document).on('click', '#btnResuelta', function() {
			 var liCurrent = $(this).closest('li');
			 var idComentario = $(liCurrent).attr('idComentario');
			 updateEstadoComentarioDetalleWs(idComentario, 2);
			 console.log(idComentario);
			 $(this).closest('li').remove();
		 });

    	function validarInput(event){
			var regex = new RegExp("[0-9]");
		    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
		    if (!regex.test(key)) {
		       event.preventDefault();
		       return false;
		    }
		}
      </script>
</body>
</html>