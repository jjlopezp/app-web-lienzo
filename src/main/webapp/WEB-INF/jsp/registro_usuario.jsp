<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Lienzanier</title>
<link href='http://fonts.googleapis.com/css?family=Headland+One' rel='stylesheet' type='text/css'>
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/canvas.css">
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/menu.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">    
    <link href="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.css" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
</head>
<body>
	<header>
		<jsp:include page='header-inicio.jsp'/>
	</header>


	<div class="container">
        <div class="row justify-content-center">
                 <div class="col-md-8">
                     <div class="card">
                         <div class="card-header">Registro</div>
                         <div class="card-body">

                             <form class="form-horizontal" method="post" action="#">

                                 <div class="form-group">
                                     <label for="username" class="cols-sm-2 control-label">Ingresa tu usuario</label>
                                     <div class="cols-sm-10">
                                         <div class="input-group">
                                             <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                             <input id="idUsuario" onblur="validateUsuario()" type="text" class="form-control" name="name" id="name" placeholder="Usuario" />                                       
                                         </div>
                                         <p id="msgUsuarioValido" style="display:none"><span style="color:green">Usuario válido</span></p>
                                             <p id="msgUsuarioNoValido" style="display:none"><span style="color:red">Usuario ya registrado</span></p>
                                     </div>
                                 </div>
                                 <div class="form-group">
                                     <label for="email" class="cols-sm-2 control-label">Ingresa tu correo</label>
                                     <div class="cols-sm-10">
                                         <div class="input-group">
                                             <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                             <input id="idEmail" onblur="validateEmail()" type="text" class="form-control" name="email" id="email" placeholder="Correo electrónico" />
                                         	 
                                         </div>
                                         <p id="msgCorreoValido" style="display:none"><span style="color:green">Correo válido</span></p>
                                             <p id="msgCorreoNoValido" style="display:none"><span style="color:red">Correo ya registrado</span></p>
                                     </div>
                                 </div>
                                 
                                 <div class="form-group">
                                     <label for="password" class="cols-sm-2 control-label">Contraseña</label>
                                     <div class="cols-sm-10">
                                         <div class="input-group">
                                             <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                             <input id="idPass"  type="password" class="form-control" name="password" id="password" placeholder="Contraseña" />
                                         </div>
                                     </div>
                                 </div>
                                 <div class="form-group">
                                     <label for="confirm" class="cols-sm-2 control-label">Confirma contraseña</label>
                                     <div class="cols-sm-10">
                                         <div class="input-group">
                                             <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                             <input id="idConfirmPass" type="password" onblur="validatePass()" class="form-control" name="confirm" id="confirm" placeholder="Confirma contraseña" />
                                       		  
                                         </div>
                                         <p id="msgPassNoValido" style="display:none"><span style="color:red">No coinciden</span></p>
                                     </div>
                                 </div>
                                
                                 <div class="form-group ">
                                     <button type="button" onclick="registrar()" style="background-color:#9e1b32" class="btn btn-primary btn-lg btn-block login-button">Registrar</button>
                                 </div>
                                  <div id="mensajeError" class="form-group" style="display:none;color:red">
                                 	<label>Verificar validaciones</label>
                                 </div>
<!--                                  <div class="login-register"> -->
<!--                                      <a href="/login">Login</a> -->
<!--                                  </div> -->
                             </form>
                         </div>

                     </div>
                 </div>
           </div>
        </div>


	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.js"></script>

	<!-- SweetAlert2 --> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ws/endpoint.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/loginService.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/proyectoService.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ws/endpoint.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/canvas.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/menu.js"></script>
	

	<script>
		function validateUsuario(){
			var usuario = $("#idUsuario").val();
			if(usuario != "")
				validateUsuarioRegistradoWs(usuario);
			
		}

		function validateEmail(){
			var usuario = $("#idEmail").val();
			if(usuario != "")
				validateEmailRegistradoWs(usuario);
		}

		function validatePass(){
			var password = $("#idPass").val();
			var password2 = $("#idConfirmPass").val();
			if(password != password2){
				$("#msgPassNoValido").show();
			}else{
				$("#msgPassNoValido").hide();
			}
			
		}

		function registrar(){

			if($('#msgUsuarioNoValido').css('display') === 'block'){
				console.log('Ver usuario');
				$("#mensajeError").show();
				
			}else if($('#msgCorreoNoValido').css('display') === 'block'){
				console.log('Ver correo');
				$("#mensajeError").show();
			}else if($('#msgPassNoValido').css('display') === 'block'){
				console.log('Ver pass');
				$("#mensajeError").show();
			}else{
				$("#mensajeError").hide();
				var usuario = $("#idUsuario").val();
				var email = $("#idEmail").val();
				var password = $("#idPass").val();
				registrarUsuario(usuario,email,password);
			}
			
		}

	</script>

</body>
</html>