<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- mobile metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="viewport" content="initial-scale=1, maximum-scale=1">
      <!-- site metas -->
      <title>Lienzanier</title>
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- bootstrap css -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
      <!-- style css -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style_theme.css">
      <!-- Responsive-->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/responsive.css">
      <!-- fevicon -->
      <link rel="icon" href="${pageContext.request.contextPath}/resources/img/fevicon.png" type="image/gif" />
      <!-- Scrollbar Custom CSS -->
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery.mCustomScrollbar.min.css">
      
      
      <style>
      
      	.card-container {
			background-color: #fafafa;
			border-radius: 5px;
			box-shadow: 0px 10px 20px -10px rgba(0,0,0,0.75);
			color: #B3B8CD;
			padding-top: 30px;
			position: relative;
			width: 350px;
			max-width: 100%;
			text-align: center;
			height: 500px;
		}
		
		
		.card-container .round {
			border: 1px solid #03BFCB;
			border-radius: 50%;
			padding: 7px;
		}
		
		button.primary {
			background-color: #03BFCB;
			border: 1px solid #03BFCB;
			border-radius: 3px;
			color: #231E39;
			font-family: Montserrat, sans-serif;
			font-weight: 500;
			padding: 10px 25px;
		}
		
		button.primary.ghost {
			background-color: transparent;
			color: #02899C;
		}
		
		
		h3 {
			margin: 10px 0 !important;
			font-family: 'Jost', sans-serif !important;;
		    font-size: 1.8rem !important;;
		    line-height: 1.5 !important;;
		}
		
		h6 {
			margin: 5px 0;
			text-transform: uppercase;
		}
		
		h1 {
			font-family: 'Jost', sans-serif !important;
		    font-size: 4.6rem !important;
		    line-height: 1.1 !important;
		    margin: auto !important;
   			 width: fit-content !important;
   			 text-align: center !important;
		}
		
		p {
			font-size: 14px;
			line-height: 21px;
		}
		
		.container-img{
			margin: 0 340px;
		}
		
		.contenido{
			font-size: 24px;
    		line-height: 1.6;
    		font-family: open-sans, Open Sans, Helvetica Neue, Helvetica, Arial, sans-serif;
/*     		color: #555; */
		}
		
		.div-img{
			margin: 25px 0;
			text-align: center;
		}
		
		.underline-bottom-brand-red {
		    padding-bottom: .5rem!important;
		    border-bottom: .3rem solid #ff352a;
		   
		}
		
		ol{
			color: #000;
		}
		
		.contenido ol{
			color: #212222;
		}
      
      </style>
      
      
</head>
 <body >
     
     <header>
		<jsp:include page='header-inicio.jsp'/>
	</header>
	
	
	<div>
		<h1>Método Lean Startup</h1>
	
	</div>
	
	<article id="top">
		<div class="row" style="margin-left: 30%;">
	
			<div class="card-container">
				<img class="round" src="${pageContext.request.contextPath}/resources/img/foco_blog.jpg" alt="user" />
				<h3><strong>¿De que se trata?</strong></h3>
				<p>
					El método lean startup es una metodología basada en “aprendizaje validado”, es decir, ir verificando poco a 
					poco las hipótesis antes de tener el producto final(la startup definitiva) y comenzar a escalar el negocio.
				</p>	
				<div class="buttons">
					<a  href="#definicion">
						<button class="primary ghost">
							Ver más
						</button>
					</a>
					
				</div>				
			</div>
			
			<div class="card-container">
				<img style="height:172px" class="round" src="${pageContext.request.contextPath}/resources/img/pasos_blog.png" alt="user" />
				<h3><strong>¿Cómo se utiliza?</strong></h3>
				<p>
					<ol>
						<li>Plantea una hipótesis</li>
						<li>Valida la hipótesis</li>
						<li>Mide la hipótesis</li>
						<li>Genera un aprendizaje validado</li>
						<li>Ciclo repetitivo</li>
					</ol>
				</p>
				<div class="buttons">	
					<a href="#usar">
						<button  class="primary ghost">
							Ver más
						</button>
					</a>			
					
				</div>
				
			</div>		
			
		
		</div>
	</article>
	
	<div class="container-img">
		<div class="div-img">
			<img src="${pageContext.request.contextPath}/resources/img/crear_medir_aprender_blog.jpg" alt="#" />
		</div>
		
		<section>
			<p class="contenido" id="definicion">
				El método  <strong>lean startup</strong> es una metodología basada en “aprendizaje validado”, es decir, ir verificando poco a poco 
				las hipótesis antes de tener el producto final(la startup definitiva) y comenzar a escalar el negocio.

				La idea es ir definiendo y acortando los ciclos de desarrollo, lanzando distintas propuestas por un periodo de tiempo y obteniendo un feedback muy 
				valioso de nuestros potenciales clientes o usuarios, con los que mejorar la siguiente versión final del producto.
			
			</p>
			<br><br><br>
			<h3 class="large underline-bottom-brand-red space-line-bottom">Cómo se utiliza?</h3>
			<p class="contenido" id="usar">
				<ol>
					<li>
						<strong>Plantea una hipótesis</strong>
						<p>
							Parte de un problema a resolver y explica por qué el cliente estaría dispuesto a pagar por tu oferta.

							Para identificar el problema/dolor, podemos realizar una serie de entrevistas a nuestros clientes potenciales e
							identificar qué les preocupa realmente. Debemos saber si el problema es lo suficientemente doloroso para atacarlo.
						</p>
						<br>
					</li>
					
					<li>
						<strong>Valida la hipótesis</strong>
						<p>
							Desde crear un producto o servicio con las características mínimas básicas para comprobar si es lo que el mercado 
							quiere hasta una demostración de cómo funciona, todo es posible. El objetivo es saber si la gente lo querría y lo compraría.

							Esta primera validación será de los “early adopters”, los primeros usuarios que lo utilizarán y los más susceptibles de 
							probar cosas nuevas en nuestro sector.
						</p>
						<br>
					</li>
					
					<li>
						<strong>Plantea una hipótesis</strong>
						<p>
							Parte de un problema a resolver y explica por qué el cliente estaría dispuesto a pagar por tu oferta.

							Para identificar el problema/dolor, podemos realizar una serie de entrevistas a nuestros clientes potenciales e
							identificar qué les preocupa realmente. Debemos saber si el problema es lo suficientemente doloroso para atacarlo.
						</p>
						<br>
					</li>
					
					<li>
						<strong>Mide la hipótesis</strong>
						<p>
							La mejor manera de saber qué métricas implementarás es identificando cuáles son los pasos a seguir para llegar hasta 
							tu oferta y cuántas veces recurrieron a ellos para comprar.

							Es fundamental identificar los indicadores de calidad o KPI de un producto, medirlas para saber si cumplimos objetivos y 
							vamos perfeccionando nuestro producto.
						</p>
						<br>
					</li>
					
					<li>
						<strong>Genera un aprendizaje validado</strong>
						<p>
							Significa que habrás realizado ajustes y cambios tanto en el producto o servicio, como en el mercado y los proveedores. 
							La idea es ir aprendiendo del entorno al que va dirigido el producto.

							Es fundamental saber escuchar a todos los stakeholders (Personas implicadas directa o indirectamente en el producto/servicio) e
							incorporar su feedback.
						</p>
						<br>
					</li>
					
					<li>
						<strong>Ciclo repetitivo</strong>
						<p>
							Pones en marcha los pasos anteriores una vez más ya con un producto o servicio mejorado y volvemos a empezar.
						</p>
						<br>
					</li>
					
				</ol>
			
			</p>
			
		
		
		</section>
		
		<br><br>
		
         <div class="row margin_top_30">
            <div class="col-md-12">
               <div class="button_section full center margin_top_30">
                  <a style="margin:0;" href="#top">Subir</a>
               </div>
            </div>
         </div>
         <br><br>
	</div>
	
	
     <script>
	     $(document).ready(function() {
		      $("#articulos").addClass("active");
		});
     </script>
     
     
     
     
      <!-- end footer -->
      <!-- Javascript files-->
      <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/bootstrap.bundle.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/plugin.js"></script>
      <!-- Scrollbar Js Files -->
      <script src="${pageContext.request.contextPath}/resources/js/jquery.mCustomScrollbar.concat.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/custom.js"></script>
      
      <script>
	     $(document).ready(function() {
		      $("#articulos").addClass("active");
		});
     </script>
   </body>
</html>