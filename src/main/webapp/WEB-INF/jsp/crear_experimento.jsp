<%@ taglib prefix="c" uri="http://www.hdiv.org/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Lienzanier</title>
	<link href='http://fonts.googleapis.com/css?family=Headland+One' rel='stylesheet' type='text/css'>
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/canvas.css">
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/menu.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/steps.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/options.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">    
    <link href="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.css" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    
    <style>
    	label{
    		font-size: 1.5rem;
    	}
    	
    	#list-indicadores li {
   			list-style-type: none;
   			margin-top:10px;	
   					
    	}
    	
    	#list-indicadores{
    		display: flex;
    		flex-wrap: wrap;
    	}
    	
    	.icon-operador{
    		font-size: 1.2rem;
    		margin: 0 0.7rem;
    	}
    	
    	
    	.div-border{
/*     		border: 2px solid black; */
    		height: auto;
/*     		border-radius: 25px; */
    		padding: 0.8rem;
    	}
    	
    	.div-border label{
    		margin: 0 5px;
    	}
    	
    	#new-listaIndicadores li{
/*     		display: inline; */
    	}
    	
    	#list-condicion-crear{
/*     		display:flex; */
    		flex-wrap: wrap;
    	}
    	#list-condicion-crear li{
    		display: inline;
    		flex-grow:1;
    	}
    	
    	
    	.input-valor{
    		text-align: center;
    	}
    	
    	ul{
    		list-style-type: none;
    	}
    	
    	.label-bold{
    		font-weight: bold;
    	}
    	
    	.div-border p{
    		font-size: 1rem;
    		margin-bottom: 0.1rem;
    	}
    	
    	.div-border  label{
    		font-size: 0.8rem;
    	}
    	
    	#main2::after{
    		content: "";
/*  			background-image: url('${pageContext.request.contextPath}/resources/img/creaMideAprende.png');    	  */
    		opacity: 0.5;
			  top: 0;
			  left: 0;
			  bottom: 0;
			  right: 0;
			  position: absolute;
			  z-index: -1; 
    	}
    	
    	.crear-experimento-container{
    		border: 1px solid black;
		    width: 70%;
		    margin: 30px auto !important;
 		    background-color: antiquewhite; 
		    border-radius: 50px;
		    display:flex
    	}
    	
    	.flex-derecha{
    		display: flex;
			flex-wrap: wrap;
			margin-bottom: 16px;
			word-break: break-word;
    	}
    	
    	
    	
    	.info-left{
    		width: 30%;
    		margin: 0 5%;
    	}
    	
    	.info-derecha{
    		width: 50%;
    		margin: 0 5%;
    	}
    	
    	select.form-control{
    		width: 300px;
    	}
    	
    	.first-element{
    		width: 100%;
    		text-align: center;
    	}
    	
    	.align-left{
    		text-align: left;
    	}
    	
    	.first-element-derecha{
    	
    		margin-top: 7rem;
    	}
    	
    	.row:not(#rowHeader){
    		flex-direction: column !important;
    		width: 131% !important;
    	}
    	
    	.list-group{
    		flex-direction: row !important;
    	}
    	
    	.col-4{
    		max-width: 100%;
    	}
    	
    	.col-8{
    		max-width: 100%;
    	}
    	
    	.col-10{
    		flex: 0 0 53.333333% !important;
    		margin-top: 2rem;
    	}
    	
    	.modal-dialog{
    		max-width: 1000px !important;
    	}
    	
    	#modal-crear-condicion{
    		color: #000;
    	}
    </style>
</head>
<body>
	<header>
		<jsp:include page='header.jsp'/>
	</header>
	<div id="main2">
		

			<div class="container-fluid">
		        <div class="">
		                <div class="">
		                    <div class="">                      
		
		                    <h2 id="heading">Estás a un paso</h2>
		                    <p style="text-align: center">Falta poco</p>
		                    <form id="msform">
		                        <!-- progressbar -->
		                        <ul id="progressbar">
		                            <li class="active" id="account"><a href="/editar/hipotesis?id=${idHipotesis}&idPro=${idProyecto}"><strong>Tu hipótesis</strong></a></li>
		                            <li class="active" id="personal"><strong>¿Listo para experimentar?</strong></li>
		                            <li id="payment"><strong>Estamos en marcha</strong></li>
		                            <li id="confirm"><strong>Finalizar</strong></li>
		                        </ul>
		                        <div class="progress">
		                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
		                        </div> <br> <!-- fieldsets -->
		                        <fieldset>
		                                
		                            <div class="">
		                                <div class="">
		                                
		                                	<div class="flex-lienzo">
		                                		<div class="col-10">
		                                        	<h2 class="fs-title">¿Que piensas?</h2>
		                                        	<textarea id="txtEditSuposicion" disabled>${hipotesisDto.suposicion}</textarea>
			                                    </div>
			                                    <div class="col-10">
			                                            <h2 class="fs-title">¿De que trata?</h2>
			                                            <textarea id="txtEditDescripcion" disabled>${hipotesisDto.descripcion}</textarea>
			                                    </div>
		                                	</div>		                                
		                                	
       
		                                   
		                                  <div class="col-10">
		                                            <h2 class="fs-title">¿Que tipo de prueba quieres hacer?</h2>
		                                            
		                                            <div class="row">
														  <div class="col-4">
														    <div class="list-group" id="list-info" role="tablist">
														      <c:out value="${headerPruebas}" escapeXml="false"/>
														    </div>
														  </div>
														  <div class="col-8">
														    <div class="tab-content" id="nav-tabContent">
														      <c:out value="${cuerpoPruebas}" escapeXml="false"/>
														    </div>
														  </div>
													</div>		                                       
		                                            
		                                    </div>
		                                    <div class="col-10">
		                                            <h2 class="fs-title">¿Como vas a medir tu hipótesis?</h2>
		                                            <div id="msgErrorMetrica" class="alert alert-danger" style="display:none">
												    	Seleccione por lo menos una métrica
											  		 </div>
		                                            <div class="flex-lienzo flex-column scrollbar" id="style-2">
		                                            	<c:out value="${optionIndicadores}" escapeXml="false"/>
													  
													</div>
		                                            
                                       
		                                    </div>
		                                    <div class="col-10">
		                                            <h2 class="fs-title">Establece tus propias condiciones!</h2>
		                                            
		                                            <div id="msgErrorCondicion" class="alert alert-danger" style="display:none">
												    	Debe colocar por lo menos una condición
											  		 </div>
                                                    <div class="flex-modal">
							                    		<div>
							                    			<label>Operaciones permitidas</label>
									                        <div style="text-align:center;">
									                        	<label class="icon-operador" onClick=""> <i class="fa fa-plus" aria-hidden="true"></i> </label>
									                        	<label class="icon-operador" onClick=""> <i class="fas fa-minus"></i> </label>
									                        	<label class="icon-operador" onClick=""> <i class="fas fa-divide"></i> </label>
									                        	<label class="icon-operador" onClick=""> <i class="fas fa-times"></i> </label>
									                        	<label class="icon-operador" onClick=""> <i class="fas fa-percentage"></i> </label>
									                        </div>   
							                    		</div>
							                    		<div>
							
									                        <div style="text-align:center;">
									                        	<label class="icon-operador" onClick=""><i class="fas fa-equals"></i></label>
									                        	<label class="icon-operador" onClick=""><i class="fas fa-less-than"></i></label>
									                        	<label class="icon-operador" onClick=""><i class="fas fa-greater-than"></i></label>
									                        </div>
							                    		</div>
								                        
								                        
							                    	</div>
		                                            
		                                            
                                                    <label>Ejemplo</label>
							                        <div class="div-border">
							                        	<p>#Usuarios ingresan a la página de publicidad -> L10</p>
							                        	<p>#Usuarios proporcionan datos para envio info -> L20</p>
							                        	<p style="text-align: center;" >40% * L10 &lt; L20</p>
							                        	<label style="width: 100%;text-align: center;">Por lo menos el 40% de L10 son L20.</label>
							                        	<label>Por lo menos el 40% de usuarios que ingresan a la página de pubilicidad proporcionan sus datos personales para que se le envíe información adicional </label>
							                        </div>
		                                            
		                                            
		                                            <button type="button" onClick="openModalCondicion();" class="swal2-confirm swal2-styled" style="background-color:#9e1b32;">Arma tu condición</button>
		                                            
		                                            <div class="list-square">														
													
													    <ul id="list_condiciones" class="SG">												        
													     
													    
													    </ul>
													</div>
		                                    </div>
		                                    
		                                </div> 
		                                                       
		                
		
		                            </div> 
		                            
		                        </fieldset>
		                        
		                    </form>
		                </div>
		            </div>
		        </div>
		        <button type="button" onClick="crearExperimento()" class="swal2-confirm swal2-styled" style="background-color:#9e1b32;float: right;">Iniciar Experimento</button>
		    </div>

	        <div class="modal fade" id="modal-crear-condicion" role="dialog" style="height: auto;">
	            <div class="modal-dialog">
	                <div class="modal-content">
	                    <div class="modal-header">
	                        <div>
	                            <label>Crear condición</label>
	                        </div>
	                    </div>
	                    <div class="modal-body pop-inner">
	                    	<div class="">
		                    	<div class="flex-modal">
		                    		<div>
		                    			<label>Operaciones permitidas</label>
				                        <div style="text-align:center;">
				                        	<label class="icon-operador" onClick=""> <i class="fa fa-plus" aria-hidden="true"></i> </label>
				                        	<label class="icon-operador" onClick=""> <i class="fas fa-minus"></i> </label>
				                        	<label class="icon-operador" onClick=""> <i class="fas fa-divide"></i> </label>
				                        	<label class="icon-operador" onClick=""> <i class="fas fa-times"></i> </label>
				                        	<label class="icon-operador" onClick=""> <i class="fas fa-percentage"></i> </label>
				                        </div>   
		                    		</div>
		                    		<div>
		
				                        <div style="text-align:center;">
				                        	<label class="icon-operador" onClick=""><i class="fas fa-equals"></i></label>
				                        	<label class="icon-operador" onClick=""><i class="fas fa-less-than"></i></label>
				                        	<label class="icon-operador" onClick=""><i class="fas fa-greater-than"></i></label>
				                        </div>
		                    		</div>
			                        
			                        
		                    	</div>
		                        
		                        
		                        
		                        <div class="div-border">		                        	
		                        	<p style="text-align: center;" >40% * L10 &lt; L20</p>
		                        	<label style="width: 100%;text-align: center;">Por lo menos el 40% de L10 son L20.</label>
		                        	
		                        </div>
							</div>	                        
	                        
	                        <label>Indicadores</label>
	                        <div class="div-border">
	                        	<ul id="new-listaIndicadores">
	                        	</ul>
	                        </div>
	                        
	                      
	                        <label>Condición</label>
	                        <div>
	                        	<ul id="list-condicion-crear">
	                        	
	                        		<li><input class="form-control input-valor" onblur="updateCondicionResultado()" id="txtCondicion"></li>
	                        	</ul>                     	
	                        </div>
	                        <div>
	                        	<label style="display:block;">Resultado</label>
	                        	<textarea class="form-control" id="txtCondicionResultado" disabled></textarea>
	                        	<br>
	                        	<textarea class="form-control" id="txtDescripcionCondicion" style="display:block">Describe la condición</textarea>
	                        </div>
	                        
	                                               
	                    </div>
	                    <div style="margin:auto;">
	                            <button type="button" onClick="confirmCrearCondicion()" class="swal2-confirm swal2-styled" style="background-color:#9e1b32;">Guardar</button>
	                            <button type="button" onClick="" class="swal2-cancel swal2-styled" style="background-color:#bdbdbd;">Cancelar</button>                                  
	                    </div>
	                </div>
	            </div>
        	</div>

       		<label style="display:none" id="idHipotesis">${idHipotesis}</label>
        	<label style="display:none" id="idProyecto">${idProyecto}</label>
        	<label style="display:none" id="idExperimento">${idExperimento}</label>


        
	</div>
	
	
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.js"></script>

	<!-- SweetAlert2 --> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ws/endpoint.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/loginService.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/proyectoService.js"></script> 
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/menu.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/steps.js"></script>
      <script>
          $(document).on( 'click', '.salir', function(){
            sessionStorage.removeItem('user');
            window.location.href = '/login';
          })
        </script>
      <script>
      	window.onload = function() {  

      		setProgressBar(2);
          	       
      		var tableCondiciones = $('#tableCondiciones').DataTable({
				searching: false,
				ordering: false,
				bInfo: false,
			    paging: false
			} );
      	}
      	
	 	function addIndicador(){
			var ul = $('#list-indicadores');
			ul.find('li:first').clone(true).appendTo(ul);
						
// 			$("#list-indicadores").append('<li><select><option>Seleccione</option></select></li>');
		 }

		 function removeIndicador(){

		    var ul = $('#list-indicadores');
			ul.find('li:last-child').remove();
		 }

	 	function openModalCondicion(){
	 		$('#new-listaIndicadores li').remove();
			var nElem = 0;


			var indicadores = document.querySelectorAll('.option-input:checked');

			if(indicadores.length == 0){
				$("#msgErrorMetrica").show();
			}else{
				$("#msgErrorMetrica").hide();
				Array.from(indicadores).forEach((element, index) => {

					  var idIndicador = $(element).val();
					  var txtIndicador = $(element).attr('texto');
					  $("#new-listaIndicadores").append('<li value='+ idIndicador +' texto="'+txtIndicador+'">'+ txtIndicador  +' -> <span class="label-bold">L'+idIndicador +'</span></li>');
					  nElem = nElem + 1;	
				});


				if(nElem == 0){
					$("#new-listaIndicadores").append('<li>No ha seleccionado ningún indicador</li>');
				}
		 		
		 		
		 		$("#modal-crear-condicion").modal();
			}
			
			
		 }

		 function condicionCheck(){
			console.log('entra condicionCheck');
		}

		function confirmCrearCondicion(){
			var condicion = $('#txtCondicionResultado').val();
			var descripcion = $('#txtDescripcionCondicion').val();
			var condiFake = $('#txtCondicion').val();
			condiFake = condiFake.replace('<','&lt;');
			condiFake = condiFake.replace('>','&gt;');
			//&lt; <
			//&gt; >

			if(contiene1operador(condicion)){
				console.log('Pasó esa validacion');
				//falta
// 				$("#list-condiciones").append('<li>'+ condicion  +'</li>');
				console.log('Condi fake ' + condiFake);

				var li = '<li condicion="'+condiFake+'" descripcion="'+descripcion+'" class="sgLi">';
				li = li.concat('<div class="box">');
					li = li.concat('<h3>'+descripcion+'</h3>');
					li = li.concat('<p>'+condicion+'</p>');
					li = li.concat('<div id="orangeBox">');
						li = li.concat('<span id="x">X</span>');
					li = li.concat('</div>');
				li = li.concat('</div>');
				li = li.concat('</li>');

				$("#list_condiciones").append(li);


			}else{
				console.log('NO paso de 1 operador');
			}
			var table = $('#tableCondiciones').DataTable();
			//table.draw();
			$("#modal-crear-condicion").modal('toggle');
		}
		
		function contiene1operador(str){
			var arr = ['<', '>', '='];
			var value = 0;
			
			arr.forEach(function(word){
		      value = value + str.includes(word);
		    });

		    return (value === 1)
		}

		function productDelete(ctl) {
		    $(ctl).parents("tr").remove();
		}

		 $('[id=txtCondicion]').on('keypress', function (event) {
			    var regex = new RegExp("[0-9=()%L<>+\-\/*]");
			    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
			    if (!regex.test(key)) {
			       event.preventDefault();
			       return false;
			    }
			});

		 $('#new-listaIndicadores li').on('click', function() {
			 var textLi = 'L'+$(this).attr('value');
// 			    $('#txtCondicion').val( $('#txtCondicion').val() + textLi );
			});

		function updateCondicionResultado(){
			//list-condicion-crear
			//txtCondicionResultado
			$('#txtCondicionResultado').val('');
			var texto = $('#txtCondicion').val();
			var textoResult = texto;


			$( "#new-listaIndicadores li" ).each(function( index ) {
			      var li = $(this);
			      var idIndicador = li.val();
			      var txtIndicador = li.attr('texto');
			      var txtReplace = 'L'+idIndicador;
			      textoResult = textoResult.replace(txtReplace, txtIndicador);
			});
			
			$('#txtCondicionResultado').val(textoResult);
			
		}

		function crearExperimento(){
			var idHipotesis = $("#idHipotesis").text();
			var idProyecto = $("#idProyecto").text();
			var idExperimento = $("#idExperimento").text();
			
			var listPruebas = $("#list-info a.active");		
			var idTipoPrueba = $(listPruebas).attr('idprueba');


			/*Metricas*/
			var metricas = [];
			var listMetricas = $('#style-2 input:checked');
			listMetricas.each(function(index) {
				var idMetrica = $(this).val();
				metricas.push(idMetrica);
			});


			/*Condiciones*/
			var listCondiciones = $('#list_condiciones li');
			var condiciones = [];
			if(listCondiciones.length == 0){
				$("#msgErrorCondicion").show();
			}else{
				listCondiciones.each(function(index) {
					var condi = $(this).attr('condicion');
					var descri = $(this).attr('descripcion');
					 condiciones.push({"txtCondicion": condi ,  "descripcion": descri});
				});
				
				var crearExperimento = {
						idHipotesis: idHipotesis,
						idExperimento: idExperimento,
						idTipoPrueba: idTipoPrueba,
						metricas: metricas,
						condiciones: condiciones,
						idProyecto: idProyecto
				}

				
				crearExperimentoWs(crearExperimento);
					
			}
			
						
		}

		$('#list-info a').on('click', function (e) {
			  e.preventDefault()
			  $(this).tab('show')
			})
			
	 	function setProgressBar(curStep){
	        var percent = parseFloat(100 / 4) * curStep;
	        percent = percent.toFixed();
	        $(".progress-bar")
	        .css("width",percent+"%")
    	}

		function iniciarExperimento(){
			//Deberia guardar el experimento y redirigir al detalle
			var idHipotesis = $("#idHipotesis").text();
			var idProyecto = $("#idProyecto").text();
			var idExperimento = $("#idExperimento").text();
			window.location.href="/experimento/detalle?id="+idProyecto+"&idExperimento="+idHipotesis;
		}

	    $(document).on('click', '#x', function() {
		
		   $(this).closest('li').remove();
		});
		
      </script>
</body>
</html>