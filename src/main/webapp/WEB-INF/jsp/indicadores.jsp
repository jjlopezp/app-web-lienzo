<%@ taglib prefix="c" uri="http://www.hdiv.org/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Lienzanier</title>
<link href='http://fonts.googleapis.com/css?family=Headland+One' rel='stylesheet' type='text/css'>
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/canvas.css">
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/menu.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/steps.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/options.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">    
    <link href="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.css" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    
    
    <style>
    	#msform input:not([type='checkbox']) {
		    padding: 8px 15px 8px 15px;
		    border: 1px solid #ccc;
		    border-radius: 0px;
		    margin-bottom: 25px;
		    margin-top: 2px;
		    width: 100%;
		    box-sizing: border-box;
		    font-family: montserrat;
		    color: #2C3E50;
		    background-color: #ECEFF1;
		    font-size: 16px;
		    letter-spacing: 1px
		}
		
		#msform label {
			color: #000;
			font-size: 23px;
		}
		
		.SG .box{
			height: auto !important;
			max-height: 800px !important; 
		}
		
		h3{
			font-weight: bold !important;
		}
		
		.info-indicador{
			text-align: justify;
		}
		
		.formula{
			border: 2px solid gray;
		    background-color: aliceblue;
		    margin: 10px 0px;
		}
		
		.scrollbar{
			width: 100% !important;
		    max-height: 300px !important;
		    margin-left: 0px !important;
		    height: auto !important;

		}
		
		.modal-content{
			max-width: 800px !important;
    		width: 800px !important;
		}
		
/* 		label+label{ */
/* 			margin-left: 15px; */
/* 		} */
    </style>
    
</head>
<body>
	<header>
		<jsp:include page='header.jsp'/>
	</header>

	<div id="main2">
		<section class="mr-ri mr-lf">
			<h1>Crea tus indicadores!</h1>
			<div>
				<button type="button" onClick="openModalCrearIndicador()" class="swal2-confirm swal2-styled" style="background-color:#9e1b32;">+ Crear indicador</button>
			</div>
			
			<ul class="SG">
			
				<c:out value="${indicadoresHtml}" escapeXml="false"/>
			
<!-- 				<li class="sgLi"> -->
<!-- 					<div class="box"> -->
<!-- 						<h3>Average Order Value</h3> -->
<!-- 						<p class="info-indicador"> -->
<!-- 							Se refiere al valor medio del carrito de compra de los clientes de una tienda online. Es decir, informa acerca de la cantidad de  -->
<!-- 							dinero que gastan los usuarios como promedio en nuestro ecommerce. -->
<!-- 						</p> -->
<!-- 						<p class="formula">Total ingresos / #ordenes</p> -->
<!-- 						<div> -->
<!-- 							<i class="fas fa-trash fa-2x"></i> -->
<!-- 						</div> -->
<!-- 					</div> -->
										
<!-- 				</li> -->
				

			</ul>
            
     
            
		</section>
		
		<!-- 						<TABLE STYLE="display:inline-table; font-size:65%; border-collapse:collapse; vertical-align:middle"> -->
<!-- 							<TR> -->
<!-- 								<TD STYLE="padding:0px; border-bottom: solid 1px; width:8px; text-align:center">1</TD> -->
<!-- 							</TR>  -->
<!-- 							<TR> -->
<!-- 								<TD STYLE="padding:0px; text-align:center">x</TD> -->
<!-- 							</TR> -->
<!-- 						</TABLE> -->
		
		
		<!--Modal nueva metrica-->
        <div class="modal fade" id="modal-nuevo-indicador" role="dialog" style="height: auto;">
            <div class="modal-dialog" id="msform">
                <div class="modal-content">
                    <div class="modal-header">
                        <div>
                            <label>Nuevo indicador</label>
                        </div>
                    </div>
                    <div class="modal-body pop-inner">
                        <label>Nombre indicador</label>
                        <input id="txtNewNomIndicador" type="text" class="form-nw-proy">
                        <label class="block">Descripción</label>
                        <textarea id="txtNewDescripcion" class="form-nw-proy"></textarea>
                        
                        <label class="block">Métricas</label>
                        <div class="flex-lienzo flex-column scrollbar" id="style-2">
<!--                         	<ul> -->
                        		<c:out value="${metricasHtml}" escapeXml="false"/>
<!--                         		<li> -->
<!--                         			<label> -->
<!--                         				Total de ingresos -->
<!--                         			</label> -->
<!--                         			<label> -->
<!--                         				L10 -->
<!--                         			</label> -->
<!--                         		</li> -->
<!--                         		<li> -->
<!--                         			<label> -->
<!--                         				Total de ingresos -->
<!--                         			</label> -->
<!--                         			<label> -->
<!--                         				L10 -->
<!--                         			</label> -->
<!--                         		</li> -->
<!--                         		<li> -->
<!--                         			<label> -->
<!--                         				Total de ingresos -->
<!--                         			</label> -->
<!--                         			<label> -->
<!--                         				L10 -->
<!--                         			</label> -->
<!--                         		</li> -->
<!--                         		<li> -->
<!--                         			<label> -->
<!--                         				Total de ingresos -->
<!--                         			</label> -->
<!--                         			<label> -->
<!--                         				L10 -->
<!--                         			</label> -->
<!--                         		</li> -->
<!--                         	</ul> -->
                        	
						</div>
						<label>Escribe el indicador</label>
						<textarea class="form-control input-valor" id="txtCondicion"></textarea>
						
						
                    </div>
                    <div style="margin:auto;">
                            <button type="button" onClick="crearNuevoIndicador()" class="swal2-confirm swal2-styled" style="background-color:#9e1b32;">Aceptar</button>
                            <button type="button" onClick="" class="swal2-cancel swal2-styled" style="background-color:#bdbdbd;">Cancelar</button>                                  
                    </div>
                </div>
            </div>
        </div>
		
		<!--Modal edit proyecto-->
        <div class="modal fade" id="modal-edit-metrica" role="dialog" style="height: auto;">
            <div class="modal-dialog" id="msform">
                <div class="modal-content">
                    <div class="modal-header">
                        <div>
                            <label>Editar métrica</label>
                        </div>
                    </div>
                    <div class="modal-body pop-inner">
                        <label id="idMetrica" style="display:none;">id</label>
                        <label>Nombre metrica</label>
                        <input id="txtEditNomMetrica" type="text" class="form-nw-proy"> 
                        <label class="block">Nombre corto</label>
                        <input id="txtEditNombreCorto" type="text" class="form-nw-proy"></input>
                    </div>
                    <div style="margin:auto;">
                            <button type="button" onClick="confirmEditarMetrica()" class="swal2-confirm swal2-styled" style="background-color:#9e1b32;">Aceptar</button>
                            <button type="button" onClick="" class="swal2-cancel swal2-styled" style="background-color:#bdbdbd;">Cancelar</button>                                  
                    </div>
                </div>
            </div>
        </div>
		<label id="idProyecto" style="display:none;">${idProyecto}</label>
	</div>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.js"></script>

	<!-- SweetAlert2 --> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ws/endpoint.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/loginService.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/proyectoService.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ws/endpoint.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/canvas.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/menu.js"></script>
	<script>
          $(document).on( 'click', '.salir', function(){
            sessionStorage.removeItem('user');
            window.location.href = '/login';
          })
        </script>
	<script>
	$(document).ready(function() {
	      $("#indicadores").addClass("active");
	});


		$('[id=txtCondicion]').on('keypress', function (event) {
		    var regex = new RegExp("[0-9()%L<>+\-\/*]");
		    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
		    if (!regex.test(key)) {
		       event.preventDefault();
		       return false;
		    }
		});

		function crearNuevoIndicador(){
			var txt_indicador = $("#txtCondicion").val();
			var nombre = $("#txtNewNomIndicador").val();
			var descripcion = $("#txtNewDescripcion").val();
			var idProyecto = $("#idProyecto").text();
			
			/*Metricas*/
			var metricas = [];
			var listMetricas = $('#style-2 input:checked');
			listMetricas.each(function(index) {
				var idMetrica = $(this).val();
				metricas.push(idMetrica);
			});
			
			var data = {
						nombre: nombre,
						descripcion: descripcion,
						txt_indicador: txt_indicador,
						metricasInt: metricas,
						idProyecto: idProyecto
					}
			console.log('Indicador nuevo');
			console.log(data);
			crearIndicadorWs(data);
		}
	
		
	    


        function openModalCrearIndicador(){
        	
	        $("#modal-nuevo-indicador").modal();
        }

       
	    
	     $(document).on( 'click', '.remMetrica', function(){
            swal.fire(
                {
                    title: 'Eliminar métrica',
                    text: 'Por favor, confirme su elección',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#9e1b32',
                    cancelButtonColor: '#bdbdbd',
                    confirmButtonText: 'Confirmar',
                    cancelButtonText: 'Cancelar'
                }
            ).then((result) => {
                if (result.value) {
                    var current_row = $(this).parents('tr');
                    var value = current_row.attr('value');
                    deleteMetricaWs(value);                 
                }
            })
        })
	
	</script>
	
</body>
</html>