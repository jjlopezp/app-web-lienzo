<%@ taglib prefix="c" uri="http://www.hdiv.org/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Lienzanier</title>
<link href='http://fonts.googleapis.com/css?family=Headland+One' rel='stylesheet' type='text/css'>
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/canvas.css">
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/menu.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/steps.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">    
    <link href="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.css" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    
    
    <style>
    	#msform input {
		    padding: 8px 15px 8px 15px;
		    border: 1px solid #ccc;
		    border-radius: 0px;
		    margin-bottom: 25px;
		    margin-top: 2px;
		    width: 100%;
		    box-sizing: border-box;
		    font-family: montserrat;
		    color: #2C3E50;
		    background-color: #ECEFF1;
		    font-size: 16px;
		    letter-spacing: 1px
		}
		
		#msform label {
			color: #000;
			font-size: 23px;
		}
    </style>
    
</head>
<body>
	<header>
		<jsp:include page='header.jsp'/>
	</header>

	<div id="main2">
		<section class="mr-ri mr-lf">
			<h1>Métricas</h1>
			<div class="mr-ri">
                <table id="listProyectos" class="table table-striped">
                    <thead>  
               	       <tr>
	               	       	<td>#</td>
	               	       	<td>Métrica</td>
	               	       	<td>Nombre corto</td>
	               	       	<td>Acciones</td>
               	       </tr>              
                        
                    </thead>
                    <tbody>
                           <c:out value="${metricas}" escapeXml="false"/>
                    </tbody>
                </table>
            </div>
            <div>
            	<button type="button" onClick="openModalNewMetrica()" class="swal2-confirm swal2-styled" style="background-color:#9e1b32;">Crear métrica</button>
            </div>
		</section>
		
		<!--Modal nueva metrica-->
        <div class="modal fade" id="modal-nueva-metrica" role="dialog" style="height: auto;">
            <div class="modal-dialog" id="msform">
                <div class="modal-content">
                    <div class="modal-header">
                        <div>
                            <label>Nueva métrica</label>
                        </div>
                    </div>
                    <div class="modal-body pop-inner">
                        <label>Nombre metrica</label>
                        <input id="txtNewNomMetrica" type="text" class="form-nw-proy">
                        <label class="block">Nombre corto</label>
                        <input id="txtNewNombreCorto" type="text" class="form-nw-proy"></input>
                    </div>
                    <div style="margin:auto;">
                            <button type="button" onClick="confirmNewMetrica()" class="swal2-confirm swal2-styled" style="background-color:#9e1b32;">Aceptar</button>
                            <button type="button" onClick="" class="swal2-cancel swal2-styled" style="background-color:#bdbdbd;">Cancelar</button>                                  
                    </div>
                </div>
            </div>
        </div>
		
		<!--Modal edit proyecto-->
        <div class="modal fade" id="modal-edit-metrica" role="dialog" style="height: auto;">
            <div class="modal-dialog" id="msform">
                <div class="modal-content">
                    <div class="modal-header">
                        <div>
                            <label>Editar métrica</label>
                        </div>
                    </div>
                    <div class="modal-body pop-inner">
                        <label id="idMetrica" style="display:none;">id</label>
                        <label>Nombre metrica</label>
                        <input id="txtEditNomMetrica" type="text" class="form-nw-proy"> 
                        <label class="block">Nombre corto</label>
                        <input id="txtEditNombreCorto" type="text" class="form-nw-proy"></input>
                    </div>
                    <div style="margin:auto;">
                            <button type="button" onClick="confirmEditarMetrica()" class="swal2-confirm swal2-styled" style="background-color:#9e1b32;">Aceptar</button>
                            <button type="button" onClick="" class="swal2-cancel swal2-styled" style="background-color:#bdbdbd;">Cancelar</button>                                  
                    </div>
                </div>
            </div>
        </div>
		<label id="idProyecto" style="display:none;">${idProyecto}</label>
	</div>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.js"></script>

	<!-- SweetAlert2 --> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ws/endpoint.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/loginService.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/proyectoService.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ws/endpoint.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/canvas.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/menu.js"></script>
	<script>
          $(document).on( 'click', '.salir', function(){
            sessionStorage.removeItem('user');
            window.location.href = '/login';
          })
        </script>
	<script>
	$(document).ready(function() {
	      $("#metricas").addClass("active");
	});

	
		$(document).on( 'click', '.editProy', function(){
	        var current_row = $(this).parents('tr');
	        var nombre = current_row.find("td:eq(1)").text();
	        var descripcion = current_row.find("td:eq(2)").text(); 
	        var id = current_row.attr('value');           
	        fillModalEditMetrica(nombre, descripcion, id);
	        $("#modal-edit-metrica").modal();
	    })
 
	    function fillModalEditMetrica(nombre, nombreCorto, id){
            $('#txtEditNomMetrica').val('');
            $('#txtEditNombreCorto').val('');
            $('#idMetrica').val('');
            $('#txtEditNomMetrica').val(nombre);
            $('#txtEditNombreCorto').val(nombreCorto);
            $('#idMetrica').val(id);
        }

        function confirmEditarMetrica(){
        	var nombre = $('#txtEditNomMetrica').val();
            var nombreCorto = $('#txtEditNombreCorto').val();
            var id = $('#idMetrica').val();
            var data = {
						nombre: nombre,
						nombreCorto: nombreCorto,
						id: id
                    }
            editMetricaWS(data);
            $('#modal-edit-metrica').modal('toggle');
        }

        function openModalNewMetrica(){
        	$('#txtNewNomMetrica').val('');
        	$('#txtNewNombreCorto').val(''); 
        	
	        $("#modal-nueva-metrica").modal();
        }

        function confirmNewMetrica(){
        	var nombre = $('#txtNewNomMetrica').val();
            var nombreCorto = $('#txtNewNombreCorto').val();
            var id = $('#idProyecto').text();
            var data = {
						nombre: nombre,
						nombreCorto: nombreCorto,
						idProyecto: id
                    }
            saveMetricaWS(data);
            $('#modal-nueva-metrica').modal('toggle');
        }
	    
	     $(document).on( 'click', '.remMetrica', function(){
            swal.fire(
                {
                    title: 'Eliminar métrica',
                    text: 'Por favor, confirme su elección',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#9e1b32',
                    cancelButtonColor: '#bdbdbd',
                    confirmButtonText: 'Confirmar',
                    cancelButtonText: 'Cancelar'
                }
            ).then((result) => {
                if (result.value) {
                    var current_row = $(this).parents('tr');
                    var value = current_row.attr('value');
                    deleteMetricaWs(value);                 
                }
            })
        })
	
	</script>
	
</body>
</html>