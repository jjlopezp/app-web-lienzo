<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Lienzanier</title>
<meta name="google-signin-client_id" content="947699620439-f0h77pkjt32as9f0ub6f773lfkvfr1ch.apps.googleusercontent.com">

<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/sesion.css">
<link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


</head>
<body>

		

		<div class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto">
		    <div class="card card0 border-0">
		        <div class="row d-flex">
		            <div class="col-lg-6">
		                <div class="card1 pb-5">
		                    <div class="row"> <a href="/"><img src="https://i.imgur.com/CXQmsmF.png" class="logo"></a> </div>
		                    <div class="row px-3 justify-content-center mt-4 mb-5 border-line"><a href="/"><img src="https://i.imgur.com/uNGdWHi.png" class="image"></a>  </div>
		                </div>
		            </div>
		            <div class="col-lg-6">
		                <div class="card2 card border-0 px-4 py-5">
		                    <div class="row mb-4 px-3">
		                        <h6 class="mb-0 mr-4 mt-2">Ingresa con</h6>
		                        
		                        
	                            <div style="margin-left: 0%;" class="g-signin2" data-onsuccess="onSignIn"></div>
		                       
		                        
		                    </div>
		                    <div class="row px-3 mb-4">
		                        <div class="line"></div> <small class="or text-center">O</small>
		                        <div class="line"></div>
		                    </div>
		                    <div class="row px-3"> <label class="mb-1">
		                            <h6 class="mb-0 text-sm">Usuario</h6>
		                        </label> <input class="mb-4" id="usuarioid" type="text" name="login" placeholder="Ingresa tu usuario"> </div>
		                       
		                        
		                        
		                    <div class="row px-3"> <label class="mb-1">
		                            <h6 class="mb-0 text-sm">Contraseña</h6>
		                        </label> <input type="password" id="passwordid" name="password" placeholder="Ingresa contraseña"> </div>
<!-- 		                    <div class="row px-3 mb-4"> -->
<!-- 		                        <div class="custom-control custom-checkbox custom-control-inline"> <input id="chk1" type="checkbox" name="chk" class="custom-control-input"> <label for="chk1" class="custom-control-label text-sm">Remember me</label> </div> <a href="#" class="ml-auto mb-0 text-sm">Forgot Password?</a> -->
<!-- 		                    </div> -->
							<p id="msgUserNoValido" style="display:none"><span style="color:red">Credenciales inválidas</span></p>
		                    <div class="row mb-3 px-3"> <button  onclick="login()" type="submit" class="btn btn-blue text-center">Ingresar</button> </div>
		                    <div class="row mb-4 px-3"> <small class="font-weight-bold">No tienes cuenta? <a  href="/registro/usuario" class="text-danger ">Regístrate</a></small> </div>
		                </div>
		            </div>
		        </div>
		        <div class="bg-blue py-4">
		            <div class="row px-3"> <small class="ml-4 ml-sm-5 mb-2">Copyright &copy; 2020. All rights reserved.</small>
		                <div class="social-contact ml-4 ml-sm-auto"> <span class="fa fa-facebook mr-4 text-sm"></span> <span class="fa fa-google-plus mr-4 text-sm"></span> <span class="fa fa-linkedin mr-4 text-sm"></span> <span class="fa fa-twitter mr-4 mr-sm-5 text-sm"></span> </div>
		            </div>
		        </div>
		    </div>
		</div>


 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/loginService.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ws/endpoint.js"></script>
	
	<script src="https://apis.google.com/js/platform.js" async defer></script>

</body>
</html>