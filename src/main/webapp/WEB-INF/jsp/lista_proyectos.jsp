<%@ taglib prefix="c" uri="http://www.hdiv.org/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Lienzanier</title>
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
	<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/cards.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/menu.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
    <link href="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.css" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    
    <style>
    	td > a {
    		color: #007bff;
    	}
    	
    	td > a:hover {
    		color: #007bff;
    		text-decoration: none;
    	}
    	
    	h2{
    		font-size: 1rem;
    		display: block !important; 
    		color: black !important;
    	}
    	
    	ul{
    	
    		list-style-type: none;
    	}
    	
    	.pro-ul{
    		border: 2px solid darkgray;
   		    background-color: darkgray;
   		    border-radius: 25px;
    	}
    	
    	ul li{
			width: max-content;
			display: inline-block;
			padding: 10px;
    	}
    	
    	img{
    		display: block;
    		margin: auto;
    	}
    	
    	.mr-ri{
    		margin-top: 20px;
    	}
    	
    	li a{
    		width: 100%;
    	}
    	
    	.modal-dialog{
    		color: black;
   		    max-width: 800px !important;
		    margin: 1.75rem auto !important;
		    max-height: 1500px !important;
		    height: fit-content !important;
    	}
    	
    	.btn-primary.btn-right{
    		width: 100%;
    	}
    	
    	.container {max-width: 850px; width: 100%; margin: 0 auto;}
		.four { width: 32.26%; max-width: 32.26%;}
		
		/* COLUMNS */

.col {
  display: block;
  float:left;
  margin: 1% 0 1% 1.6%;
}

.col:first-of-type { margin-left: 0; }

/* CLEARFIX */

.cf:before,
.cf:after {
    content: " ";
    display: table;
}

.cf:after {
    clear: both;
}

.cf {
    *zoom: 1;
}

.container input{
	display: none;
}

.container label{
	position: relative;
	color: #fff;
	background-color: #aaa;
	font-size: 26px;
	text-align: center;
/* 	height: 150px; */	
/* 	line-height: 150px; */
	display: block;
	cursor: pointer;
	border: 3px solid transparent;
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

.container input:checked + label{
	border: 3px solid #333;
	background-color: #2fcc71;
}

.container .plan input:checked + label:after{
	content: "\2713";
	width: 40px;
	height: 40px;
	line-height: 40px;
	border-radius: 100%;
	border: 2px solid #333;
	background-color: #2fcc71;
	z-index: 999;
	position: absolute;
	top: -10px;
	right: -10px;
}

.plan{
	display:flex;
	margin-bottom: 5px;
}

.info{
	width: 362.13px;
    height: 300px;
    border: 2.5px solid black;
}

#modal-crear-proyecto{
	margin-bottom: 0px !important;
}

ol {
	margin: 2px 20px;
}
    	
    </style>
</head>
<body>
	<header>
		<jsp:include page='header_medio.jsp'/>
	</header>

	<div id="main2">
        <section class="mr-ri mr-lf">
            <h1>Mis Proyectos</h1>
            <button type="button" onClick="openModalCrearProyecto()" class="btn btn-primary">+ Agregar proyecto</button>
            <br>

            
            <section class="cards">
           		 <c:out value="${proyectos}" escapeXml="false"/>

            </section>
            
            
            <!--Modal edit proyecto-->
                <div class="modal fade" id="modal-crear-proyecto" role="dialog" style="height: auto;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div>
                                    <label>Nuevo proyecto</label>
                                </div>
                            </div>
                            <div class="modal-body pop-inner">
                                <form class="form-horizontal" action="">

                                    <div class="form-group"> 
                                        <label for="nombreProyecto" class="">Ingrese nombre de proyecto</label>                                                
                                        <input type="text" class="form-nw-proy" name="nombreProyecto" id="nombreProyecto" onblur="removeErr()"/>                    
                                    </div>
                                    <p id="msgErrNombreProyecto" style="display:none"><span style="color:red">Ingrese un nombre</span></p>
                                    
                                    <div class="form-group">                                        
                                        
                                        <label for="descripProyecto" class="">Descripción (opcional)</label>                                        
                                        <textarea type="text" class="form-control" name="descripProyecto" id="descripProyecto" rows="10" cols="80"></textarea>
                                           
                                    </div>
                                    
                                    <div class="container">
										<h1>¿Que lienzo usarás?</h1>
										
											<section class="plan cf">												
												<input type="radio" name="radio1" id="free" value="free" checked><label class="free-label four col" for="free">Business Model Canvas</label>
												<input type="radio" name="radio1" id="basic" value="basic"><label class="basic-label four col" for="basic">Lean Canvas</label>
											</section>
											<div class="plan">
												<div class="info" style="margin-right: 5px;">
													<ol>
														<li>Resumen visual de los factores clave de un producto o servicio.</li>
														<li>El foco principal está en la relación con los clientes.</li>
														<li>Es importante el conocer las actividades clave, recursos clave y socios clave en el lienzo.</li>
														<li>En el caso que se realice un cambio en cualquiera de los elementos, podría afectar al resto ya que 
															están muy relacionados porque trabajan como un sistema.</li>
													</ol>
												</div>
												<div class="info" style="margin-left: 5px;">
													<ol>
														<li>El foco principal está en la ventaja competitiva, el factor diferencial y los soluciones que ofrece el 
														producto, es decir se centra en aportar valor a los clientes.
														 </li>
														 <li>Nos obliga a tener en cuenta las métricas que utilizaremos para medir nuestro progreso.
																Identifica las fortalezas y amenazas de tu negocio u organizaciones.</li>
														 <li>Refuerza o define tú la identidad de marca.</li>
														 <li>Reduce los riegos derivados de la actividad comercial.</li>
														 
													</ol>
												</div>
											</div>											
										
									</div>
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    <div style="display:none" class="mrg-form">
                                        <span>Integrantes seleccionados</span>
                                        <table id="tblUsuarioSeleccionados" data-toggle="table" class="table-no-borde">
                                            <tbody>
                                                
                                            </tbody>
                                        </table>    
                                    </div>
                                    
                                    <div style="display:none" class="mrg-bot-16">
                                        <span>Buscar integrantes</span>
                                        <input id="txtSearchUsuario" class="form-control my-0 py-1 lime-border" type="text" placeholder="Ingrese correo electrónico" aria-label="Search">
                                        <span  id="btnSearchEmail" onclick='searchUsuarioProy()'><i class="fas fa-search"></i></span>
                                        
                                    </div>
                                    <div style="display:none">
                                        <table id="tblUsuarioBuscados" data-toggle="table">
                                            <tbody>
                                                
                                            </tbody>
                                        </table>                                        
                                    </div>
                                    
                                    
                                    <div class="form-group ">
                                        <button type="button" onClick="registroProyecto()" class="btn btn-primary btn-right">Crear proyecto</button>
                                    </div>
                                </form>       
                            </div>
                        </div>
                    </div>
                </div>
            
            
         
                <!--Modal edit proyecto-->
                <div class="modal fade" id="modal-edit-proyecto" role="dialog" style="height: auto;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div>
                                    <label>Editar proyecto</label>
                                </div>
                            </div>
                            <div class="modal-body pop-inner">
                                <label id="idProyecto" style="display:none;">id</label>
                                <label>Nombre proyecto</label>
                                <input id="txtEditNomProy" type="text" class="form-nw-proy">
                                <label class="block">Descripción</label>
                                <textarea id="txtEditDescripProy" name="" cols="0" rows="8" style="width: 100%;"></textarea>
                            </div>
                            <div style="margin:auto;">
                                    <button type="button" onClick="confirmEditarProyecto()" class="swal2-confirm swal2-styled" style="background-color:#9e1b32;">Aceptar</button>
                                    <button type="button" onClick="" class="swal2-cancel swal2-styled" style="background-color:#bdbdbd;">Cancelar</button>                                  
                            </div>
                        </div>
                    </div>
                </div>

        </section>
    </div>
    
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.js"></script>

	<!-- SweetAlert2 --> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ws/endpoint.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/loginService.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/services/proyectoService.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/menu.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ckeditor.js"></script>
      
<script>
          $(document).on( 'click', '.salir', function(){
            sessionStorage.removeItem('user');
            window.location.href = '/login';
          })
        </script>
 <script>
        window.onload = function() {      
            clearTablSearchUserSeleccionado();
        }

        $('#tblUsuarioSeleccionados').bootstrapTable({
            formatNoMatches: function () {
                return '';
            }
        });

        $('#tblUsuarioSeleccionados').bootstrapTable({
            formatNoMatches: function () {
                return 'No se encontró ningún miembro con su búsqueda';
            }
        });

        $('#tblUsuarioBuscados tbody').on( 'click', 'tr', function () {
            var idUser = ($(this).attr('value'));
            var tr = '<tr id='+idUser+' value='+idUser+'>';
            tr = tr.concat($(this).html());
            tr = tr.concat('<td><i class="fas fa-user-times"></i></td>');
            tr = tr.concat('</tr>');
            $("#tblUsuarioSeleccionados").find('tbody').append(tr);
            clearTablSearchUser();
        } );

        $('#tblUsuarioSeleccionados tbody').on( 'click', 'tr', function () {
            var value = ($(this).attr('value'));
            $('#'+value).remove();
        } );

        $(document).on( 'click', '.remProy', function(){
            swal.fire(
                {
                    title: 'Eliminar proyecto',
                    text: 'Por favor, confirme su elección',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#9e1b32',
                    cancelButtonColor: '#bdbdbd',
                    confirmButtonText: 'Confirmar',
                    cancelButtonText: 'Cancelar'
                }
            ).then((result) => {
                if (result.value) {
                    var current_row = $(this).parents('tr');
                    var value = current_row.attr('value');
                    deleteProyectoWs(value);                 
                }
            })
        })

        $(document).on( 'click', '.editProy', function(){
            var current_row = $(this).parents('tr');
            var nombre = current_row.find("td:eq(0)").text();
            var descripcion = current_row.find("td:eq(1)").text(); 
            var id = current_row.attr('value');           
            fillModalEditProyecto(nombre, descripcion, id);
            $("#modal-edit-proyecto").modal();
        })
        
        function fillModalEditProyecto(nombre, descripcion, id){
            $('#txtEditNomProy').val('');
            $('#txtEditDescripProy').val('');
            $('#idProyecto').val('');
            $('#txtEditNomProy').val(nombre);
            $('#txtEditDescripProy').val(descripcion);
            $('#idProyecto').val(id);
        }

        function reloadTableProyecto(){
            clearTablProyectos();
            getAllProyectosWs();
        }

        function confirmEditarProyecto(){
            var nombre = $('#txtEditNomProy').val();
            var descripcion = $('#txtEditDescripProy').val();
            var id = $('#idProyecto').val();
            editProyectoWS(nombre, descripcion, id);
            $('#modal-edit-proyecto').modal('toggle');
        }

        function openModalCrearProyecto(){
        	$('#modal-crear-proyecto').modal();
         }

        function removeErr(){
        	$("#msgErrNombreProyecto").hide();
        }
        
    </script>
    
     <script>
          // Replace the <textarea id="editor1"> with a CKEditor 4
          // instance, using default configuration.
          CKEDITOR.replace( 'descripProyecto' );
     </script>
	
</body>
</html>