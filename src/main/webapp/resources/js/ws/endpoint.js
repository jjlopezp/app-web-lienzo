function loginWs(usuarioIn, passIn){
    var user = {
        usuario: usuarioIn,
        contrasena: passIn
    }
    var user = JSON.stringify(user); 
    $.ajax({
        url: '/login/sigin',
        type: "POST",
        data: user,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
        	console.log('Retorna de login');
            console.log(data);
            if(data.autenticado)
            	window.location = data.url;
            else{
            	$("#msgUserNoValido").show();
            }	
        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}

function loginWsGmail(nombre, email){
   var user = {
        nombre: nombre,
        email: email
    }
    var user = JSON.stringify(user); 
    $.ajax({
        url: '/login/siginGmail',
        type: "POST",
        data: user,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
        	console.log('Retorna de login');
            console.log(data);
            if(data.autenticado)
            	window.location = data.url;
            console.log('MOSTRAR ERRROR');
        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}

function registerShare(idProyecto, email, correoRevisor, urlShare){
   var user = {
        idProyecto: idProyecto,
        email: email,
        correoRevisor: correoRevisor,
        urlShare: urlShare
    }
    var user = JSON.stringify(user); 
    $.ajax({
        url: '/login/registerShare',
        type: "POST",
        data: user,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
        	Swal.fire(
                'Se compartió con el usuario elegido'
            );
        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}


function editProyectoWS(nombre, descripcion, id){
    var proyecto = {
        id: id,
        nombre: nombre,
        descripcion: descripcion
    }
    var proyecto = JSON.stringify(proyecto);
    $.ajax({
        url: '/proyecto/updateProy',
        type: "POST",
        data: proyecto,
        contentType: "application/json; charset=utf-8",
        success: function () {
            Swal.fire(
                'Modificado',
                'Se modificó el registro satisfactoriamente'
            ).then((result) => {
                location.reload(true);
            });
        },
        error: function (error) {
        	console.log(error);
            console.log(`Error ${error}`);
        }
    });
}

function getAllProyectosWs(){
	console.log('SE ACTUALIZARÁ TABLA');
    $.ajax({
        url: '/proyecto/getListProyectos',
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            fillTableListProyectos(data);
            return data;
        },
        error: function (error) {
        	console.log(error);
            console.log(`Error ${error}`);
        }
    });
}

function deleteProyectoWs(idProyect){
    $.ajax({
        url: '/proyecto/deleteById?id='+idProyect,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            Swal.fire(
                'Eliminado',
                'Se eliminó el registro satisfactoriamente'
            ).then((result) => {
                location.reload(true);
            });
            
        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}

function searchUsarioByEmailWs(data){
    
    $.ajax({
        url: '/usuario/getByEmail?email='+data,
        type: "GET",
        data: data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            fillTablSearchUser(data);
            return data;
        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}

function registrarProyectoWs(data){
    var data = JSON.stringify(data);
    $.ajax({
        url: '/proyecto/save',
        type: "POST",
        data: data,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            Swal.fire(
                'Éxito',
                'Se registró el proyecto'
            ).then((result) => {
                location.reload(true);
            });
        },
        error: function (error) {
            console.log(error);
            console.log(`Error ${error}`);
        }
    });
}

function registrarHipotesisWS(data){
	var data = JSON.stringify(data);
	$.ajax({
        url: '/lienzo/save',
        type: "POST",
        data: data,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            Swal.fire(
                'Éxito',
                'Se registró la hipótesis'
            ).then((result) => {
				location.reload(true);
            });
        },
        error: function (error) {
            console.log(error);
            console.log(`Error ${error}`);
        }
    });
}

function editarHipotesisWS(data){
	var data = JSON.stringify(data);
	$.ajax({
        url: '/lienzo/editarHipotesis',
        type: "POST",
        data: data,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            swal.fire('Se guardaron los cambios!').then((result) => {
                window.location = data.url;
            });
        },
        error: function (error) {
            console.log(error);
            console.log(`Error ${error}`);
        }
    });
}

function editMetricaWS(data){
	var data = JSON.stringify(data);
	$.ajax({
        url: '/metrica/update',
        type: "POST",
        data: data,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            Swal.fire(
                'Éxito',
                'Se actualizó la métrica'
            ).then((result) => {
				location.reload(true);
            });
        },
        error: function (error) {
            console.log(error);
            console.log(`Error ${error}`);
        }
    });
}


function deleteMetricaWs(idMetrica){
    $.ajax({
        url: '/metrica/delete?id='+idMetrica,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            Swal.fire(
                'Eliminado',
                'Se eliminó el registro satisfactoriamente'
            ).then((result) => {
                location.reload(true);
            });
            
        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}


function saveMetricaWS(data){
	var data = JSON.stringify(data);
	$.ajax({
        url: '/metrica/save',
        type: "POST",
        data: data,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            Swal.fire(
                'Éxito',
                'Se registró la métrica'
            ).then((result) => {
				location.reload(true);
            });
        },
        error: function (error) {
            console.log(error);
            console.log(`Error ${error}`);
        }
    });
}


function validateUsuarioRegistradoWs(usuario){
    $.ajax({
        url: '/usuario/validateUsuarioRegistrado?usuario='+usuario,
        type: "GET",
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
           console.log('Retorno validar usuario ' + data);
           if(data){
           		$("#msgUsuarioValido").show();
				$("#msgUsuarioNoValido").hide();
       		}else {
       			$("#msgUsuarioValido").hide();
				$("#msgUsuarioNoValido").show();
           	}
        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}

function validateEmailRegistradoWs(usuario){
    $.ajax({
        url: '/usuario/validateEmailRegistrado?email='+usuario,
        type: "GET",
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
        	console.log('Retorno validar usuario ' + data);
           if(data){
           		$("#msgCorreoValido").show();
				$("#msgCorreoNoValido").hide();
       		}else {
       			$("#msgCorreoValido").hide();
				$("#msgCorreoNoValido").show();
           	}
           
           
        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}

function registrarUsuario(usuario, email, password){
    var user = {
        usuario: usuario,
        email: email,
        contrasena: password
    }
    var user = JSON.stringify(user); 
    $.ajax({
        url: '/usuario/save',
        type: "POST",
        data: user,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
        	console.log('Retorna de login');
            console.log(data);
            swal.fire('Registro satisfactorio');
            swal.fire('Registro satisfactorio, inicie sesión con usuario').then((result) => {
                window.location = "/inicia";
            });
            

        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}

function crearExperimentoWs(data){
	var data = JSON.stringify(data); 
    $.ajax({
        url: '/experimento/save',
        type: "POST",
        dataType: "json",
        data: data,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
        	console.log('Retorna de login');
            console.log(data);
            swal.fire('Registro satisfactorio, empiece a experimentar!').then((result) => {
                window.location = data.url;
            });

        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}

function guardarCambiosExperimentoWs(data){
	var data = JSON.stringify(data); 
    $.ajax({
        url: '/editar/guardarCambiosExperimento',
        type: "POST",
        dataType: "json",
        data: data,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
        	console.log('Retorna de login');
            console.log(data);
            swal.fire('Registro satisfactorio, empiece a experimentar!').then((result) => {
                window.location = data.url;
            });

        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}

function updateEstadoExperimentoWs(idExperimento, estado, idProyecto){
	$.ajax({
        url: '/experimento/updateEstado?id='+idExperimento+'&estado='+estado+'&idProyecto='+idProyecto,
        type: "GET",
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
        	console.log('Retorno updateEstadoExperimentoWs');
            console.log(data);
            Swal.fire(
                'En curso',
                'Experimento en curso'
            ).then((result) => {
                window.location = data.url;
            });
           return data;
           
        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}


function saveMedicionesWs(data){
	var data = JSON.stringify(data); 
    $.ajax({
        url: '/experimento/saveMediciones',
        type: "POST",
        dataType: "json",
        data: data,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
        	console.log('Retorna de login');
            console.log(data);
            
            swal.fire('Se registraron los puntos de mediciones!').then((result) => {
            	
                window.location = data.url;
            });

        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}

function updateMedicionesWs(data){
	var data = JSON.stringify(data); 
    $.ajax({
        url: '/experimento/updateMediciones',
        type: "POST",
        dataType: "json",
        data: data,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            swal.fire('Se guardaron los cambios!').then((result) => {
                location.reload(); 
            });

        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}

function finalizarExperimentoWs(idExperimento, idProyecto, idHipotesis){
	var data = JSON.stringify(data); 
    $.ajax({
        url: '/experimento/finalizarExperimento?idHipotesis='+idHipotesis+'&idProyecto='+idProyecto+'&idExperimento='+idExperimento,
        type: "GET",
        dataType: "json",
        data: data,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
        	
            swal.fire('Se finalizó correctamente el experimento, observa los resultados!').then((result) => {
                window.location = data.url;
            });

        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}

function updateEstadoComentarioWs(idComentario, estado){
	var data = JSON.stringify(data); 
    $.ajax({
        url: 'comentario/estado?idComentario='+idComentario+'&estado='+estado,
        type: "GET",
        dataType: "json",
        data: data,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
        	
//            swal.fire('Se finalizó correctamente el experimento, observa los resultados!').then((result) => {
//                window.location = data.url;
//            });

        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}

function saveComentarioProyecto(data){
	var data = JSON.stringify(data); 
    $.ajax({
        url: '/lienzo/saveComentarioProyecto',
        type: "POST",
        dataType: "json",
        data: data,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
//            swal.fire('Se guardaron los cambios!').then((result) => {
//                location.reload(); 
//            });

        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}

function saveComentarioExperimento(data){
	var data = JSON.stringify(data); 
    $.ajax({
        url: '/share/saveComentarioExperimento',
        type: "POST",
        dataType: "json",
        data: data,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
//            swal.fire('Se guardaron los cambios!').then((result) => {
//                location.reload(); 
//            });

        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}

function updateEstadoComentarioDetalleWs(idComentario, estado){
	var data = JSON.stringify(data); 
    $.ajax({
        url: '/share/comentario/detalle/estado?idComentario='+idComentario+'&estado='+estado,
        type: "GET",
        dataType: "json",
        data: data,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
        	
//            swal.fire('Se finalizó correctamente el experimento, observa los resultados!').then((result) => {
//                window.location = data.url;
//            });

        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}

function eliminarProyectoWs(idProyecto){
	console.log('entra eliminarProyectoWs');
    $.ajax({
        url: '/proyecto/deleteById?id='+idProyecto,
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
        	
            swal.fire('Se eliminó el proyecto').then((result) => {
                window.location = data.url;
            });

        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}

function guardarCambiosHipotesisWS(data){
	var data = JSON.stringify(data);
	$.ajax({
        url: '/editar/guardarCambiosHipotesis',
        type: "POST",
        data: data,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            swal.fire('Se guardaron los cambios!').then((result) => {
                window.location = data.url;
            });
        },
        error: function (error) {
            console.log(error);
            console.log(`Error ${error}`);
        }
    });
}


function crearIndicadorWs(data){
	var data = JSON.stringify(data); 
    $.ajax({
        url: '/indicador/save',
        type: "POST",
        dataType: "json",
        data: data,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            swal.fire('Registro satisfactorio, empiece a experimentar!').then((result) => {
            	location.reload(true);
                //window.location = data.url;
            });

        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}

function guardarCambiosIndicadorWs(data){
	var data = JSON.stringify(data); 
    $.ajax({
        url: '/indicador/saveMediciones',
        type: "POST",
        dataType: "json",
        data: data,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            swal.fire('Se guardaron los cambios!').then((result) => {
                location.reload(); 
            });

        },
        error: function (error) {
            console.log(`Error ${error}`);
        }
    });
}
