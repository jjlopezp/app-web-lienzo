package com.tesis.lienzo.app.lienzanier.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.tesis.lienzo.app.lienzanier.bean.Usuario;
import com.tesis.lienzo.app.lienzanier.service.UsuarioService;

@Controller
@RequestMapping("/usuario")
public class UsuarioController {

	
	@Autowired
	UsuarioService usuarioService;
	
	@RequestMapping("/getByEmail")
	@ResponseBody
	@ResponseStatus(value = HttpStatus.OK)
	public List<Usuario> getByEmail(@RequestParam(value="email") String email, Model model, HttpServletRequest request){
		Usuario usuario = ((Usuario) request.getSession().getAttribute("USUARIOSESION"));
		
		return usuarioService.getByEmail(email, usuario.getId().toString());
	}
	
	@RequestMapping("/validateUsuarioRegistrado")
	@ResponseBody
	public boolean validateUsuarioRegistrado(@RequestParam(value="usuario") String usuario) {
		return usuarioService.validateUsuarioRegistrado(usuario);
	}
	
	@RequestMapping("/validateEmailRegistrado")
	@ResponseBody
	public boolean validateEmailRegistrado(@RequestParam(value="email") String usuario) {
		return usuarioService.validateEmailRegistrado(usuario);
	}
	
	@PostMapping("/save")
	@ResponseStatus(value = HttpStatus.OK)
	public void save(@RequestBody Usuario usuario) {
		usuarioService.save(usuario);
	}
	
	@PostMapping("/update")
	public void update(@RequestBody Usuario usuario) {
		usuarioService.update(usuario);
	}
}
