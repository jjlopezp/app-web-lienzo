package com.tesis.lienzo.app.lienzanier.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.tesis.lienzo.app.lienzanier.bean.Indicador;
import com.tesis.lienzo.app.lienzanier.bean.PruebaHipotesis;

@FeignClient(name = "indicador", url = "${ws.lienzo}/indicador")
public interface IndicadorFeignClient {

	@GetMapping("/listar")
	List<Indicador> listar(@RequestParam(value="idProyecto") String idProyecto);

	@PostMapping("/save")
	void save(@RequestBody Indicador indicador);

	@GetMapping("/getIndicador")
	Indicador getIndicador(@RequestParam(value="idIndicador") String idIndicador);

	@PostMapping("/saveMediciones")
	void saveMediciones(@RequestBody Indicador indicador);
}
