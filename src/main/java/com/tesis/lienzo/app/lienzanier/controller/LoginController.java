package com.tesis.lienzo.app.lienzanier.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tesis.lienzo.app.lienzanier.bean.Usuario;
import com.tesis.lienzo.app.lienzanier.service.LoginService;

@Controller
@RequestMapping("/login")
public class LoginController {

	@Autowired
	LoginService loginService;
	
	@RequestMapping(value= "/sigin", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody Map<String,Object> sigin(@RequestBody Usuario usuario, HttpServletRequest request) {
		Usuario u = loginService.login(usuario);
		Map<String, Object> parametros = new HashMap<>();
		if(u.getEmail()!=null) {
			parametros.put("autenticado", true);
			parametros.put("url", request.getContextPath()+"/proyectos/misproyectos");
			request.getSession().setAttribute("USUARIOSESION", u);
		}else {
			parametros.put("autenticado", false);
			parametros.put("url", request.getContextPath()+"/login");
			request.getSession().setAttribute("USUARIOSESION", null);
		}
		return parametros;
		
	}
	
	@RequestMapping(value= "/siginGmail", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody Map<String,Object> siginGmail(@RequestBody Usuario usuario, HttpServletRequest request) {
		System.out.println("ENtra a login con gmail");
		Usuario u = loginService.loginGmail(usuario);
		Map<String, Object> parametros = new HashMap<>();
		if(u.getEmail()!=null) {
			System.out.println("Hay un usuario con el gmail");
			System.out.println(u.getRol());
			parametros.put("autenticado", true);
			if(u.getRol()==1) {
				/*Es usuario emprendedor*/
				parametros.put("url", request.getContextPath()+"/proyectos/misproyectos");
			}else if(u.getRol()==2) {
				/*Es reviador*/
				System.out.println("Entra porue es revisador");
				parametros.put("url", request.getContextPath()+"/proyecto/listar/share");
			}else {
				parametros.put("url", request.getContextPath()+"/proyectos/misproyectos");
			}
			request.getSession().setAttribute("USUARIOSESION", u);
		}else {
			parametros.put("autenticado", false);
			parametros.put("url", request.getContextPath()+"/login");
			request.getSession().setAttribute("USUARIOSESION", null);
		}
		return parametros;
		
	}
	
	@RequestMapping(value= "/registerShare", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody Map<String,Object> registerShare(@RequestBody Usuario usuario, HttpServletRequest request) {
		Usuario u = loginService.registerShare(usuario);
		Map<String, Object> parametros = new HashMap<>();
		if(u.getEmail()!=null) {
			parametros.put("autenticado", true);
			if(u.getRol()==1) {
				/*Es usuario emprendedor*/
				parametros.put("url", request.getContextPath()+"/proyectos/misproyectos");
			}else if(u.getRol()==2) {
				/*Es reviador*/
				parametros.put("url", request.getContextPath()+"/proyectos/misproyectos");
			}
			
			request.getSession().setAttribute("USUARIOSESION", u);
		}else {
			parametros.put("autenticado", false);
			parametros.put("url", request.getContextPath()+"/login");
			request.getSession().setAttribute("USUARIOSESION", null);
		}
		return parametros;
		
	}
	
	
}
