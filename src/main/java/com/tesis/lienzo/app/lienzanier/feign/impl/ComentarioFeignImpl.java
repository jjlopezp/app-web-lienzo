package com.tesis.lienzo.app.lienzanier.feign.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.tesis.lienzo.app.lienzanier.bean.Comentario;
import com.tesis.lienzo.app.lienzanier.feign.ComentarioFeign;

@Component
public class ComentarioFeignImpl implements ComentarioFeign{


	@Override
	public List<Comentario> getComentariosByProyecto(String idProyecto) {
		// TODO Auto-generated method stub
		return new ArrayList<>();
	}

	@Override
	public void updateEstadoComentario(String idComentario, String estado) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveComentarioProyecto(Comentario comentario) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveComentarioExperimento(Comentario comentario) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Comentario> getComentariosByExperimento(String idExperimento) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Comentario> getComentariosDetalleByProyecto(String idProyecto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateEstadoComentarioExperimento(String idComentario, String estado) {
		// TODO Auto-generated method stub
		
	}

}
