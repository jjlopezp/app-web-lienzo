package com.tesis.lienzo.app.lienzanier.feign.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.tesis.lienzo.app.lienzanier.bean.Hipotesis;
import com.tesis.lienzo.app.lienzanier.bean.NodoArbol;
import com.tesis.lienzo.app.lienzanier.bean.ProyectoArea;
import com.tesis.lienzo.app.lienzanier.dto.HipotesisDto;
import com.tesis.lienzo.app.lienzanier.feign.LienzoFeignClient;

@Component
public class LienzoFeignClientImpl implements LienzoFeignClient{

	@Override
	public List<ProyectoArea> getByProyecto(String idProyecto) {
		// TODO Auto-generated method stub
		return new ArrayList<>();
	}

	@Override
	public String save(HipotesisDto hipotesisDto) {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public HipotesisDto getHipotesisDtoById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void editarHipotesis(HipotesisDto hipotesisDto) {
		
	}

	@Override
	public List<Hipotesis> getHipotesisByProyecto(String idProyecto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void guardarCambiosHipotesis(HipotesisDto hipotesisDto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<NodoArbol> arbolHipotesisByHipotesis(String idProyecto) {
		// TODO Auto-generated method stub
		return null;
	}

}
