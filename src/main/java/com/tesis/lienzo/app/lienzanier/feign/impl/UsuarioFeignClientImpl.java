package com.tesis.lienzo.app.lienzanier.feign.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.tesis.lienzo.app.lienzanier.bean.Usuario;
import com.tesis.lienzo.app.lienzanier.feign.UsuarioFeignClient;

@Component
public class UsuarioFeignClientImpl implements UsuarioFeignClient{

	@Override
	public Usuario login(Usuario usuario) {
		return new Usuario();
	}

	@Override
	public List<Usuario> getByEmail(String email, String current) {
		// TODO Auto-generated method stub
		return new ArrayList<>();
	}

	@Override
	public Usuario loginGmail(Usuario usuario) {
		// TODO Auto-generated method stub
		return new Usuario();
	}

	@Override
	public Boolean validateUsuarioRegistrado(String usuario) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Boolean validateEmailRegistrado(String usuario) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void save(Usuario usuario) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Usuario usuario) {
		// TODO Auto-generated method stub
	}

	@Override
	public List<Usuario> getByIdProyecto(String idProyecto) {
		// TODO Auto-generated method stub
		return new ArrayList<>();
	}

	@Override
	public Usuario registerShare(Usuario usuario) {
		// TODO Auto-generated method stub
		return null;
	}

}
