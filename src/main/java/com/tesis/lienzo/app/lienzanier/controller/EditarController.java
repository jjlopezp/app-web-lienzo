package com.tesis.lienzo.app.lienzanier.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.tesis.lienzo.app.lienzanier.bean.Comentario;
import com.tesis.lienzo.app.lienzanier.bean.Condicion;
import com.tesis.lienzo.app.lienzanier.bean.Experimento;
import com.tesis.lienzo.app.lienzanier.bean.Metrica;
import com.tesis.lienzo.app.lienzanier.bean.ProyectoArea;
import com.tesis.lienzo.app.lienzanier.bean.PruebaHipotesis;
import com.tesis.lienzo.app.lienzanier.dto.ExperimentoDto;
import com.tesis.lienzo.app.lienzanier.dto.HipotesisDto;
import com.tesis.lienzo.app.lienzanier.service.ComentarioService;
import com.tesis.lienzo.app.lienzanier.service.ExperimentoService;
import com.tesis.lienzo.app.lienzanier.service.LienzoService;
import com.tesis.lienzo.app.lienzanier.service.MetricaService;
import com.tesis.lienzo.app.lienzanier.util.Constantes;

@Controller
@RequestMapping("/editar")
public class EditarController {

	@Autowired
	LienzoService lienzoService;
	
	@Autowired
	ExperimentoService experimentoService;
	
	@Autowired
	ComentarioService comentarioService;
	
	@Autowired
	MetricaService metricaService;
	
	@RequestMapping("/hipotesis")
	public ModelAndView hipotesis(@RequestParam(value="id") String id, @RequestParam(value="idPro") String idProyecto, @RequestParam(value="idExperimento", required=false) String idExperimento, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView("editar_hipotesis");
		
		HipotesisDto hipotesisDto = lienzoService.getHipotesisDtoById(id);	
		filltableAreaEdit(idProyecto, mv, hipotesisDto.getProyectoAreas());
		
		mv.addObject("fechaRegistro", hipotesisDto.getFechaRegistro());
		mv.addObject("hipotesisDto", hipotesisDto);
		

		mv.addObject("idProyecto", idProyecto);
		mv.addObject("idExperimento", idExperimento);
		
		return mv;
	}
	
	@RequestMapping("/experimento")
	public ModelAndView experimento(@RequestParam(value="id") String idHipotesis, @RequestParam(value="idPro") String idProyecto, @RequestParam(value="idExperimento", required=false) String idExperimento, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView("editar_experimento");
		Experimento experimento = experimentoService.getDetalleExperimento(idExperimento);
		
		String optionIndicadores = getOptionMetricas(idProyecto, experimento.getMetricas(), experimento.getEstado());
		
		String condiciones = "";

		for(Condicion co : experimento.getCondiciones()) {
			
			//condiFake = condiFake.replace('<','&lt;');
			//condiFake = condiFake.replace('>','&gt;');
			
			condiciones = condiciones.concat("<li condicion='"+co.getTxtCondicion()+"' descripcion='"+co.getDescripcion()+"' class='sgLi'>");
			condiciones = condiciones.concat("<div class='box'>");
			condiciones = condiciones.concat("<h3>"+co.getDescripcion()+"</h3>");
			String condicionStr = "";
			for(Metrica me: experimento.getMetricas()) {
				condicionStr = co.getTxtCondicion().replace("L"+me.getId(), me.getNombreCorto());
			}
			condiciones = condiciones.concat("<p>"+condicionStr+"</p>");
			
			condiciones = condiciones.concat("<div id='orangeBox'>");	
			condiciones = condiciones.concat("<span id='x'>X</span>");	
			condiciones = condiciones.concat("</div>");
			condiciones = condiciones.concat("</div>");
			condiciones = condiciones.concat("</li>");

		}
		
		getPruebasHipotesis(mv);
		HipotesisDto hipotesisDto = lienzoService.getHipotesisDtoById(idHipotesis);	
		mv.addObject("optionIndicadores", optionIndicadores);
		mv.addObject("condiciones", condiciones);
		mv.addObject("idHipotesis", idHipotesis);
		mv.addObject("idProyecto", idProyecto);
		mv.addObject("hipotesisDto", hipotesisDto);
		mv.addObject("idExperimento", idExperimento);
		mv.addObject("estadoExperimento", experimento.getEstado());
		return mv;
	}
	
	@PostMapping("/guardarCambiosHipotesis")
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody Map<String,Object> editarHipotesis(@RequestBody HipotesisDto hipotesisDto, HttpServletRequest request) {
		Map<String, Object> parametros = new HashMap<>();
		lienzoService.guardarCambiosHipotesis(hipotesisDto);
		System.out.println("LLega estado");
		System.out.println(hipotesisDto.getEstado());
		//Estado == 2 -> crear experimento
		if(hipotesisDto.getEstado() == Constantes.ESTADO_HIPOTESIS_CREAR_EXPERIMENTO) {
			parametros.put("url", request.getContextPath()+"/experimento?id="+hipotesisDto.getIdProyecto()+"&idHipotesis="+hipotesisDto.getId());
		}else if(hipotesisDto.getEstado() == Constantes.ESTADO_EXPERIMENTO_EN_CURSO_NUEVO) {
			parametros.put("url", request.getContextPath()+"/experimento/detalle?id="+hipotesisDto.getIdProyecto()+"&idExperimento="+hipotesisDto.getIdExperimento());
		}
		
		
		return parametros;
	}
	
	@PostMapping("/guardarCambiosExperimento")
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody Map<String,Object> guardarCambiosExperimento(@RequestBody ExperimentoDto experimentDto, HttpServletRequest request) {
		Map<String, Object> parametros = new HashMap<>();
		experimentoService.guardarCambiosExperimento(experimentDto);
//		//Estado == 2 -> crear experimento
		if(experimentDto.getEstado() == Constantes.ESTADO_EXPERIMENTO_EN_CURSO_NUEVO) {
			parametros.put("url", request.getContextPath()+"/experimento/detalle?id="+experimentDto.getIdProyecto()+"&idExperimento="+experimentDto.getId());
		}else if(experimentDto.getEstado() == Constantes.ESTADO_EXPERIMENTO_EN_CURSO_VIEJO) {
			parametros.put("url", request.getContextPath()+"/experimento/agenda?id="+experimentDto.getIdProyecto()+"&idExperimento="+experimentDto.getId());
		}
		
		
		return parametros;
	}
	
	private void getPruebasHipotesis(ModelAndView mv) {
		List<PruebaHipotesis> pruebas = experimentoService.getPruebasHipotesis();
		String header = "";
		String html = "";
		int i =0;
		for(PruebaHipotesis ph: pruebas) {
			if(i==0) {
				header = header.concat("<a idPrueba='"+ph.getId()+"'  class='list-group-item list-group-item-action active' id='"+ph.getNombreList()+"' data-toggle='list' href='#"+ph.getNombreUpper()+"' role='tab' aria-controls='"+ph.getNombreUpper()+"'>"+ph.getNombre()+"</a>");
				html = html.concat("<div class='tab-pane fade show active border-gray' id='"+ph.getNombreUpper()+"' role='tabpanel' aria-labelledby='"+ph.getNombreList()+"'>");
				html = html.concat("<p>"+ph.getDescripcion()+"</p>");
				html = html.concat("</div>");
				
				i++;	
			}else {
				header = header.concat("<a idPrueba='"+ph.getId()+"'  class='list-group-item list-group-item-action' id='"+ph.getNombreList()+"' data-toggle='list' href='#"+ph.getNombreUpper()+"' role='tab' aria-controls='"+ph.getNombreUpper()+"'>"+ph.getNombre()+"</a>");
				html = html.concat("<div class='tab-pane fade border-gray' id='"+ph.getNombreUpper()+"' role='tabpanel' aria-labelledby='"+ph.getNombreList()+"'>");
				html = html.concat("<p>"+ph.getDescripcion()+"</p>");
				html = html.concat("</div>");
			}
		}
		mv.addObject("headerPruebas", header);
		mv.addObject("cuerpoPruebas", html);
	}
	
	private String getOptionMetricas(String idProyecto, List<Metrica> metricasExperimento, int estado) {
		List<Metrica> metricas = metricaService.getByProyecto(idProyecto);
		String html = "";
		
		for(Metrica me: metricas) {
			
	
			
			html = html.concat("<label>");
			boolean encontrado = false;
			for(Metrica meExp: metricasExperimento) {
				if(me.getId() == meExp.getId()) {
					
					if(estado == Constantes.ESTADO_EXPERIMENTO_EN_CURSO_VIEJO) {
						html = html.concat(" <input value='"+me.getId()+"' texto='"+me.getNombreCorto()+"' type='checkbox' disabled class='option-input checkbox' checked />");
					}else {
						html = html.concat(" <input value='"+me.getId()+"' texto='"+me.getNombreCorto()+"' type='checkbox' class='option-input checkbox' checked />");
					}
					
					html = html.concat(me.getNombreCorto());
					html = html.concat("</label>");
					encontrado = true;
					break;
					
				}						
			}
			
			if(!encontrado) {
				
				if(estado == Constantes.ESTADO_EXPERIMENTO_EN_CURSO_VIEJO) {
					html = html.concat(" <input value='"+me.getId()+"' texto='"+me.getNombreCorto()+"' type='checkbox' disabled class='option-input checkbox'/>");
				}else {
					html = html.concat(" <input value='"+me.getId()+"' texto='"+me.getNombreCorto()+"' type='checkbox' class='option-input checkbox'/>");
				}
				
				
				html = html.concat(me.getNombreCorto());
				html = html.concat("</label>");
			}
			
			
		}
		return html;
	}
	
	private boolean areaSeleccionada(int idArea, List<ProyectoArea> proyectoArea) {
		for(ProyectoArea pA : proyectoArea) {
			if(pA.getIdArea() == idArea)
				return true;
		}
		return false;
	}
	
	private void filltableAreaEdit(String idProyecto, ModelAndView mv, List<ProyectoArea> proyectoArea) {
		List<ProyectoArea> listProyectoArea = lienzoService.getLienzoByProyecto(idProyecto);

		String sociosClave ="";
		String actividadesClave = "";
		String recursosClave = "";
		String propuestaValor ="";
		String relacionClientes ="";
		String canales = "";
		String segmentosClientes ="";
		String estructuraCostos = "";
		String fuentesIngreso ="";
		for(ProyectoArea pA : listProyectoArea) {
			if(pA.getIdArea() == Constantes.ID_SOCIOS_CLAVE) {
				
				if(areaSeleccionada(pA.getIdArea(), proyectoArea)) 
					sociosClave = sociosClave.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_sociosClave' class='checkbox-circle' checked>");
				else
					sociosClave = sociosClave.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_sociosClave' class='checkbox-circle'>");
				sociosClave = sociosClave.concat("<label for='check_sociosClave'>"
													+ "    <div class='tick_mark'></div>"
													+ "</label>");
				

				
			}else if (pA.getIdArea() == Constantes.ID_ACTIVIDADES_CLAVE){
				
				if(areaSeleccionada(pA.getIdArea(), proyectoArea)) 
					actividadesClave = actividadesClave.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_actividadesClave' class='checkbox-circle' checked>");
				else
					actividadesClave = actividadesClave.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_actividadesClave' class='checkbox-circle'>");
				actividadesClave = actividadesClave.concat("<label for='check_actividadesClave'>"
													+ "    <div class='tick_mark'></div>"
													+ "</label>");
				

				
			}else if (pA.getIdArea() == Constantes.ID_RECURSOS_CLAVE){
				if(areaSeleccionada(pA.getIdArea(), proyectoArea)) 
					recursosClave = recursosClave.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_recursosClave' class='checkbox-circle' checked>");
				else
					recursosClave = recursosClave.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_recursosClave' class='checkbox-circle'>");
				recursosClave = recursosClave.concat("<label for='check_recursosClave'>"
													+ "    <div class='tick_mark'></div>"
													+ "</label>");
				
			}else if (pA.getIdArea() == Constantes.ID_PROPUESTA_VALOR){
				
				if(areaSeleccionada(pA.getIdArea(), proyectoArea)) 
					propuestaValor = propuestaValor.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_propuestaValor' class='checkbox-circle' checked>");
				else
					propuestaValor = propuestaValor.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_propuestaValor' class='checkbox-circle'>");
				propuestaValor = propuestaValor.concat("<label for='check_propuestaValor'>"
													+ "    <div class='tick_mark'></div>"
													+ "</label>");
				
			}else if (pA.getIdArea() == Constantes.ID_RELACION_CLIENTES){
				
				if(areaSeleccionada(pA.getIdArea(), proyectoArea)) 
					relacionClientes = relacionClientes.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_relacionClientes' class='checkbox-circle' checked>");
				else
					relacionClientes = relacionClientes.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_relacionClientes' class='checkbox-circle'>");
				relacionClientes = relacionClientes.concat("<label for='check_relacionClientes'>"
													+ "    <div class='tick_mark'></div>"
													+ "</label>");
				
			}else if (pA.getIdArea() == Constantes.ID_CANALES){
				
				if(areaSeleccionada(pA.getIdArea(), proyectoArea)) 
					canales = canales.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_canales' class='checkbox-circle' checked>");
				else
					canales = canales.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_canales' class='checkbox-circle'>");
				canales = canales.concat("<label for='check_canales'>"
													+ "    <div class='tick_mark'></div>"
													+ "</label>");
				
			}else if (pA.getIdArea() == Constantes.ID_SEGMENTOS_CLIENTES){
				
				if(areaSeleccionada(pA.getIdArea(), proyectoArea)) 
					segmentosClientes = segmentosClientes.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_segmentacionClientes' class='checkbox-circle' checked>");
				else
					segmentosClientes = segmentosClientes.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_segmentacionClientes' class='checkbox-circle'>");
				segmentosClientes = segmentosClientes.concat("<label for='check_segmentacionClientes'>"
													+ "    <div class='tick_mark'></div>"
													+ "</label>");
				
			}else if (pA.getIdArea() == Constantes.ID_ESTRUCTURA_COSTOS){
				
				if(areaSeleccionada(pA.getIdArea(), proyectoArea)) 
					estructuraCostos = estructuraCostos.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_estructuraCostos' class='checkbox-circle' checked>");
				else
					estructuraCostos = estructuraCostos.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_estructuraCostos' class='checkbox-circle'>");
				estructuraCostos = estructuraCostos.concat("<label for='check_estructuraCostos'>"
													+ "    <div class='tick_mark'></div>"
													+ "</label>");
				
			}else if (pA.getIdArea() == Constantes.ID_FUENTES_INGRESO){
				
				if(areaSeleccionada(pA.getIdArea(), proyectoArea)) 
					fuentesIngreso = fuentesIngreso.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_fuentesIngreso' class='checkbox-circle' checked>");
				else
					fuentesIngreso = fuentesIngreso.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_fuentesIngreso' class='checkbox-circle'>");
				fuentesIngreso = fuentesIngreso.concat("<label for='check_fuentesIngreso'>"
													+ "    <div class='tick_mark'></div>"
													+ "</label>");
			}
		}
		mv.addObject("sociosClave", sociosClave);
		mv.addObject("actividadesClave", actividadesClave);
		mv.addObject("recursosClave", recursosClave);
		mv.addObject("propuestaValor", propuestaValor);
		mv.addObject("relacionClientes", relacionClientes);
		mv.addObject("canales", canales);
		mv.addObject("segmentosClientes", segmentosClientes);
		mv.addObject("estructuraCostos", estructuraCostos);
		mv.addObject("fuentesIngreso", fuentesIngreso);
		
		
	}
	
	private void fillComentarioDetalle(String idExperimento, ModelAndView mv, HttpServletRequest request) {
		// TODO Auto-generated method stub
		String comentariosHtml = "";
		List<Comentario> comentarios = comentarioService.getComentariosByExperimento(idExperimento);
		String liGenerales ="";
		
		for(Comentario co : comentarios) {
			liGenerales = liGenerales.concat("<li idProyecto='"+co.getIdProyecto()+"' idComentario='"+co.getId()+"' class='item'>");
			liGenerales = liGenerales.concat("<span>");
			liGenerales = liGenerales.concat(co.getComentario());
			liGenerales = liGenerales.concat("</span>");
			liGenerales = liGenerales.concat("<button id='btnResuelta'>Marcar como resuelta</button>");
			liGenerales = liGenerales.concat("</li>");
		}
		comentariosHtml = comentariosHtml.concat(liGenerales);
		mv.addObject("comentariosHipotesisHtml", comentariosHtml);
	}
}
