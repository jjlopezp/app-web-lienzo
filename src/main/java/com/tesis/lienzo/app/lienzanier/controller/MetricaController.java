package com.tesis.lienzo.app.lienzanier.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.tesis.lienzo.app.lienzanier.bean.Metrica;
import com.tesis.lienzo.app.lienzanier.bean.Proyecto;
import com.tesis.lienzo.app.lienzanier.bean.Usuario;
import com.tesis.lienzo.app.lienzanier.service.MetricaService;

@Controller
@RequestMapping("/metrica")
public class MetricaController {

	@Autowired
	MetricaService metricaService;
	
	@RequestMapping("/listar")
	public ModelAndView sigin(@RequestParam(value="id") String idProyecto, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView("metricas");
		
		Usuario usuario = ((Usuario) request.getSession().getAttribute("USUARIOSESION"));
		if(usuario==null) {
			return mv;
		}else {
			String metricas = getMetricasByProyecto(idProyecto, request);
			mv.addObject("metricas", metricas);
			mv.addObject("idProyecto", idProyecto);
			return mv;
		}
	}
	
	@PostMapping("/save")
	@ResponseStatus(value = HttpStatus.OK)
	public void save(@RequestBody Metrica metrica) {
		metricaService.save(metrica);
	}
	
	@PostMapping("/update")
	@ResponseStatus(value = HttpStatus.OK)
	public void update(@RequestBody Metrica metrica) {
		metricaService.update(metrica);
	}
	
	@RequestMapping("/delete")
	@ResponseStatus(value = HttpStatus.OK)
	public void deleteProyecto(@RequestParam(value="id") String id) {
		metricaService.delete(id);
	}

	private String getMetricasByProyecto(String idProyecto, HttpServletRequest request) {
		List<Metrica> metricas = metricaService.getByProyecto(idProyecto);
		String html = "";
		int i = 1;
		for(Metrica me : metricas){
			html = html.concat("<tr value=" + me.getId()+">");
//			html = html.concat("<td><a href='"+request.getContextPath()+"/home?id="+pro.getId()+"'>"+pro.getNombre()+"</td>");
			html = html.concat("<td>"+i+++"</td>");
			html = html.concat("<td>"+me.getNombre()+"</td>");
			html = html.concat("<td>"+me.getNombreCorto()+"</td>");
			html = html.concat("<td><i class='editProy fas fa-edit'></i></td>");
			html = html.concat("<td><i class='remMetrica fas fa-trash-alt'></i></td>");
			html = html.concat("</tr>");
		}
		
		return html;
	}
	
	@GetMapping("getByProyecto")
	@ResponseBody
	public List<Metrica> getMetricasByIdProyecto(@RequestParam(value="id") String idProyecto){
		return metricaService.getByProyecto(idProyecto);
	}
	
	
}
