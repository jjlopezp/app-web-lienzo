package com.tesis.lienzo.app.lienzanier.service;

import com.tesis.lienzo.app.lienzanier.bean.Usuario;

public interface LoginService {

	Usuario login(Usuario usuario);

	Usuario loginGmail(Usuario usuario);

	Usuario registerShare(Usuario usuario);
}
