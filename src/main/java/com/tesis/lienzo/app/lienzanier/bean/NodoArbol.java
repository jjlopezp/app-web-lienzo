package com.tesis.lienzo.app.lienzanier.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NodoArbol {

	
	List<NodoArbol> children;
	
	int id;
	String suposicion;
	String conclu;
	String description;
	String name;
	

//	
//	public void printTreeIdented() {		 
//		
//	
//		for (NodoArbol hijo : hijosJson) {
//			
//            if (hijo != null) {      	
//            	
//            	hijo.printTreeIdented();
//            }
//            	 
//		}		 
//	}
}
