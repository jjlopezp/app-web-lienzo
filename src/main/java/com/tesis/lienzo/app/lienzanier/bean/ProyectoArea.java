package com.tesis.lienzo.app.lienzanier.bean;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProyectoArea {

	private int id;
	private int idProyecto;
	private int idArea;
	private List<ProyectoAreaHipotesis> hipotesisXarea;
	private List<Hipotesis> hipotesis;
	private List<Experimento> experimentos;
}
