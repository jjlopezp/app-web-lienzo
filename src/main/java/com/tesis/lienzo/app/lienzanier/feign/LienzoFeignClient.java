package com.tesis.lienzo.app.lienzanier.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.tesis.lienzo.app.lienzanier.bean.Hipotesis;
import com.tesis.lienzo.app.lienzanier.bean.NodoArbol;
import com.tesis.lienzo.app.lienzanier.bean.ProyectoArea;
import com.tesis.lienzo.app.lienzanier.dto.HipotesisDto;


@FeignClient(name = "lienzo", url = "${ws.lienzo}/lienzo")
public interface LienzoFeignClient {

	@GetMapping("/getByProyecto")
	List<ProyectoArea> getByProyecto(@RequestParam(value="id") String idProyecto);

	@PostMapping("/save")
	String save(@RequestBody HipotesisDto hipotesisDto);
	
	@GetMapping("/getHipotesisById")
	public HipotesisDto getHipotesisDtoById(@RequestParam(value="id") String id);

	@PostMapping("/updateHipotesis")
	void  editarHipotesis(@RequestBody HipotesisDto hipotesisDto);

	@GetMapping("/getHipotesisByProyecto")
	List<Hipotesis> getHipotesisByProyecto(@RequestParam(value="id") String idProyecto);

	@PostMapping("/guardarCambiosHipotesis")
	void guardarCambiosHipotesis(@RequestBody HipotesisDto hipotesisDto);

	@GetMapping("/arbolHipotesis")
	List<NodoArbol> arbolHipotesisByHipotesis(@RequestParam(value="idProyecto") String idProyecto);
}
