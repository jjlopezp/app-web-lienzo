package com.tesis.lienzo.app.lienzanier.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.tesis.lienzo.app.lienzanier.bean.Experimento;
import com.tesis.lienzo.app.lienzanier.bean.ProyectoArea;
import com.tesis.lienzo.app.lienzanier.bean.PruebaHipotesis;
import com.tesis.lienzo.app.lienzanier.dto.ExperimentoDto;

@FeignClient(name = "experimento", url = "${ws.lienzo}/experimento")
public interface ExperimentoFeignClient {

	@GetMapping("/getPruebas")
	List<PruebaHipotesis> getByProyecto();

	@PostMapping("/save")
	ResponseEntity<Integer> save(@RequestBody ExperimentoDto experimentDto);

	@GetMapping("/getByProjecto")
	List<Experimento> getExperimentosByProjecto(@RequestParam(value="id") String id);

	@GetMapping("/updateEstado")
	ResponseEntity<String> updateEstado(@RequestParam(value="id") String idExperimento,
										@RequestParam(value="estado") String estado);

	@GetMapping("/getDetalleExperimento")
	Experimento getDetalleExperimento(@RequestParam(value="id") String idExperimento);

	@GetMapping("/getByProyectoAndEstado")
	List<ProyectoArea> getExperimentoByProyectoAndEstado(@RequestParam(value="id")String id, @RequestParam(value="estado") String estado);

	@PostMapping("/saveMediciones")
	void saveMediciones(@RequestBody Experimento experimento);

	@PostMapping("/updateMediciones")
	void updateMediciones(Experimento experimento);

	@GetMapping("/finalizarExperimento")
	void finalizarExperimento(@RequestParam(value="idHipotesis") String idHipotesis,
								@RequestParam(value="idProyecto") String idProyecto,
								@RequestParam(value="idExperimento") String idExperimento);

	@PostMapping("/guardarCambiosExperimento")
	void guardarCambiosExperimento(@RequestBody ExperimentoDto experimentDto);
						

}
