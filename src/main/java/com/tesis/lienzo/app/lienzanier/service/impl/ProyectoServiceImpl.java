package com.tesis.lienzo.app.lienzanier.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tesis.lienzo.app.lienzanier.bean.Proyecto;
import com.tesis.lienzo.app.lienzanier.feign.ProyectoFeignClient;
import com.tesis.lienzo.app.lienzanier.service.ProyectoService;

@Service
public class ProyectoServiceImpl implements ProyectoService{

	@Autowired
	ProyectoFeignClient proyectoFeignClient;
	
	@Override
	public List<Proyecto> getByUser(String id) {
		// TODO Auto-generated method stub
		return proyectoFeignClient.getByUser(id);
	}

	@Override
	public void update(Proyecto proyecto) {
		// TODO Auto-generated method stub
		proyectoFeignClient.update(proyecto);
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub
		proyectoFeignClient.delete(id);
	}

	@Override
	public String save(Proyecto proyecto) {
		return proyectoFeignClient.save(proyecto);
	}

	@Override
	public Proyecto getById(String id) {
		// TODO Auto-generated method stub
		return proyectoFeignClient.getById(id);
	}

}
