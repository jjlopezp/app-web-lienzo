package com.tesis.lienzo.app.lienzanier.util;

public class Constantes {

	
	public static final int ID_SOCIOS_CLAVE = 1;
	public static final int ID_ACTIVIDADES_CLAVE = 2;
	public static final int ID_RECURSOS_CLAVE = 3;
	public static final int ID_PROPUESTA_VALOR = 4;
	public static final int ID_RELACION_CLIENTES = 5;
	public static final int ID_CANALES = 6;
	public static final int ID_SEGMENTOS_CLIENTES = 7;
	public static final int ID_ESTRUCTURA_COSTOS = 8;
	public static final int ID_FUENTES_INGRESO = 9;
	
	public static final int ESTADO_EXPERIMENTO_ELIMINADO = 0;
	public static final int ESTADO_EXPERIMENTO_EN_CURSO_NUEVO = 3;
	public static final int ESTADO_EXPERIMENTO_EN_CURSO_VIEJO = 4;
	public static final int ESTADO_EXPERIMENTO_FINALIZADO = 5;
	
	public static final int ESTADO_HIPOTESIS_ELIMINADO = 0;
	public static final int ESTADO_HIPOTESIS_ACTIVO = 1;
	public static final int ESTADO_HIPOTESIS_CREAR_EXPERIMENTO = 2;
	public static final int ESTADO_HIPOTESIS_EN_CURSO_NUEVO = 3;
	public static final int ESTADO_HIPOTESIS_EN_CURSO_VIEJO = 4;
	public static final int ESTADO_HIPOTESIS_FINALIZADO = 5;
	
	
	public static final String ARREGLO_DIAS_SEMANAS[] = {"Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"}; 
	public static final String ARREGLO_MESES[] = {"Enero", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"};
	
	public static final String ARTICLE_LISTA_PROYECTO = "<article class='card card--3'> <div class='card__info-hover'> <svg class='card__like'  viewBox='0 0 24 24'>\n"
			+ "					    	<path fill='#000000' d='M12.1,18.55L12,18.65L11.89,18.55C7.14,14.24 4,11.39 4,8.5C4,6.5 5.5,5 7.5,5C9.04,5 10.54,6 11.07,7.36H12.93C13.46,6 14.96,5 16.5,5C18.5,5 20,6.5 20,8.5C20,11.39 16.86,14.24 12.1,18.55M16.5,3C14.76,3 13.09,3.81 12,5.08C10.91,3.81 9.24,3 7.5,3C4.42,3 2,5.41 2,8.5C2,12.27 5.4,15.36 10.55,20.03L12,21.35L13.45,20.03C18.6,15.36 22,12.27 22,8.5C22,5.41 19.58,3 16.5,3Z' /> </svg> </div> <div class='card__img'></div> ";
	
	public static final int TIPO_PROYECTO_LIENZO = 1;
	public static final int TIPO_PROYECTO_LEAN = 2;

}
