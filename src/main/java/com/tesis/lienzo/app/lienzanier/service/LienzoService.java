package com.tesis.lienzo.app.lienzanier.service;

import java.util.List;

import com.tesis.lienzo.app.lienzanier.bean.Hipotesis;
import com.tesis.lienzo.app.lienzanier.bean.NodoArbol;
import com.tesis.lienzo.app.lienzanier.bean.ProyectoArea;
import com.tesis.lienzo.app.lienzanier.dto.HipotesisDto;

public interface LienzoService {

	public List<ProyectoArea> getLienzoByProyecto(String id);

	public String save(HipotesisDto hipotesisDto);

	public HipotesisDto getHipotesisDtoById(String id);

	public void editarHipotesis(HipotesisDto hipotesisDto);

	List<Hipotesis> getHipotesisByProyecto(String idProyecto);

	public void guardarCambiosHipotesis(HipotesisDto hipotesisDto);

	public List<NodoArbol> arbolHipotesisByHipotesis(String idProyecto);
}
