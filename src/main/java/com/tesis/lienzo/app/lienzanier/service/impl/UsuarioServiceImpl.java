package com.tesis.lienzo.app.lienzanier.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tesis.lienzo.app.lienzanier.bean.Usuario;
import com.tesis.lienzo.app.lienzanier.feign.UsuarioFeignClient;
import com.tesis.lienzo.app.lienzanier.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService{

	@Autowired
	UsuarioFeignClient usuarioFeignClient;
	
	@Override
	public List<Usuario> getByEmail(String email, String id) {
		
		return usuarioFeignClient.getByEmail(email, id);
	}

	@Override
	public boolean validateUsuarioRegistrado(String usuario) {
		// TODO Auto-generated method stub
		return usuarioFeignClient.validateUsuarioRegistrado(usuario);
	}

	@Override
	public boolean validateEmailRegistrado(String usuario) {
		// TODO Auto-generated method stub
		return usuarioFeignClient.validateEmailRegistrado(usuario);
	}

	@Override
	public void save(Usuario usuario) {
		// TODO Auto-generated method stub
		usuarioFeignClient.save(usuario);
	}

	@Override
	public void update(Usuario usuario) {
		// TODO Auto-generated method stub
		usuarioFeignClient.update(usuario);
	}

	@Override
	public List<Usuario> getByIdProyecto(String idProyecto) {
		// TODO Auto-generated method stub
		return usuarioFeignClient.getByIdProyecto(idProyecto);
	}

}
