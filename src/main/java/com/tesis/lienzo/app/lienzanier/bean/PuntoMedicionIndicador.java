package com.tesis.lienzo.app.lienzanier.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PuntoMedicionIndicador {

	private int id;
	private int idDia;
	private int idMetricaIndicador;
	private int idMetrica;
	private String metrica;
	private int valor;
	private String identificador;
}
