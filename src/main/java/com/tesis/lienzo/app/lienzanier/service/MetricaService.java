package com.tesis.lienzo.app.lienzanier.service;

import java.util.List;

import com.tesis.lienzo.app.lienzanier.bean.Metrica;

public interface MetricaService {

	List<Metrica> getByProyecto(String idProyecto);

	void update(Metrica metrica);

	void delete(String id);

	void save(Metrica metrica);

}
