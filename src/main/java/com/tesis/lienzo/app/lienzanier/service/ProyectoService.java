package com.tesis.lienzo.app.lienzanier.service;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.tesis.lienzo.app.lienzanier.bean.Proyecto;



public interface ProyectoService {

	List<Proyecto> getByUser(String id);
	public void update(@RequestBody Proyecto proyecto);
	void delete(String id);
	String save(Proyecto proyecto);
	Proyecto getById(String id);
}
