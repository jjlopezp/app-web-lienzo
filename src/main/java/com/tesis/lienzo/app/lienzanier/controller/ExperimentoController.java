package com.tesis.lienzo.app.lienzanier.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tesis.lienzo.app.lienzanier.bean.Comentario;
import com.tesis.lienzo.app.lienzanier.bean.Condicion;
import com.tesis.lienzo.app.lienzanier.bean.Dia;
import com.tesis.lienzo.app.lienzanier.bean.Experimento;
import com.tesis.lienzo.app.lienzanier.bean.Fecha;
import com.tesis.lienzo.app.lienzanier.bean.Hipotesis;
import com.tesis.lienzo.app.lienzanier.bean.Metrica;
import com.tesis.lienzo.app.lienzanier.bean.PruebaHipotesis;
import com.tesis.lienzo.app.lienzanier.bean.PuntoMedicion;
import com.tesis.lienzo.app.lienzanier.dto.ExperimentoDto;
import com.tesis.lienzo.app.lienzanier.dto.HipotesisDto;
import com.tesis.lienzo.app.lienzanier.service.ComentarioService;
import com.tesis.lienzo.app.lienzanier.service.ExperimentoService;
import com.tesis.lienzo.app.lienzanier.service.LienzoService;
import com.tesis.lienzo.app.lienzanier.service.MetricaService;
import com.tesis.lienzo.app.lienzanier.util.Constantes;

@Controller
@RequestMapping("/experimento")
public class ExperimentoController {
	
	@Autowired
	MetricaService metricaService;
	
	@Autowired
	ExperimentoService experimentoService;
	
	@Autowired
	LienzoService lienzoService;
	
	@Autowired
	ComentarioService comentarioService;
	
	@RequestMapping("/crear")
	public ModelAndView crearExperimento(@RequestParam(value="id") String idProyecto, @RequestParam(value="idHipotesis") String idHipotesis,HttpServletRequest request) {
		ModelAndView mv = new ModelAndView("crear_experimento");
		String optionIndicadores = getOptionMetricas(idProyecto);
		getPruebasHipotesis(mv);
		HipotesisDto hipotesisDto = lienzoService.getHipotesisDtoById(idHipotesis);	
		mv.addObject("optionIndicadores", optionIndicadores);
		mv.addObject("idHipotesis", idHipotesis);
		mv.addObject("idProyecto", idProyecto);
		mv.addObject("hipotesisDto", hipotesisDto);
		return mv;
	}
	
	@PostMapping("/save")
	public @ResponseBody Map<String,Object> saveExperimento(@RequestBody ExperimentoDto experimentDto, HttpServletRequest request) {
		Map<String, Object> parametros = new HashMap<>();
		ResponseEntity<Integer> responseExperimento = experimentoService.save(experimentDto);
		Integer idNewExperimento = responseExperimento.getBody();
		parametros.put("url", request.getContextPath()+"/experimento/detalle?id="+experimentDto.getIdProyecto()+"&idExperimento="+idNewExperimento);
		return parametros;
	}
	
	@PostMapping("/updateMediciones")
	public @ResponseBody Map<String,Object> updateMediciones(@RequestBody Experimento experimento, Model model, HttpServletRequest request) {
		Map<String, Object> parametros = new HashMap<>();
		parametros.put("url", request.getContextPath()+"/experimento/historial?id="+experimento.getIdProyecto());
		experimentoService.updateMediciones(experimento);
		return parametros;
	}
	
	@PostMapping("/saveMediciones")
	public @ResponseBody Map<String,Object> saveMediciones(@RequestBody Experimento experimento, Model model, HttpServletRequest request) {
		Map<String, Object> parametros = new HashMap<>();
		parametros.put("url", request.getContextPath()+"/experimento/agenda?id="+experimento.getIdProyecto()+"&idExperimento="+experimento.getId());
		experimentoService.saveMediciones(experimento);
		return parametros;
	}
	
	@RequestMapping("/historial")
	public ModelAndView crearExperimento(@RequestParam(value="id") String idProyecto, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView("historial_experimentos");
		
		List<Hipotesis> hipotesis = lienzoService.getHipotesisByProyecto(idProyecto);
		String historico = "";
		
	
		for(Hipotesis hi : hipotesis){
			historico = historico.concat("<li>");
			historico = historico.concat("<p>"+hi.getSuposicion()+"</p>");
			
			String sinExperimento ="<label class='green'><i class='fas fa-check fa-3x'></i></label>";
			String sinIniciar ="<label class='red'><i class='fas fa-minus fa-3x'></i></label>";
			String enCurso="<label class='red'><i class='fas fa-minus fa-3x'></i></label>";
			String finalizado="<label class='red'><i class='fas fa-minus fa-3x'></i></label>";
			String detalle = "";
			
			if(hi.getEstado() == Constantes.ESTADO_HIPOTESIS_ACTIVO) {
				detalle = "<label><strong><a href='/home/hipotesis?id="+hi.getId()+"&idPro="+idProyecto+"'>Ver detalle</a></strong></label>";
			}else if(hi.getEstado() == Constantes.ESTADO_HIPOTESIS_CREAR_EXPERIMENTO) {
				sinIniciar = "<label class='green'><i class='fas fa-check fa-3x'></i></label>";
				detalle = "<label><strong><a href='"+request.getContextPath()+"/experimento?id="+idProyecto+"&idHipotesis="+hi.getId()+"'>Ver detalle</a></strong></label>";
			}else if(hi.getEstado() == Constantes.ESTADO_HIPOTESIS_EN_CURSO_NUEVO) {
				sinIniciar = "<label class='green'><i class='fas fa-check fa-3x'></i></label>";
				enCurso = "<label class='green'><i class='fas fa-check fa-3x'></i></label>";
				//<a href='/detalleExperimento?id="+idProyecto+"&idExperimento="+h.getId()+"'>
				detalle = "<label><strong><a href='/detalleExperimento?id="+idProyecto+"&idExperimento="+hi.getIdExperimento()+"'>Ver detalle</a></strong></label>";
				
			}else if(hi.getEstado() == Constantes.ESTADO_HIPOTESIS_EN_CURSO_VIEJO) {
				sinIniciar = "<label class='green'><i class='fas fa-check fa-3x'></i></label>";
				enCurso = "<label class='green'><i class='fas fa-check fa-3x'></i></label>";
				//<a href='"+request.getContextPath()+"/experimento/agenda?id="+idProyecto+"&idExperimento="+h.getId()+"'>
				detalle = "<label><strong><a href='"+request.getContextPath()+"/experimento/agenda?id="+idProyecto+"&idExperimento="+hi.getIdExperimento()+"'>Ver detalle</a></strong></label>";
				
			}else if(hi.getEstado() == Constantes.ESTADO_HIPOTESIS_FINALIZADO) {
				sinIniciar = "<label class='green'><i class='fas fa-check fa-3x'></i></label>";
				enCurso = "<label class='green'><i class='fas fa-check fa-3x'></i></label>";
				finalizado = "<label class='green'><i class='fas fa-check fa-3x'></i></label>";
				
				detalle = "<label><strong><a href='"+request.getContextPath()+"/experimento/finalizar?id="+idProyecto+"&idExperimento="+hi.getIdExperimento()+"'>Ver detalle</a></strong></label>";
				
			}			
			
			
			
			historico = historico.concat(sinExperimento);
			historico = historico.concat(sinIniciar);
			historico = historico.concat(enCurso);
			historico = historico.concat(finalizado);
			historico = historico.concat(detalle);
			
			historico = historico.concat("</li>");
		}

		System.out.println(historico);
		mv.addObject("idProyecto", idProyecto);
		mv.addObject("historico", historico);
		
		return mv;
	}
	
	@RequestMapping ("/updateEstado")
	public @ResponseBody Map<String,Object> updateExperimento (@RequestParam(value="id") String idExperimento,
												@RequestParam(value="estado") String estado, 
												@RequestParam(value="idProyecto") String idProyecto, HttpServletRequest request){
		Map<String, Object> parametros = new HashMap<>();
		parametros.put("estado", "2");
		parametros.put("url", request.getContextPath()+"/experimento/detalle?id="+idProyecto+"&idExperimento="+idExperimento);
		experimentoService.updateEstado(idExperimento, estado);
		
		return parametros;
	}
	
	@RequestMapping("/finalizarExperimento")
	public @ResponseBody Map<String, Object> finalizarExperimento(@RequestParam(value="idHipotesis") String idHipotesis,
																	@RequestParam(value="idProyecto") String idProyecto,
																	@RequestParam(value="idExperimento") String idExperimento, HttpServletRequest request){
		experimentoService.finalizarExperimento(idHipotesis, idProyecto, idExperimento);
		Map<String, Object> parametros = new HashMap<>();
		parametros.put("url", request.getContextPath()+"/experimento/finalizar?id="+idProyecto+"&idExperimento="+idExperimento);
		
		return parametros;
	}
	
	@RequestMapping("/detalle")
	public ModelAndView detalleExperimento(@RequestParam(value="id") String idProyecto, @RequestParam(value="idExperimento", required = false) String idExperimento, HttpServletRequest request) throws ScriptException {
		ModelAndView mv = new ModelAndView("detalle_experimento");
		Experimento experimento = experimentoService.getDetalleExperimento(idExperimento);
		String suposicion = experimento.getHipotesis();
		String descripcion = experimento.getDescripcion();
		fillComentarioDetalle(idExperimento, mv, request);
		String estado = "";
		String tipoPrueba = experimento.getTipoPrueba();
		String metricas = "";
		int i=0;
		for(Metrica me : experimento.getMetricas()) {
			
			metricas = metricas.concat("<li nombre='"+me.getNombreCorto()+"' codigo='"+me.getId()+"'>");
			metricas = metricas.concat("<h2>"+i+++"</h2>");
			metricas = metricas.concat("<h3>"+me.getNombreCorto()+"</h3>");
			metricas = metricas.concat("<p><strong>L"+me.getId()+"</strong></p>");
//			metricas = metricas.concat("<<button>Read more</button>>");
			metricas = metricas.concat("</li>");

		}
		
		
		String condiciones = "";
		i=0;
		for(Condicion co : experimento.getCondiciones()) {
			condiciones = condiciones.concat("<li nombre='"+co.getTxtCondicion()+"' codigo='"+co.getId()+"'>");
			condiciones = condiciones.concat("<h2>"+i+++"</h2>");
			condiciones = condiciones.concat("<p><strong>"+co.getDescripcion()+"</strong></p>");

			String condicionHTML = co.getTxtCondicion().replace("<", "&#60;");

			condiciones = condiciones.concat("<h3>"+condicionHTML+"</h3>");
//			condiciones = condiciones.concat("<<button>Read more</button>>");
			condiciones = condiciones.concat("</li>");
		}
		

		mv.addObject("suposicion", suposicion);
		mv.addObject("descripcion", descripcion);
//		mv.addObject("estado", estado);
		mv.addObject("tipoPrueba", tipoPrueba);
		mv.addObject("metricas", metricas);
		mv.addObject("condiciones", condiciones);

		mv.addObject("idHipotesis", experimento.getIdHipotesis());
		mv.addObject("idProyecto", idProyecto);
		mv.addObject("idExperimento", idExperimento);
		return mv;
	}
	
	@RequestMapping("/agenda")
	public ModelAndView agendaExperimento(@RequestParam(value="id") String idProyecto, @RequestParam(value="idExperimento", required = false) String idExperimento, HttpServletRequest request) throws ScriptException {
		ModelAndView mv = new ModelAndView("agenda");
		Experimento experimento = experimentoService.getDetalleExperimento(idExperimento);
		String suposicion = experimento.getHipotesis();
		String descripcion = experimento.getDescripcion();
		String estado = "";
		
		String tipoPrueba = experimento.getTipoPrueba();
		String metricas = "";
		int i=0;
		for(Metrica me : experimento.getMetricas()) {
			
			metricas = metricas.concat("<li nombre='"+me.getNombreCorto()+"' codigo='"+me.getId()+"'>");
			metricas = metricas.concat("<h2>"+i+++"</h2>");
			metricas = metricas.concat("<h3>"+me.getNombreCorto()+"</h3>");
			metricas = metricas.concat("<p>L"+me.getId()+"</p>");
			metricas = metricas.concat("</li>");

		}
		
		
		String condiciones = "";
		i=0;
		for(Condicion co : experimento.getCondiciones()) {
			condiciones = condiciones.concat("<li nombre='"+co.getTxtCondicion()+"' codigo='"+co.getId()+"'>");
			condiciones = condiciones.concat("<h2>"+i+++"</h2>");
			condiciones = condiciones.concat("<p>"+co.getDescripcion()+"</p>");
			String condicionHTML = co.getTxtCondicion().replace("<", "&#60;");
			condiciones = condiciones.concat("<h3>"+condicionHTML+"</h3>");		
			condiciones = condiciones.concat("</li>");

		}
		
		fillComentarioDetalle(idExperimento, mv, request);
		
		String html = "";
		for(Fecha fe: experimento.getFechas()) {
			html = html.concat("<li>");
			html = html.concat("<div mes='"+fe.getMes()+"' class='timeline-month'>");
			html = html.concat(Constantes.ARREGLO_MESES[fe.getMes()]+", 2020");
			html = html.concat("<span>3 Entries</span>");
			html = html.concat("</div>");
			html = html.concat("<ul>");
			
			
			for(Dia dia: fe.getDias()) {
				html = html.concat("<li class='timeline-section li-dia'>");
				html = html.concat("<div semana='"+dia.getSemana()+"' dia='"+dia.getDia()+"' class='timeline-date'>");
				html = html.concat(dia.getDia() +", "+Constantes.ARREGLO_DIAS_SEMANAS[dia.getSemana()]);
				html = html.concat("</div>");
				html = html.concat("<ul class='row'>");
				
				for(PuntoMedicion pm: dia.getMediciones()) {
					html = html.concat("<li id='boxMetrica'>");
					html = html.concat("<div class='col-sm-4'>");
					html = html.concat("<div class='timeline-box'>");
					html = html.concat("<div class='box-tittle'>");
					html = html.concat("<i class='fa fa-asterisk text-success' aria-hidden='true'></i> Métrica");
					html = html.concat("</div>");

					html = html.concat("<div class='box-content'>");
					html = html.concat("<div class='box-item'><strong>"+pm.getMetrica()+" ("+pm.getIdentificador()+")</strong></div>");
					html = html.concat("<div class='box-item'><input idMetrica='"+pm.getIdMetrica()+"' indentificador='"+pm.getIdentificador()+"' value='"+pm.getValor()+"' class='' onkeypress='validarInput(event)'></div>");
					html = html.concat("</div>");

					html = html.concat("<div class='box-footer'>- </div>");
					html = html.concat("</div>");
					html = html.concat("</div>");
					html = html.concat("</li>");
				}
				
				html = html.concat(analizarPuntoMedicion(dia.getMediciones(), experimento.getCondiciones()));
				
				
				
				html = html.concat("</ul>");
				html = html.concat("</li>");
			}


			html = html.concat("</ul>");
			html = html.concat("</li>");
		}
		

		mv.addObject("suposicion", suposicion);
		mv.addObject("descripcion", descripcion);
//		mv.addObject("estado", estado);
		mv.addObject("tipoPrueba", tipoPrueba);
		mv.addObject("metricas", metricas);
		mv.addObject("condiciones", condiciones);
//		mv.addObject("cabecera", cabecera);
		mv.addObject("mediciones", html);
//		mv.addObject("listCondiciones", experimento.getCondiciones());
//		mv.addObject("listMetricas", experimento.getMetricas());
		mv.addObject("idHipotesis", experimento.getIdHipotesis());
		mv.addObject("idProyecto", idProyecto);
		mv.addObject("idExperimento", idExperimento);
		
		mv.addObject("evidencias", experimento.getEvidencias());
		return mv;
	}
	
	
	
	
	@RequestMapping("/finalizar")
	public ModelAndView finalizar(@RequestParam(value="id") String idProyecto, @RequestParam(value="idExperimento", required = false) String idExperimento, HttpServletRequest request) throws ScriptException, JsonProcessingException {
		ModelAndView mv = new ModelAndView("resumen_finalizar");
		Experimento experimento = experimentoService.getDetalleExperimento(idExperimento);
		String suposicion = experimento.getHipotesis();
		String descripcion = experimento.getDescripcion();
		String estado = "";
		fillComentarioDetalle(idExperimento, mv, request);

		String tipoPrueba = experimento.getTipoPrueba();
		String metricas = "";
		int i=0;
		for(Metrica me : experimento.getMetricas()) {
			
			metricas = metricas.concat("<li nombre='"+me.getNombreCorto()+"' codigo='"+me.getId()+"'>");
			metricas = metricas.concat("<h2>"+i+++"</h2>");
			metricas = metricas.concat("<h3>"+me.getNombreCorto()+"</h3>");
			metricas = metricas.concat("<p>L"+me.getId()+"</p>");
//			metricas = metricas.concat("<button>Read more</button>");
			metricas = metricas.concat("</li>");

		}
		
		
		String condiciones = "";
		i=0;
		for(Condicion co : experimento.getCondiciones()) {
			condiciones = condiciones.concat("<li nombre='"+co.getTxtCondicion()+"' codigo='"+co.getId()+"'>");
			condiciones = condiciones.concat("<h2>"+i+++"</h2>");
			condiciones = condiciones.concat("<p>"+co.getDescripcion()+"</p>");
			String condicionHTML = co.getTxtCondicion().replace("<", "&#60;");
			condiciones = condiciones.concat("<h3>"+condicionHTML+"</h3>");			
//			condiciones = condiciones.concat("<button>Read more</button>");
			condiciones = condiciones.concat("</li>");

		}
		
		ObjectMapper objectMapper = new ObjectMapper();
		

		String dataJson="hola";
		
		mv.addObject("suposicion", suposicion);
		mv.addObject("descripcion", descripcion);
		mv.addObject("tipoPrueba", tipoPrueba);
		mv.addObject("metricasObject", objectMapper.writeValueAsString(experimento.getMetricas()));
		mv.addObject("metricas", metricas);
		mv.addObject("condiciones", condiciones);
		mv.addObject("mediciones", objectMapper.writeValueAsString(experimento.getFechas()));
	
		
//		mv.addObject("mediciones", dataJson);
		mv.addObject("idProyecto", idProyecto);
		mv.addObject("idExperimento", idExperimento);
		return mv;
	}
	
	
	
	
	private String getOptionMetricas(String idProyecto) {
		List<Metrica> metricas = metricaService.getByProyecto(idProyecto);
		String html = "";
		int i=0;
		for(Metrica me: metricas) {
			
			html = html.concat("<label>");
			if(i==0) {
				html = html.concat(" <input value='"+me.getId()+"' texto='"+me.getNombreCorto()+"' type='checkbox' class='option-input checkbox' checked />");
				html = html.concat(me.getNombreCorto());
				html = html.concat("</label>");
				i++;
			}else {
				html = html.concat(" <input value='"+me.getId()+"' texto='"+me.getNombreCorto()+"' type='checkbox' class='option-input checkbox'/>");
				html = html.concat(me.getNombreCorto());
				html = html.concat("</label>");
			}
			
			
		}
		return html;
	}
	
	private void getPruebasHipotesis(ModelAndView mv) {
		List<PruebaHipotesis> pruebas = experimentoService.getPruebasHipotesis();
		String header = "";
		String html = "";
		int i =0;
		for(PruebaHipotesis ph: pruebas) {
			if(i==0) {
				header = header.concat("<a idPrueba='"+ph.getId()+"'  class='list-group-item list-group-item-action active' id='"+ph.getNombreList()+"' data-toggle='list' href='#"+ph.getNombreUpper()+"' role='tab' aria-controls='"+ph.getNombreUpper()+"'>"+ph.getNombre()+"</a>");
				html = html.concat("<div class='tab-pane fade show active border-gray' id='"+ph.getNombreUpper()+"' role='tabpanel' aria-labelledby='"+ph.getNombreList()+"'>");
				html = html.concat("<p>"+ph.getDescripcion()+"</p>");
				html = html.concat("</div>");
				
				i++;	
			}else {
				header = header.concat("<a idPrueba='"+ph.getId()+"'  class='list-group-item list-group-item-action' id='"+ph.getNombreList()+"' data-toggle='list' href='#"+ph.getNombreUpper()+"' role='tab' aria-controls='"+ph.getNombreUpper()+"'>"+ph.getNombre()+"</a>");
				html = html.concat("<div class='tab-pane fade border-gray' id='"+ph.getNombreUpper()+"' role='tabpanel' aria-labelledby='"+ph.getNombreList()+"'>");
				html = html.concat("<p>"+ph.getDescripcion()+"</p>");
				html = html.concat("</div>");
			}
		}
		mv.addObject("headerPruebas", header);
		mv.addObject("cuerpoPruebas", html);
	}
	
	
	
	private String getExperimentosByProyecto(String id, HttpServletRequest request) {
		List<Experimento> pruebas = experimentoService.getExperimentosByProjecto(id);
		String html = "";
		int i=1;
		for(Experimento ph: pruebas) {
			html = html.concat("<tr value=" + ph.getId()+">");
			html = html.concat("<td>"+i+++"</td>");
			html = html.concat("<td><a href='"+request.getContextPath()+"/experimento/detalle?id="+id+"&idExperimento="+ph.getId()+"'>"+ph.getHipotesis()+"</a></td>");

			html = html.concat("<td><i class='editProy fas fa-edit'></i></td>");
			html = html.concat("<td><i class='remMetrica fas fa-trash-alt'></i></td>");
			html = html.concat("</tr>");
		}
		return html;
	}
	
	
	
	

	private String analizarPuntoMedicion(List<PuntoMedicion> mediciones, List<Condicion> condiciones) {
		
		
		
		String re = "";
		ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
		ScriptEngine scriptEngine = scriptEngineManager.getEngineByName("JavaScript");
		for(Condicion co : condiciones) {
			
			re = re.concat("<li txtCondicion='"+co.getTxtCondicion()+"' class='liCondicion'>");
			re = re.concat("<div class='col-sm-4'>");
			re = re.concat("<div class='timeline-box box-condicion'>");
			re = re.concat("<div class='box-tittle'>");
			re = re.concat("<i class='fa fa-asterisk text-success' aria-hidden='true'></i> Condición");
			re = re.concat("</div>");

			re = re.concat("<div class='box-content'>");
			String condicionHTML = co.getTxtCondicion().replace("<", "&#60;");
			re = re.concat("<div class='box-item'><p><strong>"+condicionHTML+"</strong></p></div>");

			String condi = co.getTxtCondicion();//L2+50=L1
			for(PuntoMedicion pm : mediciones) {
				condi = condi.replace(pm.getIdentificador(), String.valueOf(pm.getValor()));
			}
			
			String operador="";
			int opera = 0;
			if (condi.contains("=")) {
				operador="=";
				opera=1;
			}else if(condi.contains("<")) {
				operador="<";
				opera=2;
			}else if(condi.contains(">")) {
				operador=">";
				opera=3;
			}
			String esperado = condi.substring(condi.indexOf(operador)+1, condi.length());
			String evaluar = condi.substring(0, condi.indexOf(operador));
			int esperadoInt = Integer.parseInt(esperado);
			Object ob = null;
			int resp =0;
			try {
				evaluar = evaluar.replace("%", "/100");
				ob = scriptEngine.eval(evaluar);
				resp = Integer.parseInt(ob.toString());
			}catch(Exception ex) {
				resp=0;
			}
			
			
			boolean cumple = false;
			if(opera==1) {
				if(resp == esperadoInt)
					cumple = true;
			}else if(opera==2) {
				if(resp < esperadoInt)
					cumple = true;
			}else if(opera==3) {
				if(resp > esperadoInt)
					cumple = true;
			}
			if(cumple) {
				re = re.concat("<div class='box-item'>");
				re = re.concat("<div id='inheritBox'>");
				re = re.concat("<span><i class='far fa-thumbs-down dislike' style='display:none'></i></span>");
				re = re.concat("<span><i class='far fa-thumbs-up like' ></i></span>");
				re = re.concat("<span><i class='fas fa-ban stop' style='display:none'></i></span>");
				re = re.concat("</div>");
				re = re.concat("</div>");
			}else {
				re = re.concat("<div class='box-item'>");
				re = re.concat("<div id='inheritBox'>");
				re = re.concat("<span><i class='far fa-thumbs-down dislike' ></i></span>");
				re = re.concat("<span><i class='far fa-thumbs-up like' style='display:none'></i></span>");
				re = re.concat("<span><i class='fas fa-ban stop' style='display:none'></i></span>");
				re = re.concat("</div>");
				re = re.concat("</div>");
			
			}
			
			re = re.concat("</div>");

			re = re.concat("<div class='box-footer'>- </div>");
			re = re.concat("</div>");
			re = re.concat("</div>");
			re = re.concat("</li>");
		}
		
		return re;
	}
	
	private void fillComentarioDetalle(String idExperimento, ModelAndView mv, HttpServletRequest request) {
		// TODO Auto-generated method stub
		String comentariosHtml = "";
		List<Comentario> comentarios = comentarioService.getComentariosByExperimento(idExperimento);
		String liGenerales ="";
		
		for(Comentario co : comentarios) {
			liGenerales = liGenerales.concat("<li idProyecto='"+co.getIdProyecto()+"' idComentario='"+co.getId()+"' class='item'>");
			liGenerales = liGenerales.concat("<span>");
			liGenerales = liGenerales.concat(co.getComentario());
			liGenerales = liGenerales.concat("</span>");
			liGenerales = liGenerales.concat("<button id='btnResuelta'>Marcar como resuelta</button>");
			liGenerales = liGenerales.concat("</li>");
		}
		comentariosHtml = comentariosHtml.concat(liGenerales);
		mv.addObject("comentariosHipotesisHtml", comentariosHtml);
	}

}
