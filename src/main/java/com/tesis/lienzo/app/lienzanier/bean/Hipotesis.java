package com.tesis.lienzo.app.lienzanier.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Hipotesis {

	private int id;
	private String suposicion;
	private String publico;
	private int duracion;
	private String descripcion;
	private int estado;
	private int idExperimento;
	
}
