package com.tesis.lienzo.app.lienzanier.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tesis.lienzo.app.lienzanier.bean.Comentario;
import com.tesis.lienzo.app.lienzanier.feign.ComentarioFeign;
import com.tesis.lienzo.app.lienzanier.service.ComentarioService;

@Service
public class ComentarioServiceImpl implements ComentarioService{
	
	@Autowired
	ComentarioFeign comentarioFeign;

	@Override
	public List<Comentario> getComentariosByProyecto(String idProyecto) {
		// TODO Auto-generated method stub
		return comentarioFeign.getComentariosByProyecto(idProyecto);
	}

	@Override
	public void updateEstadoComentario(String idComentario, String estado) {
		// TODO Auto-generated method stub
		comentarioFeign.updateEstadoComentario(idComentario, estado);
	}

	@Override
	public void saveComentarioProyecto(Comentario comentario) {
		// TODO Auto-generated method stub
		comentarioFeign.saveComentarioProyecto(comentario);
	}

	@Override
	public void saveComentarioExperimento(Comentario comentario) {
		// TODO Auto-generated method stub
		comentarioFeign.saveComentarioExperimento(comentario);
		
	}

	@Override
	public List<Comentario> getComentariosByExperimento(String idExperimento) {
		// TODO Auto-generated method stub
		return comentarioFeign.getComentariosByExperimento(idExperimento);
	}

	@Override
	public List<Comentario> getComentariosDetalleByProyecto(String idProyecto) {
		// TODO Auto-generated method stub
		return comentarioFeign.getComentariosDetalleByProyecto(idProyecto);
	}

	@Override
	public void updateEstadoComentarioExperimento(String idComentario, String estado) {
		// TODO Auto-generated method stub
		comentarioFeign.updateEstadoComentarioExperimento(idComentario, estado);
	}

}
