package com.tesis.lienzo.app.lienzanier.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.tesis.lienzo.app.lienzanier.bean.Proyecto;
import com.tesis.lienzo.app.lienzanier.bean.Usuario;
import com.tesis.lienzo.app.lienzanier.bean.UsuarioProyecto;
import com.tesis.lienzo.app.lienzanier.service.ProyectoService;
import com.tesis.lienzo.app.lienzanier.util.Constantes;

@Controller
@RequestMapping("/proyecto")
public class ProyectoController {

	@Autowired
	ProyectoService proyectoService;
	
	@RequestMapping("/listar")
	public ModelAndView sigin(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView("lista_proyectos");
		
		Usuario usuario = ((Usuario) request.getSession().getAttribute("USUARIOSESION"));
		if(usuario==null) {
			return mv;
		}else {
			String proyectos = getProyectosByUser(usuario, request);
			mv.addObject("proyectos", proyectos);
			return mv;
		}
	}
	
	@PostMapping("/updateProy")
	@ResponseStatus(value = HttpStatus.OK)
	public void update(@RequestBody Proyecto proyecto) {
		proyectoService.update(proyecto);
	}
	
	@RequestMapping("/getListProyectos")
	@ResponseBody
	@ResponseStatus(value = HttpStatus.OK)
	public List<Proyecto> getListProyectos(Model model, HttpServletRequest request){
		Usuario usuario = ((Usuario) request.getSession().getAttribute("USUARIOSESION"));
		return proyectoService.getByUser(usuario.getId().toString());
	}
	
	@PostMapping("/deleteById")
	public @ResponseBody Map<String,Object> deleteProyecto(@RequestParam(value="id") String id, HttpServletRequest request) {
		Map<String, Object> parametros = new HashMap<>();
		parametros.put("url", request.getContextPath()+"/proyectos/misproyectos");
		proyectoService.delete(id);
		return parametros;
	}
	
	@PostMapping("/save")
	@ResponseStatus(value = HttpStatus.OK)
	public void saveProyecto(@RequestBody Proyecto proyecto, HttpServletRequest request) {
		Usuario usuario = ((Usuario) request.getSession().getAttribute("USUARIOSESION"));
		UsuarioProyecto usuarioProyecto = new UsuarioProyecto();
		usuarioProyecto.setId_usuario(usuario.getId());
		proyecto.getUsuarioXproyecto().add(usuarioProyecto);
		proyectoService.save(proyecto);
	}
	
	@RequestMapping("/listar/share")
	public ModelAndView listarShare(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView("lista_proyectos_share");
		
		Usuario usuario = ((Usuario) request.getSession().getAttribute("USUARIOSESION"));
		if(usuario==null) {
			return mv;
		}else {
			String proyectos = getProyectosByUserShare(usuario, request);
			mv.addObject("proyectos", proyectos);
			return mv;
		}
	}
	
	private String getProyectosByUserShare(Usuario usuario, HttpServletRequest request) {
		List<Proyecto> proyectos = proyectoService.getByUser(usuario.getId().toString());
		String html = "";
		for(Proyecto pro : proyectos){
			
			html = html.concat(Constantes.ARTICLE_LISTA_PROYECTO);
			html = html.concat("<a href='"+request.getContextPath()+"/share/hipotesis/"+pro.getId()+"'>");
			html = html.concat("<div class='card__img--hover'></div>");
			html = html.concat("</a>");
			html = html.concat("<div class='card__info'>");
			html = html.concat("<span class='card__category'></span>");
			html = html.concat("<h3 class='card__title'>"+pro.getNombre()+"</h3>");
			html = html.concat("<p>"+pro.getDescripcion()+"</p>");
			html = html.concat("</div>");
			html = html.concat("</article>");
			
//http://localhost:8085/share/hipotesis/23
		}
		return html;
	}

	private String getProyectosByUser(Usuario usuario, HttpServletRequest request) {
		List<Proyecto> proyectos = proyectoService.getByUser(usuario.getId().toString());
		String html = "";
		for(Proyecto pro : proyectos){
			
			html = html.concat(Constantes.ARTICLE_LISTA_PROYECTO);
			html = html.concat("<a href='"+request.getContextPath()+"/home?id="+pro.getId()+"'>");
			html = html.concat("<div class='card__img--hover'></div>");
			html = html.concat("</a>");
			html = html.concat("<div class='card__info'>");
			html = html.concat("<span class='card__category'></span>");
			html = html.concat("<h3 class='card__title'>"+pro.getNombre()+"</h3>");
			html = html.concat("<p>"+pro.getDescripcion()+"</p>");
			html = html.concat("</div>");
			html = html.concat("</article>");
			

		}
		return html;
		
		
	}
}
