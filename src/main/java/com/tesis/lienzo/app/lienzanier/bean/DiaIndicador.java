package com.tesis.lienzo.app.lienzanier.bean;

import java.util.List;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class DiaIndicador {

	private int id;
	private int idMes;
	private int dia;
	private int semana;
	private List<PuntoMedicionIndicador> puntos;
}
