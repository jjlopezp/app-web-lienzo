package com.tesis.lienzo.app.lienzanier.controller;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.file.Files;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/files")
public class FilesController {

	@Autowired
	ServletContext context; 
	
	@RequestMapping(value = "/{filename2:.+}", method = RequestMethod.GET)
	public void submit(@PathVariable String filename2, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		System.out.println("****** entra a get image ****");
		System.out.println(filename2);
//		filename = filename.concat(".png");
		String filename = URLDecoder.decode(filename2, "UTF-8");
        File file = new File(  context.getRealPath("/resources/img/editor/") , filename);
        response.setHeader("Content-Type", context.getMimeType(filename));
        response.setHeader("Content-Length", String.valueOf(file.length()));
        response.setHeader("Content-Disposition", "inline; filename=\"" + file.getName() + "\"");
        Files.copy(file.toPath(), response.getOutputStream());
	}
}
