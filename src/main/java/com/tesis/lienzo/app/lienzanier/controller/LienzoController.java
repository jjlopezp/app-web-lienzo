package com.tesis.lienzo.app.lienzanier.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tesis.lienzo.app.lienzanier.bean.Comentario;
import com.tesis.lienzo.app.lienzanier.bean.Experimento;
import com.tesis.lienzo.app.lienzanier.bean.Hipotesis;
import com.tesis.lienzo.app.lienzanier.bean.Metrica;
import com.tesis.lienzo.app.lienzanier.bean.NodoArbol;
import com.tesis.lienzo.app.lienzanier.bean.Proyecto;
import com.tesis.lienzo.app.lienzanier.bean.ProyectoArea;
import com.tesis.lienzo.app.lienzanier.bean.Usuario;
import com.tesis.lienzo.app.lienzanier.dto.HipotesisDto;
import com.tesis.lienzo.app.lienzanier.service.ComentarioService;
import com.tesis.lienzo.app.lienzanier.service.ExperimentoService;
import com.tesis.lienzo.app.lienzanier.service.LienzoService;
import com.tesis.lienzo.app.lienzanier.service.ProyectoService;
import com.tesis.lienzo.app.lienzanier.service.UsuarioService;
import com.tesis.lienzo.app.lienzanier.util.Constantes;

@Controller
@RequestMapping("/lienzo")
public class LienzoController {

	private final String SOCIOS_CLAVE = "sociosClave";
	private final String ACTIVIDADES_CLAVE = "actividadesClave";
	private final String RECURSOS_CLAVE = "recursosClave";
	private final String PROPUESTA_VALOR = "propuestaValor";
	private final String RELACION_CLIENTES = "relacionClientes";
	private final String CANALES = "canales";
	private final String SEGMENTOS_CLIENTES = "segmentosClientes";
	private final String ESTRUCTURA_COSTOS = "estructuraCostos";
	private final String FUENTES_INGRESO = "fuentesIngreso";
	
	@Autowired
	LienzoService lienzoService;
	
	@Autowired
	UsuarioService usuarioService;
	
	@Autowired
	ExperimentoService experimentoService;
	
	@Autowired
	ComentarioService comentarioService;
	
	@Autowired
	ProyectoService proyectoService;
	
	@RequestMapping("/home")
	public ModelAndView home(@RequestParam(value="id") String idProyecto, HttpServletRequest request) throws JsonProcessingException {
		/*Hipótesis sin experimento -> Estado 1 y 2*/
		ModelAndView mv = new ModelAndView("home");
		fillFiltro(idProyecto, mv, request);
		String filtroSinExperimento ="";
		filtroSinExperimento = filtroSinExperimento.concat("<button type='button' class='btn btn-primary btn-filtro active'><a href='"+request.getContextPath()+"/home?id="+idProyecto+"'>Sin experimento</a></button>");
		
		String filtroExperimento ="";
		filtroExperimento = filtroExperimento.concat("<button type='button' class='btn btn-primary btn-filtro'><a href='"+request.getContextPath()+"/home/experimento?id="+idProyecto+"&estado=3'>Sin iniciar</a></button>");
		filtroExperimento = filtroExperimento.concat("<button type='button' class='btn btn-primary btn-filtro'><a href='"+request.getContextPath()+"/home/experimento?id="+idProyecto+"&estado=4'>En curso</a></button>");
		filtroExperimento = filtroExperimento.concat("<button type='button' class='btn btn-primary btn-filtro'><a href='"+request.getContextPath()+"/home/experimento?id="+idProyecto+"&estado=5'>Finalizado</a></button>");
		
		String globillo = "<hgroup id='globillo' class='speech-bubble'>";
		globillo = globillo.concat("<label onClick='closeGlobo()' class='close'><i class='fas fa-times'></i></label>");
		globillo = globillo.concat("<h6 style='clear:both;font-weight: bold;'>¿Que son las actividades claves?</h6>");
		globillo = globillo.concat("</hgroup>");

		mv.addObject("globillo", globillo);
		mv.addObject("filtroSinExperimento", filtroSinExperimento);
		mv.addObject("filtroExperimento", filtroExperimento);
		
		Proyecto proyecto = proyectoService.getById(idProyecto);
		fillInfoSectores(proyecto, mv, request);
		fillHipotesisLienzo(idProyecto, proyecto, mv, request);
		fillComentarios(idProyecto, mv, request);
		mv.addObject("idProyecto", idProyecto);
		
		ObjectMapper objectMapper = new ObjectMapper();
		List<NodoArbol> nodos = lienzoService.arbolHipotesisByHipotesis(idProyecto);
		
		
		
		mv.addObject("arbolObject", objectMapper.writeValueAsString(nodos));
		return mv;
	}
	
	private void fillInfoSectores(Proyecto proyecto, ModelAndView mv, HttpServletRequest request) {
		
		String tituloPropuestaValor = "";
		String tituloRelacionClientes = "";
		String tituloRecursosClave = "";
		String tituloActividadesClave = "";
		String tituloSociosClave = "";
		
		String infoSegmentosClientes = "<p>"
				+ "				                         	 En este sector debes plasmar para quienes está dirigido tu negocio."
				+ "				    						 Se puede segmentar de acuerdo a características como genero, edad, gustos"
				+ "				                         </p>"
				+ "				                         <p>"
				+ "				                         	Debe responder a estas preguntas:"
				+ "				                         </p>"
				+ "				                         <ul>"
				+ "				   							<li>¿Para quién se está creando valor?</li>"
				+ "				   							<li>¿Quienes son los clientes más importantes?</li>"
				+ "				   							<li>¿Son clientes específicos o público general?</li>"
				+ "				   						</ul>     ";
		String infoPropuestaValor = ""; //Proposición de valor única
		String infoCanales = "<p>"
				+ "				                         	En este sector se define como es que se hará la entrega del servicio (Propuesta de valor) hacia tus clientes (Segmento de clientes)"
				+ "				                         </p>"
				+ "				                         <ul>"
				+ "				   							<li>¿Será necesario alquilar un local?</li>"
				+ "				   							<li>¿Se requiere de una página web?</li>"
				+ "				   							<li>¿Cómo será el proceso de compra?</li>"
				+ "				   						</ul>  ";
		String infoRelacionClientes = ""; //Ventaja especial
		String infoFuentesIngreso = "<p>"
				+ "			                         	Especificar la manera en que se genera ingresos para la empresa, se debe tener en consideración los precios y"
				+ "			                         	estrategias de pricing que debemos llevar a cabo y la posibilidad de generar ingresos indirectos."
				+ "			                         	Considerar el valor percibido por el cliente (propuesta de valor, segmentos de clientes y la relación)  para plantear el precio"
				+ "			                         </p>"
				+ "			                         <ul>"
				+ "			   							<li>¿Por que están dispuestos a pagar los clientes?</li>"
				+ "			   							<li>¿Serán ingresos puntuales o de forma recurrente?</li>"
				+ "			   							<li>¿Como se va a fijar los precios? ¿Serán fijos o variables?</li>"
				+ "			   							<li>¿Los precios serán estables? ¿Habrá estacionalidad?</li>"
				+ "			   						</ul>";
		String infoRecursosClave = ""; //Metricas clave
		String infoActividadesClave = ""; //Solucion
		String infoSociosClave = ""; //Problema
		String infoEstructuraCostos = "<p>"
				+ "				                         	Definir los costos que se debe incurrir para brindar el servicio"
				+ "				                         </p>"
				+ "				                         <ul>"
				+ "				   							<li>¿Qué recursos clave tienen mayor costo?</li>"
				+ "				   							<li>¿Cuáles son las actividades claves con mayor costo?</li>"
				+ "				   							<li>¿Existen costos variables e indirectos?</li>"
				+ "				   						</ul>";
		
		if(proyecto.getTipo() == Constantes.TIPO_PROYECTO_LIENZO) {
			
			tituloPropuestaValor = "Propuesta de valor";
			tituloRelacionClientes = "Relación con los clientes";
			tituloRecursosClave = "Recursos clave";
			tituloActividadesClave = "Actividades clave";
			tituloSociosClave = "Socios clave";
			
			infoPropuestaValor = "<p>"
					+ "				                         	Este sector está referido a que valor proporciona tu servicio a los usuarios."
					+ "				                         </p>"
					+ "				                         <p>"
					+ "				                         	Debe responder estas preguntas:   "
					+ "				                         </p>"
					+ "				                         <ul>"
					+ "				   							<li>¿Por qué es necesario tu produco o servicio?</li>"
					+ "				   							<li>¿Cuál es el problema que vive el usuario para generar una oportunidad al crear tu servicio?</li>"
					+ "				   							<li>¿Que necesidad satisface tu producto al cliente objetivo?</li>"
					+ "				   							<li>¿Cuáles son tus valores diferenciales? </li>"
					+ "				   						</ul>";
			infoRelacionClientes ="<p>"
					+ "				                         	En función a la propuesta de valor que quieres generar, se debe plantear el tipo de comunicación que se tendra con los clientes."
					+ "				                         </p>"
					+ "				                         <ul>"
					+ "				                     	 	<li>¿Puedes plantear una relación personalizada con tus clientes?</li>"
					+ "				   							<li>¿Es necesario que la marca sea conocida para impulsar las ventas? Si es así, ¿cómo lo vas hacer?</li>"
					+ "				   							<li>¿Podrás generar publicidad gratuita o debe ser de paga?</li>"
					+ "				                     	 </ul>  ";
			infoRecursosClave ="<p>"
					+ "			                         	Elementos clave para que la empresa funcione correctamente"
					+ "			                         </p>"
					+ "			                         <ul>"
					+ "			                         	<li>¿Qué recursos se necesitan para garantizar que las actividades clave se pueden desarrollar de acuerdo al cronograma de la empresa?</li>"
					+ "			                         	<li>¿La empresa necesita unos perfiles determinados? ¿Programación? ¿Marketing?</li>"
					+ "			                         	<li>Patentes/Licencias, ¿necesitas un permiso para poder operar? Puedes proteger algún recurso intelectual</li>"
					+ "			                         	<li>¿Vas a necesitar unas instalaciones determinadas para la empresa? ¿Alquiler? ¿Compra?</li>"
					+ "			                         </ul>";
			infoActividadesClave = " <p>"
					+ "				                         	Una vez definido el área de mercado y clientes."
					+ "										    Para que el cliente [Segmento de clientes] conozca [relación con clientes] el servicio [propuesta de valor]"
					+ "										    y se le entregue [canales] una vez que lo compre [fuente de ingresos] hay una trabajo detrás que la empresa debe llevar a cabo."
					+ "				                         </p>"
					+ "				                         <ul>"
					+ "				                         	<li>"
					+ "				                         		Actividades fundamentales para poder entregar el servicio"
					+ "				                         	</li>"
					+ "				                         </ul>";
			infoSociosClave = " <p>"
					+ "				                         	Entidades ajenas a la empresa que se requiere para que funcione la empresa."
					+ "				    						Generar que tanto la empresa como los socios obtengan beneficios."
					+ "				                         </p>"
					+ "				                         <ul>"
					+ "				                         	<li>"
					+ "				                         		¿Quiénes son nuestros proveedores más importantes?"
					+ "				                         	</li>"
					+ "				                         	<li>"
					+ "				                         		¿Qué tipo de asociación podemos desarrollar? ¿Estratégica? ¿Desarrollo conjunto? ¿Coopetición corporativa?"
					+ "				                         	</li>"
					+ "				                         </ul>";
			
		}else if(proyecto.getTipo() == Constantes.TIPO_PROYECTO_LEAN) {
			
			tituloPropuestaValor = "Propuesta de valor";
			tituloRelacionClientes = "Ventaja especial única";
			tituloRecursosClave = "Métricas clave";
			tituloActividadesClave = "Solución";
			tituloSociosClave = "Problema";

			
			infoPropuestaValor = "<p>"
					+ "				                         	Este sector está referido a que valor proporciona tu servicio a los usuarios."
					+ "				                         </p>"
					+ "				                         <p>"
					+ "				                         	Debe responder estas preguntas:   "
					+ "				                         </p>"
					+ "				                         <ul>"
					+ "				   							<li>¿Que estamos creando?</li>"
					+ "				   							<li>¿Cuál es el problema que vive el usuario para generar una oportunidad al crear tu servicio?</li>"
					+ "				   							<li>¿Que necesidad satisface tu producto al cliente objetivo?</li>"
					+ "				   							<li>¿Cuáles son tus valores diferenciales? </li>"
					+ "				   						</ul>";
			infoRelacionClientes ="<p>"
					+ "No hay que confundir este sector con las ventajas competitivas de su producto/servicio. Este apartado está referido a aquello"
					+ " que <strong>NO SE PUEDE COPIAR O COMPRAR</strong>.</p>"
					+ " <p> Por lo general esta sección es la más díficil de llenar</p>";
			infoRecursosClave ="<p>"
					+ "De que manera vas a medir el progreso de tu proyecto. Posiblemente las métricas cambien conforme pase el tiempo pero en todo momento"
					+ " se debe considerar métricas para medir";
			infoActividadesClave = "<p>"
					+ " La solución que das a esos problemas está <strong>materializada en tu producto o servicio</strong>. Si detectas adecuadamente los problemas, <strong>podrás "
					+ " afinar mucho mejor</strong> la solución que ofreces. Céntrate en desarrollar óptimamente <strong>las 3 características</strong> de tu producto o servicio que "
					+ " van a permitirte dar una solución a los problemas detectados, y deja el resto para más adelante.";
			infoSociosClave = "<p>"
					+ " El problema que resuelve tu producto o servicio es <strong>tu razón de ser como negocio</strong>, así que ten claro a qué problema o problemas le estás dando solución. "
					+ " Se recomienda <strong>detectar los 3 principales problemas</strong> de tu público objetivo relacionados con tu campo de acción.";
		} 
		        
		
		mv.addObject("tituloPropuestaValor", tituloPropuestaValor);
		mv.addObject("tituloRelacionClientes", tituloRelacionClientes);
		mv.addObject("tituloRecursosClave", tituloRecursosClave);
		mv.addObject("tituloActividadesClave", tituloActividadesClave);
		mv.addObject("tituloSociosClave", tituloSociosClave);
		
		mv.addObject("infoSegmentosClientes", infoSegmentosClientes);
		mv.addObject("infoPropuestaValor", infoPropuestaValor);
		mv.addObject("infoCanales", infoCanales);
		mv.addObject("infoRelacionClientes", infoRelacionClientes);
		mv.addObject("infoFuentesIngreso", infoFuentesIngreso);
		mv.addObject("infoRecursosClave", infoRecursosClave);
		mv.addObject("infoActividadesClave", infoActividadesClave);
		mv.addObject("infoSociosClave", infoSociosClave);
		mv.addObject("infoEstructuraCostos", infoEstructuraCostos);
		
	}

	private void fillFiltro(String idProyecto, ModelAndView mv, HttpServletRequest request) {
		// TODO Auto-generated method stub
		String comentariosHtml = "";
		
	}

	private void fillComentarios(String idProyecto, ModelAndView mv, HttpServletRequest request) {
		
		String comentariosHtml = "";
		List<Comentario> comentarios = comentarioService.getComentariosByProyecto(idProyecto);
		String liGenerales ="";
		
		for(Comentario co : comentarios) {
			liGenerales = liGenerales.concat("<li idProyecto='"+co.getIdProyecto()+"' idComentario='"+co.getId()+"' class='item'>");
			liGenerales = liGenerales.concat("<span>");
			liGenerales = liGenerales.concat(co.getComentario());
			liGenerales = liGenerales.concat("</span>");
			liGenerales = liGenerales.concat("<button id='btnResuelta'>Marcar como resuelta</button>");
			liGenerales = liGenerales.concat("</li>");
		}
		
		List<Comentario> comentariosDetalle = comentarioService.getComentariosDetalleByProyecto(idProyecto);
		
		for(Comentario co : comentariosDetalle) {
			liGenerales = liGenerales.concat("<li idProyecto='"+co.getIdProyecto()+"' idComentario='"+co.getId()+"' class='item'>");
			liGenerales = liGenerales.concat("<span><strong>HIPOTESIS:<br></strong>");
			liGenerales = liGenerales.concat(co.getSuposicion());
			liGenerales = liGenerales.concat("</span>");
			
			
			if(co.getEstadoExperimento() == Constantes.ESTADO_EXPERIMENTO_EN_CURSO_NUEVO) {
				liGenerales = liGenerales.concat("<a href='/detalleExperimento?id="+idProyecto+"&idExperimento="+co.getIdExperimento()+"'>Ver comentarios</a>");
			}else if(co.getEstadoExperimento() == Constantes.ESTADO_EXPERIMENTO_EN_CURSO_VIEJO) {
				//mandar a agenda version 
				liGenerales = liGenerales.concat("<a href='"+request.getContextPath()+"/experimento/agenda?id="+idProyecto+"&idExperimento="+co.getIdExperimento()+"'>Ver comentarios</a>");
			}else if(co.getEstadoExperimento() == Constantes.ESTADO_EXPERIMENTO_FINALIZADO) {
				liGenerales = liGenerales.concat("<a href='"+request.getContextPath()+"/experimento/finalizar?id="+idProyecto+"&idExperimento="+co.getIdExperimento()+"'>Ver comentarios</a>");
			}
			liGenerales = liGenerales.concat("</li>");
	
		}
		
		
		
	
		
		
		comentariosHtml = comentariosHtml.concat(liGenerales);
		mv.addObject("comentariosHtml", comentariosHtml);
		
	}

	@RequestMapping("/home/todo")
	public ModelAndView homeTodo(@RequestParam(value="id") String id, @RequestParam(value="estado") String estado, HttpServletRequest request) {
		/*Todas las hipotesis -> Estado 1 y 2*/
		ModelAndView mv = new ModelAndView("home");
		
		Proyecto proyecto = proyectoService.getById(id);
		fillInfoSectores(proyecto, mv, request);
		fillExperimentoLienzo(id, estado, proyecto, mv, request);
		mv.addObject("idProyecto", id);
		return mv;
	}
	
	@RequestMapping("/experimento")
	public ModelAndView experimento(@RequestParam(value="id") String id, @RequestParam(value="estado") String estado, HttpServletRequest request) throws JsonProcessingException {
		
		ModelAndView mv = new ModelAndView("home");
		
		
		String filtroSinExperimento ="";
		filtroSinExperimento = filtroSinExperimento.concat("<button type='button' class='btn btn-primary btn-filtro'><a href='"+request.getContextPath()+"/home?id="+id+"'>Sin experimento</a></button>");
		
		String filtroExperimento ="";
		String filtroEstado3 ="<button type='button' class='btn btn-primary btn-filtro'><a href='"+request.getContextPath()+"/home/experimento?id="+id+"&estado=3'>Sin iniciar</a></button>";
		String filtroEstado4 ="<button type='button' class='btn btn-primary btn-filtro'><a href='"+request.getContextPath()+"/home/experimento?id="+id+"&estado=4'>En curso</a></button>";
		String filtroEstado5 ="<button type='button' class='btn btn-primary btn-filtro'><a href='"+request.getContextPath()+"/home/experimento?id="+id+"&estado=5'>Finalizado</a></button>";

		if(estado.equals("3")) {
			filtroEstado3 ="<button type='button' class='btn btn-primary btn-filtro active'><a href='"+request.getContextPath()+"/home/experimento?id="+id+"&estado=3'>Sin iniciar</a></button>";
		}else if(estado.equals("4")) {
			filtroEstado4 ="<button type='button' class='btn btn-primary btn-filtro active'><a href='"+request.getContextPath()+"/home/experimento?id="+id+"&estado=4'>En curso</a></button>";
		}else if(estado.equals("5")) {
			filtroEstado5 ="<button type='button' class='btn btn-primary btn-filtro active'><a href='"+request.getContextPath()+"/home/experimento?id="+id+"&estado=5'>Finalizado</a></button>";
		}
		
		filtroExperimento = filtroExperimento.concat(filtroEstado3);
		filtroExperimento = filtroExperimento.concat(filtroEstado4);
		filtroExperimento = filtroExperimento.concat(filtroEstado5);
		
		mv.addObject("filtroSinExperimento", filtroSinExperimento);
		mv.addObject("filtroExperimento", filtroExperimento);
		
		Proyecto proyecto = proyectoService.getById(id);
		fillInfoSectores(proyecto, mv, request);
		fillExperimentoLienzo(id, estado, proyecto, mv, request);
		mv.addObject("idProyecto", id);
		
		
		ObjectMapper objectMapper = new ObjectMapper();
		List<NodoArbol> nodos = lienzoService.arbolHipotesisByHipotesis(String.valueOf(proyecto.getId()));
		
		
		
		mv.addObject("arbolObject", objectMapper.writeValueAsString(nodos));
		
		return mv;
	}
	
	@PostMapping("/save")
	@ResponseStatus(value = HttpStatus.OK)
	public void saveUsuario(@RequestBody HipotesisDto hipotesisDto) {
		lienzoService.save(hipotesisDto);
	}
	
	@RequestMapping("/home/hipotesis")
	public ModelAndView editHipotesis(@RequestParam(value="id") String id, @RequestParam(value="idPro") String idProyecto, HttpServletRequest request) throws JsonProcessingException {
		ModelAndView mv = new ModelAndView("hipotesis_edit");
		
		
		ObjectMapper objectMapper = new ObjectMapper();
		List<NodoArbol> nodos = lienzoService.arbolHipotesisByHipotesis(idProyecto);
		
		
		
		mv.addObject("arbolObject", objectMapper.writeValueAsString(nodos));
		
		fillHipotesisEdit(id, idProyecto, mv);
		mv.addObject("idProyecto", idProyecto);
		return mv;
	}
	
	@PostMapping("/editarHipotesis")
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody Map<String,Object> editarHipotesis(@RequestBody HipotesisDto hipotesisDto, HttpServletRequest request) {
		Map<String, Object> parametros = new HashMap<>();
		lienzoService.editarHipotesis(hipotesisDto);
		
		parametros.put("url", request.getContextPath()+"/experimento?id="+hipotesisDto.getIdProyecto()+"&idHipotesis="+hipotesisDto.getId());
		
		return parametros;
	}
	
	@RequestMapping("/comentario/estado")
	@ResponseStatus(value = HttpStatus.OK)
	public void updateEstadoComentario(@RequestParam(value="idComentario") String idComentario, @RequestParam(value="estado") String estado, HttpServletRequest request) {
		comentarioService.updateEstadoComentario(idComentario, estado);

	}
	
	@PostMapping("/saveComentarioProyecto")
	@ResponseStatus(value = HttpStatus.OK)
	public void saveComentarioProyecto(@RequestBody Comentario comentario, HttpServletRequest request) {
		comentarioService.saveComentarioProyecto(comentario);		
	}
	
	@RequestMapping("/miembros")
	public ModelAndView miembros(@RequestParam(value="id") String idProyecto, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView("miembros");
		
		Proyecto proyecto = proyectoService.getById(idProyecto);
		
		
		List<Usuario> usuarios = usuarioService.getByIdProyecto(idProyecto);
		
		String html = "";
		int i = 1;
		for(Usuario u : usuarios){
			html = html.concat("<tr>");
			html = html.concat("<td>"+i+++"</td>");
			if(u.getNombre()!=null)
				html = html.concat("<td>"+u.getNombre()+"</td>");
			else
				html = html.concat("<td>"+u.getEmail()+"</td>");
			html = html.concat("<td>"+u.getEmail()+"</td>");
			html = html.concat("<td><i class='remMetrica fas fa-trash-alt'></i></td>");
			html = html.concat("</tr>");
		}
		mv.addObject("proyecto", proyecto);
		mv.addObject("idProyecto", idProyecto);
		mv.addObject("lista", html);
		return mv;
	}
	
	private void fillExperimentoLienzo(String id, String estado, Proyecto proyecto,ModelAndView mv, HttpServletRequest request) {
		List<ProyectoArea> listProyectoArea = experimentoService.getExperimentoByProyectoAndEstado(id, estado);
		

		String infoSociosClave ="";
		String infoActividadesClave ="";
		String infoRecursosClave ="";
		String infoRelacionClientes ="";
		String infoPropuestaValor ="";
		if(proyecto.getTipo() == Constantes.TIPO_PROYECTO_LIENZO) {
			infoSociosClave ="Socios clave";
			infoActividadesClave ="Actividades clave";
			infoRecursosClave ="Recursos clave";
			infoRelacionClientes ="Relación con clientes";
			infoPropuestaValor ="Propuesta valor";
		}else if(proyecto.getTipo() == Constantes.TIPO_PROYECTO_LEAN) {
			infoSociosClave ="Problema";
			infoActividadesClave ="Solución";
			infoRecursosClave ="Métricas clave";
			infoRelacionClientes ="Ventaja especial";
			infoPropuestaValor ="Propuesta valor";
		}
		
		String tablaAreas = "";
		for(ProyectoArea pA : listProyectoArea) {
			if(pA.getIdArea() == Constantes.ID_SOCIOS_CLAVE) {
				String header = "<h4>"+infoSociosClave+" <i onClick='sociosClave()' class='fas fa-eye fa-2x' style='float:right';></i></h4>";
				String sociosClave = "";
				sociosClave = experimentoToHtml(pA.getExperimentos(), id, header, request);
//				sociosClave = sociosClave.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>"+infoSociosClave+"</td>");
				tablaAreas = tablaAreas.concat("</tr>");
					
				mv.addObject(SOCIOS_CLAVE, sociosClave);
				
			}else if (pA.getIdArea() == Constantes.ID_ACTIVIDADES_CLAVE){
				String header = "<h4>"+infoActividadesClave+" <i onClick='actividadesClave()' class='fas fa-eye fa-2x' style='float:right';></i></h4>";
				String actividadesClave = "";
				actividadesClave = (experimentoToHtml(pA.getExperimentos(), id, header, request));
//				actividadesClave = actividadesClave.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>"+infoActividadesClave+"</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(ACTIVIDADES_CLAVE, actividadesClave);
				
			}else if (pA.getIdArea() == Constantes.ID_RECURSOS_CLAVE){
				String header = "<h4>"+infoRecursosClave+" <i onClick='recursosClave()' class='fas fa-eye fa-2x' style='float:right';></i></h4>";
				String recursosClave = "";
				recursosClave = (experimentoToHtml(pA.getExperimentos(), id, header, request));
//				recursosClave = recursosClave.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>"+infoRecursosClave+"</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(RECURSOS_CLAVE, recursosClave);
				
			}else if (pA.getIdArea() == Constantes.ID_PROPUESTA_VALOR){
				String header = "<h4>"+infoPropuestaValor+" <i onClick='propuestaValor()' class='fas fa-eye fa-2x' style='float:right';></i></h4>";
				String propuestaValor = "";
				propuestaValor = (experimentoToHtml(pA.getExperimentos(), id, header, request));
//				propuestaValor = propuestaValor.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>"+infoPropuestaValor+"</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(PROPUESTA_VALOR, propuestaValor);
				
			}else if (pA.getIdArea() == Constantes.ID_RELACION_CLIENTES){
				String header = "<h4>"+infoRelacionClientes+" <i onClick='reclacionClientes()' class='fas fa-eye fa-2x' style='float:right';></i></h4>";
				String relacionClientes = "";
				relacionClientes = (experimentoToHtml(pA.getExperimentos(), id, header, request));
//				relacionClientes = relacionClientes.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>"+infoRelacionClientes+"</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(RELACION_CLIENTES, relacionClientes);
				
			}else if (pA.getIdArea() == Constantes.ID_CANALES){
				
				String header = "<h4>Canales <i onClick='canales()' class='fas fa-eye fa-2x' style='float:right';></i></h4>";
				String canales = "";
				canales = canales.concat(experimentoToHtml(pA.getExperimentos(), id, header, request));
//				canales = canales.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Canales</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(CANALES, canales);
				
			}else if (pA.getIdArea() == Constantes.ID_SEGMENTOS_CLIENTES){
				String header = "<h4>Segmentos de clientes <i onClick='segmentosClientes()' class='fas fa-eye fa-2x' style='float:right';></i></h4>";
				String segmentosClientes = "";
				segmentosClientes = (experimentoToHtml(pA.getExperimentos(), id, header, request));
//				segmentosClientes = segmentosClientes.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Segmentos de clientes</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(SEGMENTOS_CLIENTES, segmentosClientes);
				
			}else if (pA.getIdArea() == Constantes.ID_ESTRUCTURA_COSTOS){
				String header = "<h4>Estructura de costos <i onClick='estructuraCostos()' class='fas fa-eye fa-2x' style='float:right';></i></h4>";
				String estructuraCostos = "";
				estructuraCostos = (experimentoToHtml(pA.getExperimentos(), id, header, request));
//				estructuraCostos = estructuraCostos.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Estructura de costos</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(ESTRUCTURA_COSTOS, estructuraCostos);
				
			}else if (pA.getIdArea() == Constantes.ID_FUENTES_INGRESO){
				String header = "<h4>Fuentes de ingreso <i onClick='fuentesIngreso()' class='fas fa-eye fa-2x' style='float:right';></i></h4>";
				String fuentesIngreso = "";
				fuentesIngreso = (experimentoToHtml(pA.getExperimentos(), id, header, request));
//				fuentesIngreso = fuentesIngreso.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Fuentes de ingreso</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(FUENTES_INGRESO, fuentesIngreso);
			}
		}
		mv.addObject("tablaAreas", tablaAreas);
		
		
		
	}

	private void fillHipotesisEdit(String id, String idProyecto, ModelAndView mv) {
		HipotesisDto hipotesisDto = lienzoService.getHipotesisDtoById(id);	
//		List<Usuario> usuarios = usuarioService.getByIdProyecto(idProyecto);
		filltableAreaEdit(idProyecto, mv, hipotesisDto.getProyectoAreas());
//		String listMiembros = "";
//		for(Usuario u: usuarios) {
//			listMiembros = listMiembros.concat("<option value='"+u.getId()+"'>"+u.getNombre()+" " + u.getApellidoPaterno()+"</option>");
//		}
//		mv.addObject("listMiembros", listMiembros);
		System.out.println("Fecha registro" + hipotesisDto.getFechaRegistro());
		String fechaRegistro = (hipotesisDto.getFechaRegistro() == null) ? "-" : hipotesisDto.getFechaRegistro();
		
		mv.addObject("fechaRegistro", hipotesisDto.getFechaRegistro());
		mv.addObject("hipotesisDto", hipotesisDto);
		mv.addObject("idProyecto", idProyecto);
	}

	private void filltableAreaEdit(String idProyecto, ModelAndView mv, List<ProyectoArea> proyectoArea) {
		List<ProyectoArea> listProyectoArea = lienzoService.getLienzoByProyecto(idProyecto);

		String sociosClave ="";
		String actividadesClave = "";
		String recursosClave = "";
		String propuestaValor ="";
		String relacionClientes ="";
		String canales = "";
		String segmentosClientes ="";
		String estructuraCostos = "";
		String fuentesIngreso ="";
		for(ProyectoArea pA : listProyectoArea) {
			if(pA.getIdArea() == Constantes.ID_SOCIOS_CLAVE) {
				
				if(areaSeleccionada(pA.getIdArea(), proyectoArea)) 
					sociosClave = sociosClave.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_sociosClave' class='checkbox-circle' checked>");
				else
					sociosClave = sociosClave.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_sociosClave' class='checkbox-circle'>");
				sociosClave = sociosClave.concat("<label for='check_sociosClave'>"
													+ "    <div class='tick_mark'></div>"
													+ "</label>");
				

				
			}else if (pA.getIdArea() == Constantes.ID_ACTIVIDADES_CLAVE){
				
				if(areaSeleccionada(pA.getIdArea(), proyectoArea)) 
					actividadesClave = actividadesClave.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_actividadesClave' class='checkbox-circle' checked>");
				else
					actividadesClave = actividadesClave.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_actividadesClave' class='checkbox-circle'>");
				actividadesClave = actividadesClave.concat("<label for='check_actividadesClave'>"
													+ "    <div class='tick_mark'></div>"
													+ "</label>");
				

				
			}else if (pA.getIdArea() == Constantes.ID_RECURSOS_CLAVE){
				if(areaSeleccionada(pA.getIdArea(), proyectoArea)) 
					recursosClave = recursosClave.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_recursosClave' class='checkbox-circle' checked>");
				else
					recursosClave = recursosClave.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_recursosClave' class='checkbox-circle'>");
				recursosClave = recursosClave.concat("<label for='check_recursosClave'>"
													+ "    <div class='tick_mark'></div>"
													+ "</label>");
				
			}else if (pA.getIdArea() == Constantes.ID_PROPUESTA_VALOR){
				
				if(areaSeleccionada(pA.getIdArea(), proyectoArea)) 
					propuestaValor = propuestaValor.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_propuestaValor' class='checkbox-circle' checked>");
				else
					propuestaValor = propuestaValor.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_propuestaValor' class='checkbox-circle'>");
				propuestaValor = propuestaValor.concat("<label for='check_propuestaValor'>"
													+ "    <div class='tick_mark'></div>"
													+ "</label>");
				
			}else if (pA.getIdArea() == Constantes.ID_RELACION_CLIENTES){
				
				if(areaSeleccionada(pA.getIdArea(), proyectoArea)) 
					relacionClientes = relacionClientes.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_relacionClientes' class='checkbox-circle' checked>");
				else
					relacionClientes = relacionClientes.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_relacionClientes' class='checkbox-circle'>");
				relacionClientes = relacionClientes.concat("<label for='check_relacionClientes'>"
													+ "    <div class='tick_mark'></div>"
													+ "</label>");
				
			}else if (pA.getIdArea() == Constantes.ID_CANALES){
				
				if(areaSeleccionada(pA.getIdArea(), proyectoArea)) 
					canales = canales.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_canales' class='checkbox-circle' checked>");
				else
					canales = canales.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_canales' class='checkbox-circle'>");
				canales = canales.concat("<label for='check_canales'>"
													+ "    <div class='tick_mark'></div>"
													+ "</label>");
				
			}else if (pA.getIdArea() == Constantes.ID_SEGMENTOS_CLIENTES){
				
				if(areaSeleccionada(pA.getIdArea(), proyectoArea)) 
					segmentosClientes = segmentosClientes.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_segmentacionClientes' class='checkbox-circle' checked>");
				else
					segmentosClientes = segmentosClientes.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_segmentacionClientes' class='checkbox-circle'>");
				segmentosClientes = segmentosClientes.concat("<label for='check_segmentacionClientes'>"
													+ "    <div class='tick_mark'></div>"
													+ "</label>");
				
			}else if (pA.getIdArea() == Constantes.ID_ESTRUCTURA_COSTOS){
				
				if(areaSeleccionada(pA.getIdArea(), proyectoArea)) 
					estructuraCostos = estructuraCostos.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_estructuraCostos' class='checkbox-circle' checked>");
				else
					estructuraCostos = estructuraCostos.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_estructuraCostos' class='checkbox-circle'>");
				estructuraCostos = estructuraCostos.concat("<label for='check_estructuraCostos'>"
													+ "    <div class='tick_mark'></div>"
													+ "</label>");
				
			}else if (pA.getIdArea() == Constantes.ID_FUENTES_INGRESO){
				
				if(areaSeleccionada(pA.getIdArea(), proyectoArea)) 
					fuentesIngreso = fuentesIngreso.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_fuentesIngreso' class='checkbox-circle' checked>");
				else
					fuentesIngreso = fuentesIngreso.concat("<input idProyectoArea='"+pA.getId()+"' type='checkbox' id='check_fuentesIngreso' class='checkbox-circle'>");
				fuentesIngreso = fuentesIngreso.concat("<label for='check_fuentesIngreso'>"
													+ "    <div class='tick_mark'></div>"
													+ "</label>");
			}
		}
		mv.addObject("sociosClave", sociosClave);
		mv.addObject("actividadesClave", actividadesClave);
		mv.addObject("recursosClave", recursosClave);
		mv.addObject("propuestaValor", propuestaValor);
		mv.addObject("relacionClientes", relacionClientes);
		mv.addObject("canales", canales);
		mv.addObject("segmentosClientes", segmentosClientes);
		mv.addObject("estructuraCostos", estructuraCostos);
		mv.addObject("fuentesIngreso", fuentesIngreso);
		
		
	}

	private boolean areaSeleccionada(int idArea, List<ProyectoArea> proyectoArea) {
		for(ProyectoArea pA : proyectoArea) {
			if(pA.getIdArea() == idArea)
				return true;
		}
		return false;
	}

	private void fillHipotesisLienzo(String id, Proyecto proyecto, ModelAndView mv, HttpServletRequest request) {
		//infoPropuestaValor infoRelacionClientes   
		List<ProyectoArea> listProyectoArea = lienzoService.getLienzoByProyecto(id);
		String tablaAreas = "";
		String infoSociosClave ="";
		String infoActividadesClave ="";
		String infoRecursosClave ="";
		String infoRelacionClientes ="";
		String infoPropuestaValor ="";
		if(proyecto.getTipo() == Constantes.TIPO_PROYECTO_LIENZO) {
			infoSociosClave ="Socios clave";
			infoActividadesClave ="Actividades clave";
			infoRecursosClave ="Recursos clave";
			infoRelacionClientes ="Relación con clientes";
			infoPropuestaValor ="Propuesta valor";
		}else if(proyecto.getTipo() == Constantes.TIPO_PROYECTO_LEAN) {
			infoSociosClave ="Problema";
			infoActividadesClave ="Solución";
			infoRecursosClave ="Métricas clave";
			infoRelacionClientes ="Ventaja especial";
			infoPropuestaValor ="Propuesta valor";
		}
		
		for(ProyectoArea pA : listProyectoArea) {
			
			if(pA.getIdArea() == Constantes.ID_SOCIOS_CLAVE) {
				String header = "<h4>"+infoSociosClave+" <i onClick='sociosClave()' class='fas fa-eye fa-2x' style='float:right';></i></h4>";
				String sociosClave = "";
				sociosClave = hipotesisToHtml(pA.getHipotesis(), id, header, request);
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>"+infoSociosClave+"</td>");
				tablaAreas = tablaAreas.concat("</tr>");
					
				mv.addObject(SOCIOS_CLAVE, sociosClave);
				
			}else if (pA.getIdArea() == Constantes.ID_ACTIVIDADES_CLAVE){
				String header = "<h4>"+infoActividadesClave+" <i onClick='actividadesClave()' class='fas fa-eye fa-2x' style='float:right';></i></h4>";
				String actividadesClave = "";
				actividadesClave = (hipotesisToHtml(pA.getHipotesis(), id, header, request));
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>"+infoActividadesClave+"</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(ACTIVIDADES_CLAVE, actividadesClave);
				
			}else if (pA.getIdArea() == Constantes.ID_RECURSOS_CLAVE){
				String header = "<h4>"+infoRecursosClave+" <i onClick='recursosClave()' class='fas fa-eye fa-2x' style='float:right';></i></h4>";
				String recursosClave = "";
				recursosClave = (hipotesisToHtml(pA.getHipotesis(), id, header, request));
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>"+infoRecursosClave+"</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(RECURSOS_CLAVE, recursosClave);
				
			}else if (pA.getIdArea() == Constantes.ID_PROPUESTA_VALOR){
				String header = "<h4>"+infoPropuestaValor+" <i onClick='propuestaValor()' class='fas fa-eye fa-2x' style='float:right';></i></h4>";
				String propuestaValor = "";
				propuestaValor = (hipotesisToHtml(pA.getHipotesis(), id, header, request));
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>"+infoPropuestaValor+"</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(PROPUESTA_VALOR, propuestaValor);
				
			}else if (pA.getIdArea() == Constantes.ID_RELACION_CLIENTES){
				String header = "<h4>"+infoRelacionClientes+" <i onClick='reclacionClientes()' class='fas fa-eye fa-2x' style='float:right';></i></h4>";
				String relacionClientes = "";
				relacionClientes = (hipotesisToHtml(pA.getHipotesis(), id, header, request));
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>"+infoRelacionClientes+"</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(RELACION_CLIENTES, relacionClientes);
				
			}else if (pA.getIdArea() == Constantes.ID_CANALES){
				
				String header = "<h4>Canales <i onClick='canales()' class='fas fa-eye fa-2x' style='float:right';></i></h4>";
				String canales = "";
				canales = canales.concat(hipotesisToHtml(pA.getHipotesis(), id, header, request));
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Canales</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(CANALES, canales);
				
			}else if (pA.getIdArea() == Constantes.ID_SEGMENTOS_CLIENTES){
				String header = "<h4>Segmentos de clientes <i onClick='segmentosClientes()' class='fas fa-eye fa-2x' style='float:right';></i></h4>";
				String segmentosClientes = "";
				segmentosClientes = (hipotesisToHtml(pA.getHipotesis(), id, header, request));
//				segmentosClientes = segmentosClientes.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Segmentos de clientes</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(SEGMENTOS_CLIENTES, segmentosClientes);
				
			}else if (pA.getIdArea() == Constantes.ID_ESTRUCTURA_COSTOS){
				String header = "<h4>Estructura de costos <i onClick='estructuraCostos()' class='fas fa-eye fa-2x' style='float:right';></i></h4>";
				String estructuraCostos = "";
				estructuraCostos = (hipotesisToHtml(pA.getHipotesis(), id, header, request));
//				estructuraCostos = estructuraCostos.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Estructura de costos</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(ESTRUCTURA_COSTOS, estructuraCostos);
				
			}else if (pA.getIdArea() == Constantes.ID_FUENTES_INGRESO){
				String header = "<h4>Fuentes de ingreso <i onClick='fuentesIngreso()' class='fas fa-eye fa-2x' style='float:right';></i></h4>";
				String fuentesIngreso = "";
				fuentesIngreso = (hipotesisToHtml(pA.getHipotesis(), id, header, request));
//				fuentesIngreso = fuentesIngreso.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Fuentes de ingreso</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(FUENTES_INGRESO, fuentesIngreso);
			}
		}
		mv.addObject("tablaAreas", tablaAreas);
		
	} 
		
	private String experimentoToHtml(List<Experimento> listHipotesis, String idProyecto, String html,  HttpServletRequest request) {
		html = html.concat("<ul>");
		for(Experimento h : listHipotesis) {
			if(h.getEstado() == Constantes.ESTADO_EXPERIMENTO_EN_CURSO_NUEVO) {
				html = html.concat("<li> <a href='/detalleExperimento?id="+idProyecto+"&idExperimento="+h.getId()+"'><h2>"+h.getHipotesis()+"</h2></a></li>");
			}else if(h.getEstado() == Constantes.ESTADO_EXPERIMENTO_EN_CURSO_VIEJO) {
				//mandar a agenda version 
				html = html.concat("<li> <a href='"+request.getContextPath()+"/experimento/agenda?id="+idProyecto+"&idExperimento="+h.getId()+"'><h2>"+h.getHipotesis()+"</h2></a></li>");
			}else if(h.getEstado() == Constantes.ESTADO_EXPERIMENTO_FINALIZADO) {
				html = html.concat("<li> <a href='"+request.getContextPath()+"/experimento/finalizar?id="+idProyecto+"&idExperimento="+h.getId()+"'><h2>"+h.getHipotesis()+"</h2></a></li>");
			}
			
		}
		html = html.concat("</ul>");
		return html;
	}
	
	private String hipotesisToHtml(List<Hipotesis> listHipotesis, String idProyecto, String html, HttpServletRequest request) {
		html = html.concat("<ul>");
		for(Hipotesis h : listHipotesis) {
			String descripcion = (h.getDescripcion() == null) ? "" : h.getDescripcion(); 
			if(h.getEstado() == Constantes.ESTADO_HIPOTESIS_ACTIVO) {
				html = html.concat("<li> <a href='home/hipotesis?id="+h.getId()+"&idPro="+idProyecto+"'><h2>"+h.getSuposicion()+"</h2></a></li>");
			}else if(h.getEstado() == Constantes.ESTADO_HIPOTESIS_CREAR_EXPERIMENTO) {
				//mandar a crear experimento flujo nuevo
				html = html.concat("<li> <a href='"+request.getContextPath()+"/experimento?id="+idProyecto+"&idHipotesis="+h.getId()+"'><h2>"+h.getSuposicion()+"</h2></a></li>");
			}
			
			
		}
		html = html.concat("</ul>");
		return html;
	}

	private String getRandomColor() {
		Random rand = new Random();
		float r = rand.nextInt(240);
		float g = rand.nextInt(240);
		float b = rand.nextInt(240);
		while(r==255 && g==255 && b==255) {
			r = rand.nextInt();
			g = rand.nextInt();
			b = rand.nextInt();
		}
		return "rgb("+r+","+g+","+b+")";
	}
}
