package com.tesis.lienzo.app.lienzanier.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PruebaHipotesis implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String nombre;
	private String descripcion;
	private int estado;
	private String nombreList;
	private String nombreUpper;
}
