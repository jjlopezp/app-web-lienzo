package com.tesis.lienzo.app.lienzanier.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.tesis.lienzo.app.lienzanier.bean.Comentario;

@FeignClient(name = "comentario", url = "${ws.lienzo}/comentario")
public interface ComentarioFeign {
	
	@GetMapping("/getComentariosByProyecto")
	List<Comentario> getComentariosByProyecto(@RequestParam(value="idProyecto") String idProyecto);

	@GetMapping("/updateEstadoComentario")
	void updateEstadoComentario(@RequestParam(value="idComentario") String idComentario, @RequestParam(value="estado") String estado);

	@PostMapping("/saveComentarioProyecto")
	void saveComentarioProyecto(@RequestBody Comentario comentario);

	@PostMapping("/saveComentarioExperimento")
	void saveComentarioExperimento(Comentario comentario);

	@GetMapping("/getComentariosByExperimento")
	List<Comentario> getComentariosByExperimento(@RequestParam(value="idExperimento") String idExperimento);

	@GetMapping("/getComentariosDetalleByProyecto")
	List<Comentario> getComentariosDetalleByProyecto(@RequestParam(value="idProyecto") String idProyecto);

	@GetMapping("/updateEstadoComentarioExperimento")
	void updateEstadoComentarioExperimento(@RequestParam(value="idComentario") String idComentario, @RequestParam(value="estado") String estado);

}
