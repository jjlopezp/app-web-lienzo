package com.tesis.lienzo.app.lienzanier.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.tesis.lienzo.app.lienzanier.bean.Proyecto;


@FeignClient(name = "proyecto", url = "${ws.lienzo}/proyecto")
public interface ProyectoFeignClient {

	@GetMapping("/getByUser")
	List<Proyecto> getByUser(@RequestParam(value="id") String id);
	
	@PostMapping("/update")
	void update(@RequestBody Proyecto proyecto);
	
	@GetMapping("/delete")
	void delete(@RequestParam(value="id") String id);
	
	@PostMapping("/save")
	String save(@RequestBody Proyecto proyecto);

	@GetMapping("/getById")
	Proyecto getById(@RequestParam(value="id") String idProyecto);
}
