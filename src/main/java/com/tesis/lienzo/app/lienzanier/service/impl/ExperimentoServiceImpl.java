package com.tesis.lienzo.app.lienzanier.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.tesis.lienzo.app.lienzanier.bean.Experimento;
import com.tesis.lienzo.app.lienzanier.bean.ProyectoArea;
import com.tesis.lienzo.app.lienzanier.bean.PruebaHipotesis;
import com.tesis.lienzo.app.lienzanier.dto.ExperimentoDto;
import com.tesis.lienzo.app.lienzanier.feign.ExperimentoFeignClient;
import com.tesis.lienzo.app.lienzanier.service.ExperimentoService;

@Service
public class ExperimentoServiceImpl implements ExperimentoService{

	@Autowired
	ExperimentoFeignClient experimentoFeignClient;
	
	@Override
	public List<PruebaHipotesis> getPruebasHipotesis() {
		// TODO Auto-generated method stub
		return experimentoFeignClient.getByProyecto();
	}

	@Override
	public ResponseEntity<Integer> save(ExperimentoDto experimentDto) {
		// TODO Auto-generated method stub
		return experimentoFeignClient.save(experimentDto);
	}

	@Override
	public List<Experimento> getExperimentosByProjecto(String id) {
		// TODO Auto-generated method stub
		return experimentoFeignClient.getExperimentosByProjecto(id);
	}

	@Override
	public ResponseEntity<String> updateEstado(String idExperimento, String estado) {
		// TODO Auto-generated method stub
		return experimentoFeignClient.updateEstado(idExperimento, estado);
		
	}

	@Override
	public Experimento getDetalleExperimento(String idExperimento) {
		// TODO Auto-generated method stub
		return experimentoFeignClient.getDetalleExperimento(idExperimento);
	}

	@Override
	public List<ProyectoArea> getExperimentoByProyectoAndEstado(String id, String estado) {
		// TODO Auto-generated method stub
		return experimentoFeignClient.getExperimentoByProyectoAndEstado(id, estado);
	}

	@Override
	public void saveMediciones(Experimento experimento) {
		// TODO Auto-generated method stub
		experimentoFeignClient.saveMediciones(experimento);
	}

	@Override
	public void updateMediciones(Experimento experimento) {
		// TODO Auto-generated method stub
		experimentoFeignClient.updateMediciones(experimento);
	}

	@Override
	public void finalizarExperimento(String idHipotesis, String idProyecto, String idExperimento) {
		// TODO Auto-generated method stub
		experimentoFeignClient.finalizarExperimento(idHipotesis, idProyecto, idExperimento);
	}

	@Override
	public void guardarCambiosExperimento(ExperimentoDto experimentDto) {
		// TODO Auto-generated method stub
		experimentoFeignClient.guardarCambiosExperimento(experimentDto);
	}

}
