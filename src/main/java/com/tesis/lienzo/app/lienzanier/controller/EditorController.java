package com.tesis.lienzo.app.lienzanier.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
//import org.apache.commons.lang.ArrayUtils;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

@Controller
@RequestMapping("/editor")
public class EditorController {
	
	@Autowired
	ServletContext servletContext;
	
	@RequestMapping(value = "/upload_image", method = RequestMethod.POST)
	public void submit(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
	    
		
		
	   System.out.println("LLEGA UPLOAD");
	   
	   
	// The route on which the file is saved.
       File uploads = new File(   servletContext.getRealPath("/resources/img/editor/")  );
       String multipartContentType = "multipart/form-data";
       String fieldname = "file";
       Part filePart = request.getPart(fieldname);
       Map < Object, Object > responseData = null;

       // Create path components to save the file.
       final PrintWriter writer = response.getWriter();

       String linkName = null;
       String name = null;

       try {
           // Check content type.
           if (request.getContentType() == null ||
               request.getContentType().toLowerCase().indexOf(multipartContentType) == -1) {

               throw new Exception("Invalid contentType. It must be " + multipartContentType);
           }

           // Get file Part based on field name and also image extension.
           filePart = request.getPart(fieldname);
           String type = filePart.getContentType();
           type = type.substring(type.lastIndexOf("/") + 1);

           // Generate random name.
           String extension = type;
           extension = (extension != null && extension != "") ? "." + extension : extension;
           name = UUID.randomUUID().toString() + extension ;

           // Get absolute server path.
           String absoluteServerPath = uploads + name;

           // Create link.
           String path = request.getHeader("origin");
           linkName = path + "/files/" + name;

           // Validate image.
           String mimeType = filePart.getContentType();
           String[] allowedExts = new String[] {
               "gif",
               "jpeg",
               "jpg",
               "png",
               "svg",
               "blob"
           };
           String[] allowedMimeTypes = new String[] {
               "image/gif",
               "image/jpeg",
               "image/pjpeg",
               "image/x-png",
               "image/png",
               "image/svg+xml"
           };

           // Save the file on server.

           File file = new File(uploads, name);

           try (InputStream input = filePart.getInputStream()) {
               Files.copy(input, file.toPath());
           } catch (Exception e) {
           	writer.println("<br/> ERROR: " + e);
           }

       } catch (Exception e) {
           e.printStackTrace();
           writer.println("You either did not specify a file to upload or are " +
               "trying to upload a file to a protected or nonexistent " +
               "location.");
           writer.println("<br/> ERROR: " + e.getMessage());
           responseData = new HashMap < Object, Object > ();
           responseData.put("error", e.toString());

       } finally {
           // Build response data.
           responseData = new HashMap < Object, Object > ();
           System.out.println("++++++++++++++LINK NAME " + linkName);
           responseData.put("link", linkName);

           // Send response data.
           String jsonResponseData = new Gson().toJson(responseData);
           response.setContentType("application/json");
           response.setCharacterEncoding("UTF-8");
           response.getWriter().write(jsonResponseData);
       }
	   
	   
	   
	}
	
	@RequestMapping(value = "/upload_file", method = RequestMethod.POST)
	public void upload_file(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		// The route on which the file is saved.
		File uploads = new File(   servletContext.getRealPath("/resources/img/editor/")  );

	      String multipartContentType = "multipart/form-data";
	      String fieldname = "file";
	      Part filePart = request.getPart(fieldname);
	
	      // Create path components to save the file.
	      Map < Object, Object > responseData;
	      final PrintWriter writer = response.getWriter();
	      String linkName = null;
	
	      try {
	          // Check content type.
	          if (request.getContentType() == null ||
	              request.getContentType().toLowerCase().indexOf(multipartContentType) == -1) {
	              throw new Exception("Invalid contentType. It must be " + multipartContentType);
	          }
	
	          // Get file Part based on field name and also file extension.
	          String type = filePart.getContentType();
	          type = type.substring(type.lastIndexOf("/") + 1);
	
	          // Generate random name.
	          String extension = type;
	          extension = (extension != null && extension != "") ? "." + extension : extension;
	          String name = UUID.randomUUID().toString() + extension;
	
	          // Get absolute server path.
	          String absoluteServerPath = uploads + name;
	
	          // Create link.
	          String path = request.getHeader("origin");
	           linkName = path + "/files/" + name;

	
	          // Validate file.
	          String mimeType = filePart.getContentType();
	          String[] allowedExts = new String[] {
	              "txt",
	              "pdf",
	              "doc"
	          };
	          String[] allowedMimeTypes = new String[] {
	              "text/plain",
	              "application/msword",
	              "application/x-pdf",
	              "application/pdf"
	          };
	
	        
	          // Save the file on server.
	          File file = new File(uploads, name);
	
	          try (InputStream input = filePart.getInputStream()) {
	              Files.copy(input, file.toPath());
	          }catch (Exception e) {
	          	writer.println("<br/> ERROR: " + e);
	          }
	
	      } catch (Exception e) {
	          e.printStackTrace();
	          writer.println("You either did not specify a file to upload or are " +
	              "trying to upload a file to a protected or nonexistent " +
	              "location.");
	          writer.println("<br/> ERROR: " + e.getMessage());
	          responseData = new HashMap < Object, Object > ();
	          responseData.put("error", e.toString());
	
	      } finally {
	          responseData = new HashMap < Object, Object > ();
	          responseData.put("link", linkName);
	
	          // Send response data.
	          String jsonResponseData = new Gson().toJson(responseData);
	          response.setContentType("application/json");
	          response.setCharacterEncoding("UTF-8");
	          response.getWriter().write(jsonResponseData);
	      }
      
	  }
		
		
	
	@RequestMapping(value = "/upload_video", method = RequestMethod.POST)
	public void upload_video(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		System.out.println("ENTREGAN VIDEO");
		
		 // The route on which the file is saved.
		File uploads = new File(   servletContext.getRealPath("/resources/img/editor/")  );

	       String multipartContentType = "multipart/form-data";
	       String fieldname = "file";
	       Part filePart = request.getPart(fieldname);

	       // Create path components to save the file.
	       Map < Object, Object > responseData;
	       final PrintWriter writer = response.getWriter();
	       String linkName = null;

	       try {
	           // Check content type.
	           if (request.getContentType() == null ||
	               request.getContentType().toLowerCase().indexOf(multipartContentType) == -1) {

	               throw new Exception("Invalid contentType. It must be " + multipartContentType);
	           }

	           // Get file Part based on field name and also file extension.
	           String type = filePart.getContentType();
	           type = type.substring(type.lastIndexOf("/") + 1);

	           // Generate random name.
	           String extension = type;
	           extension = (extension != null && extension != "") ? "." + extension : extension;
	           String name = UUID.randomUUID().toString() + extension;

	           // Get absolute server path.
	           String absoluteServerPath = uploads + name;

	        // Create link.
	          String path = request.getHeader("origin");
	           linkName = path + "/files/" + name;

	           // Validate file.
	           String mimeType = filePart.getContentType();
	           String[] allowedExts = new String[] {
	               "mp4",
	               "webm",
	               "ogg"
	           };
	           String[] allowedMimeTypes = new String[] {
	               "video/mp4",
	               "video/webm",
	               "video/ogg"
	           };


	           // Save the file on server.
	           File file = new File(uploads, name);

	           try (InputStream input = filePart.getInputStream()) {
	               Files.copy(input, file.toPath());
	           } catch (Exception e) {
	            writer.println("<br/> ERROR: " + e);
	           }

	       } catch (Exception e) {
	           e.printStackTrace();
	           writer.println("You either did not specify a file to upload or are " +
	               "trying to upload a file to a protected or nonexistent " +
	               "location.");
	           writer.println("<br/> ERROR: " + e.getMessage());
	           responseData = new HashMap < Object, Object > ();
	           responseData.put("error", e.toString());

	       } finally {
	           responseData = new HashMap < Object, Object > ();
	           responseData.put("link", linkName);

	           // Send response data.
	           String jsonResponseData = new Gson().toJson(responseData);
	           response.setContentType("application/json");
	           response.setCharacterEncoding("UTF-8");
	           response.getWriter().write(jsonResponseData);
	       }
		
	}

}
