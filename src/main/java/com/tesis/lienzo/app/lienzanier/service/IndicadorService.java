package com.tesis.lienzo.app.lienzanier.service;

import java.util.List;



import com.tesis.lienzo.app.lienzanier.bean.Indicador;

public interface IndicadorService {

	List<Indicador> listar(String idProyecto);

	void save(Indicador indicador);

	Indicador getIndicador(String idIndicador);

	void saveMediciones(Indicador indicador);

}
