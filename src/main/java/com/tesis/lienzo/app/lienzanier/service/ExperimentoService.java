package com.tesis.lienzo.app.lienzanier.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.tesis.lienzo.app.lienzanier.bean.Experimento;
import com.tesis.lienzo.app.lienzanier.bean.ProyectoArea;
import com.tesis.lienzo.app.lienzanier.bean.PruebaHipotesis;
import com.tesis.lienzo.app.lienzanier.dto.ExperimentoDto;

public interface ExperimentoService {

	List<PruebaHipotesis> getPruebasHipotesis();

	ResponseEntity<Integer> save(ExperimentoDto experimentDto);

	List<Experimento> getExperimentosByProjecto(String id);

	ResponseEntity<String> updateEstado(String idExperimento, String estado);

	Experimento getDetalleExperimento(String idExperimento);

	List<ProyectoArea> getExperimentoByProyectoAndEstado(String id, String estado);

	void saveMediciones(Experimento experimento);

	void updateMediciones(Experimento experimento);

	void finalizarExperimento(String idHipotesis, String idProyecto, String idExperimento);

	void guardarCambiosExperimento(ExperimentoDto experimentDto);
}
