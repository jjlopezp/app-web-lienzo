package com.tesis.lienzo.app.lienzanier.bean;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Experimento {

	private int id;
	private String hipotesis;
	private int idHipotesis;
	private String descripcion;
	private int estado;
	private int idProyecto;
	private String txtEstado;
	private String tipoPrueba;
	private List<Metrica> metricas;
	private List<Condicion> condiciones;
	private List<Fecha> fechas;
	private String evidencias;
	
}

