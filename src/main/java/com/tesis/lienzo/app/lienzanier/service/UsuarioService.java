package com.tesis.lienzo.app.lienzanier.service;

import java.util.List;

import com.tesis.lienzo.app.lienzanier.bean.Usuario;

public interface UsuarioService {

	List<Usuario> getByEmail(String email, String id);

	boolean validateUsuarioRegistrado(String usuario);

	boolean validateEmailRegistrado(String usuario);

	void save(Usuario usuario);

	void update(Usuario usuario);
	
	List<Usuario> getByIdProyecto(String idProyecto);
}
