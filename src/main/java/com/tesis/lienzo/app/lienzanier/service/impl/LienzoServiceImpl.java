package com.tesis.lienzo.app.lienzanier.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tesis.lienzo.app.lienzanier.bean.Hipotesis;
import com.tesis.lienzo.app.lienzanier.bean.NodoArbol;
import com.tesis.lienzo.app.lienzanier.bean.ProyectoArea;
import com.tesis.lienzo.app.lienzanier.dto.HipotesisDto;
import com.tesis.lienzo.app.lienzanier.feign.LienzoFeignClient;
import com.tesis.lienzo.app.lienzanier.service.LienzoService;

@Service
public class LienzoServiceImpl implements LienzoService{

	@Autowired
	LienzoFeignClient lienzoFeignClient;
	
	@Override
	public List<ProyectoArea> getLienzoByProyecto(String id) {
		// TODO Auto-generated method stub
		return lienzoFeignClient.getByProyecto(id);
	}
	
	@Override
	public List<Hipotesis> getHipotesisByProyecto(String idProyecto) {
		// TODO Auto-generated method stub
		return lienzoFeignClient.getHipotesisByProyecto(idProyecto);
	}

	@Override
	public String save(HipotesisDto hipotesisDto) {
		// TODO Auto-generated method stub
		return lienzoFeignClient.save(hipotesisDto);
	}

	@Override
	public HipotesisDto getHipotesisDtoById(String id) {
		// TODO Auto-generated method stub
		return lienzoFeignClient.getHipotesisDtoById(id);
	}

	@Override
	public void editarHipotesis(HipotesisDto hipotesisDto) {
		// TODO Auto-generated method stub
		lienzoFeignClient.editarHipotesis(hipotesisDto);
	}

	@Override
	public void guardarCambiosHipotesis(HipotesisDto hipotesisDto) {
		// TODO Auto-generated method stub
		lienzoFeignClient.guardarCambiosHipotesis(hipotesisDto);
	}

	@Override
	public List<NodoArbol> arbolHipotesisByHipotesis(String idProyecto) {
		// TODO Auto-generated method stub
		return lienzoFeignClient.arbolHipotesisByHipotesis(idProyecto);
	}

}
