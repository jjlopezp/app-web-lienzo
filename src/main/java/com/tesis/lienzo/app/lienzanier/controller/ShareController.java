
package com.tesis.lienzo.app.lienzanier.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tesis.lienzo.app.lienzanier.bean.Comentario;
import com.tesis.lienzo.app.lienzanier.bean.Condicion;
import com.tesis.lienzo.app.lienzanier.bean.Experimento;
import com.tesis.lienzo.app.lienzanier.bean.Hipotesis;
import com.tesis.lienzo.app.lienzanier.bean.Metrica;
import com.tesis.lienzo.app.lienzanier.bean.ProyectoArea;
import com.tesis.lienzo.app.lienzanier.service.ComentarioService;
import com.tesis.lienzo.app.lienzanier.service.ExperimentoService;
import com.tesis.lienzo.app.lienzanier.service.LienzoService;
import com.tesis.lienzo.app.lienzanier.util.Constantes;

@Controller
@RequestMapping("/share")
public class ShareController {
	
	private final String SOCIOS_CLAVE = "sociosClave";
	private final String ACTIVIDADES_CLAVE = "actividadesClave";
	private final String RECURSOS_CLAVE = "recursosClave";
	private final String PROPUESTA_VALOR = "propuestaValor";
	private final String RELACION_CLIENTES = "relacionClientes";
	private final String CANALES = "canales";
	private final String SEGMENTOS_CLIENTES = "segmentosClientes";
	private final String ESTRUCTURA_COSTOS = "estructuraCostos";
	private final String FUENTES_INGRESO = "fuentesIngreso";
	
	@Autowired
	ComentarioService comentarioService;
	
	@Autowired
	LienzoService lienzoService;
	
	@Autowired
	ExperimentoService experimentoService;

	@RequestMapping("/hipotesis/{idProyecto}")
	public ModelAndView share(@PathVariable String idProyecto, HttpServletRequest request) {
		/*Returna las hipotesis*/
		ModelAndView mv = new ModelAndView("home_share");
		fillHipotesisLienzo(idProyecto, mv, request);
		fillComentarios(idProyecto, mv, request);
		mv.addObject("idProyecto", idProyecto);
		return mv;
	}
	
	@RequestMapping("/experimento")
	public ModelAndView experimentos(@RequestParam(value="id") String idProyecto, @RequestParam(value="estado") String estado, HttpServletRequest request) {
		System.out.println("ENTRA");
		/*Retorna los experimentos*/
		ModelAndView mv = new ModelAndView("home_share");
		
		fillExperimentoLienzo(idProyecto, estado, mv, request);
		fillComentarios(idProyecto, mv, request);
		mv.addObject("idProyecto", idProyecto);
		return mv;
	}
	
	@RequestMapping("/experimento/detalle/{idProyecto}")
	public ModelAndView share(@PathVariable String idProyecto, @RequestParam(value="idHipotesis", required = false) String idHipotesis, 
							  @RequestParam(value="idExperimento", required = false) String idExperimento ,HttpServletRequest request) throws JsonProcessingException {
		ModelAndView mv = new ModelAndView("resumen_share");
		Experimento experimento = experimentoService.getDetalleExperimento(idExperimento);
		fillComentarioDetalle(idExperimento, mv, request);
		String suposicion = experimento.getHipotesis();
		String descripcion = experimento.getDescripcion();
		String estado = "";

		String tipoPrueba = experimento.getTipoPrueba();
		String metricas = "";
		int i=0;
		for(Metrica me : experimento.getMetricas()) {
			
			metricas = metricas.concat("<li nombre='"+me.getNombreCorto()+"' codigo='"+me.getId()+"'>");
			metricas = metricas.concat("<h2>"+i+++"</h2>");
			metricas = metricas.concat("<h3>"+me.getNombreCorto()+"</h3>");
			metricas = metricas.concat("<p>L"+me.getId()+"</p>");
			metricas = metricas.concat("</li>");

		}
		
		
		String condiciones = "";
		i=0;
		for(Condicion co : experimento.getCondiciones()) {
			condiciones = condiciones.concat("<li nombre='"+co.getTxtCondicion()+"' codigo='"+co.getId()+"'>");
			condiciones = condiciones.concat("<h2>"+i+++"</h2>");
			condiciones = condiciones.concat("<h3>"+co.getTxtCondicion()+"</h3>");
			condiciones = condiciones.concat("<p>"+co.getDescripcion()+"</p>");
			condiciones = condiciones.concat("</li>");

		}
		
		ObjectMapper objectMapper = new ObjectMapper();
		

		
		mv.addObject("suposicion", suposicion);
		mv.addObject("descripcion", descripcion);
		mv.addObject("tipoPrueba", tipoPrueba);
		mv.addObject("metricasObject", objectMapper.writeValueAsString(experimento.getMetricas()));
		mv.addObject("metricas", metricas);
		mv.addObject("condiciones", condiciones);
		mv.addObject("mediciones", objectMapper.writeValueAsString(experimento.getFechas()));
	
		
//		mv.addObject("mediciones", dataJson);
		mv.addObject("idProyecto", idProyecto);
		mv.addObject("idExperimento", idExperimento);
		return mv;
	}
	
	
	@RequestMapping("/comentario/detalle/estado")
	@ResponseStatus(value = HttpStatus.OK)
	public void updateEstadoComentarioExperimento(@RequestParam(value="idComentario") String idComentario, @RequestParam(value="estado") String estado, HttpServletRequest request) {
		comentarioService.updateEstadoComentarioExperimento(idComentario, estado);

	}
	
	private void fillComentarioDetalle(String idExperimento, ModelAndView mv, HttpServletRequest request) {
		// TODO Auto-generated method stub
		String comentariosHtml = "";
		List<Comentario> comentarios = comentarioService.getComentariosByExperimento(idExperimento);
		String liGenerales ="";
		
		for(Comentario co : comentarios) {
			liGenerales = liGenerales.concat("<li class='item'>");
			liGenerales = liGenerales.concat("<span>");
			liGenerales = liGenerales.concat(co.getComentario());
			liGenerales = liGenerales.concat("</span>");
			liGenerales = liGenerales.concat("</li>");
		}
		comentariosHtml = comentariosHtml.concat(liGenerales);
		mv.addObject("comentariosHipotesisHtml", comentariosHtml);
	}

	@PostMapping("/saveComentarioExperimento")
	@ResponseStatus(value = HttpStatus.OK)
	public void saveComentarioExperimento(@RequestBody Comentario comentario, HttpServletRequest request) {
		comentarioService.saveComentarioExperimento(comentario);
		
	}
	
	
	private void fillHipotesisLienzo(String id, ModelAndView mv, HttpServletRequest request) {
		List<ProyectoArea> listProyectoArea = lienzoService.getLienzoByProyecto(id);
		String tablaAreas = "";
		for(ProyectoArea pA : listProyectoArea) {
			if(pA.getIdArea() == Constantes.ID_SOCIOS_CLAVE) {
				String header = "<h4>Socios clave</h4>";
				String sociosClave = "";
				sociosClave = hipotesisToHtml(pA.getHipotesis(), id, header, request);
//				sociosClave = sociosClave.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Socios clave</td>");
				tablaAreas = tablaAreas.concat("</tr>");
					
				mv.addObject(SOCIOS_CLAVE, sociosClave);
				
			}else if (pA.getIdArea() == Constantes.ID_ACTIVIDADES_CLAVE){
				String header = "<h4>Actividades clave</h4>";
				String actividadesClave = "";
				actividadesClave = (hipotesisToHtml(pA.getHipotesis(), id, header, request));
//				actividadesClave = actividadesClave.concat("</div>");
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Actividades clave</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(ACTIVIDADES_CLAVE, actividadesClave);
				
			}else if (pA.getIdArea() == Constantes.ID_RECURSOS_CLAVE){
				String header = "<h4>Recursos clave</h4>";
				String recursosClave = "";
				recursosClave = (hipotesisToHtml(pA.getHipotesis(), id, header, request));
//				recursosClave = recursosClave.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Recursos clave</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(RECURSOS_CLAVE, recursosClave);
				
			}else if (pA.getIdArea() == Constantes.ID_PROPUESTA_VALOR){
				String header = "<h4>Propuesta de valor</h4>";
				String propuestaValor = "";
				propuestaValor = (hipotesisToHtml(pA.getHipotesis(), id, header, request));
//				propuestaValor = propuestaValor.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Propuesta de valor</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(PROPUESTA_VALOR, propuestaValor);
				
			}else if (pA.getIdArea() == Constantes.ID_RELACION_CLIENTES){
				String header = "<h4>Relación con clientes</h4>";
				String relacionClientes = "";
				relacionClientes = (hipotesisToHtml(pA.getHipotesis(), id, header, request));
//				relacionClientes = relacionClientes.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Relación con clientes</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(RELACION_CLIENTES, relacionClientes);
				
			}else if (pA.getIdArea() == Constantes.ID_CANALES){
				
				String header = "<h4>Canales</h4>";
				String canales = "";
				canales = canales.concat(hipotesisToHtml(pA.getHipotesis(), id, header, request));
//				canales = canales.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Canales</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(CANALES, canales);
				
			}else if (pA.getIdArea() == Constantes.ID_SEGMENTOS_CLIENTES){
				String header = "<h4>Segmentos de clientes</i></h4>";
				String segmentosClientes = "";
				segmentosClientes = (hipotesisToHtml(pA.getHipotesis(), id, header, request));
//				segmentosClientes = segmentosClientes.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Segmentos de clientes</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(SEGMENTOS_CLIENTES, segmentosClientes);
				
			}else if (pA.getIdArea() == Constantes.ID_ESTRUCTURA_COSTOS){
				String header = "<h4>Estructura de costos</h4>";
				String estructuraCostos = "";
				estructuraCostos = (hipotesisToHtml(pA.getHipotesis(), id, header, request));
//				estructuraCostos = estructuraCostos.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Estructura de costos</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(ESTRUCTURA_COSTOS, estructuraCostos);
				
			}else if (pA.getIdArea() == Constantes.ID_FUENTES_INGRESO){
				String header = "<h4>Fuentes de ingreso</h4>";
				String fuentesIngreso = "";
				fuentesIngreso = (hipotesisToHtml(pA.getHipotesis(), id, header, request));
//				fuentesIngreso = fuentesIngreso.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Fuentes de ingreso</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(FUENTES_INGRESO, fuentesIngreso);
			}
		}
		mv.addObject("tablaAreas", tablaAreas);
		
	} 
	
	private String hipotesisToHtml(List<Hipotesis> listHipotesis, String idProyecto, String html, HttpServletRequest request) {
		html = html.concat("<ul>");
		for(Hipotesis h : listHipotesis) {
			String descripcion = (h.getDescripcion() == null) ? "" : h.getDescripcion(); 
			html = html.concat("<li><a> <h2>"+h.getSuposicion()+"</h2></a></li>");
			
			
		}
		html = html.concat("</ul>");
		return html;
	}
	
	private void fillComentarios(String idProyecto, ModelAndView mv, HttpServletRequest request) {
		String comentariosHtml = "";
		List<Comentario> comentarios = comentarioService.getComentariosByProyecto(idProyecto);
		String liGenerales ="";
		
		for(Comentario co : comentarios) {
			liGenerales = liGenerales.concat("<li idProyecto='"+co.getIdProyecto()+"' idComentario='"+co.getId()+"' class='item'>");
			liGenerales = liGenerales.concat("<span>");
			liGenerales = liGenerales.concat(co.getComentario());
			liGenerales = liGenerales.concat("</span>");
			liGenerales = liGenerales.concat("</li>");
		}
		
		List<Comentario> comentariosDetalle = comentarioService.getComentariosDetalleByProyecto(idProyecto);
		
		/*
		 Detalle
		 Agenda
		 Resumen
		 */
		
		for(Comentario co : comentariosDetalle) {
			liGenerales = liGenerales.concat("<li idProyecto='"+co.getIdProyecto()+"' idComentario='"+co.getId()+"' class='item'>");
			
			liGenerales = liGenerales.concat("<span><strong>HIPOTESIS:<br></strong>");
			liGenerales = liGenerales.concat(co.getSuposicion());
			liGenerales = liGenerales.concat("</span>");
			liGenerales = liGenerales.concat("<a href='"+request.getContextPath()+"/share/experimento/detalle/"+idProyecto+"?idExperimento="+co.getIdExperimento()+"'>Ver comentarios</a>");
			
			liGenerales = liGenerales.concat("</li>");
		}
		
		
		comentariosHtml = comentariosHtml.concat(liGenerales);
		mv.addObject("comentariosHtml", comentariosHtml);
		
	}
	
	private void fillExperimentoLienzo(String id, String estado, ModelAndView mv, HttpServletRequest request) {
		List<ProyectoArea> listProyectoArea = experimentoService.getExperimentoByProyectoAndEstado(id, estado);
		String tablaAreas = "";
		for(ProyectoArea pA : listProyectoArea) {
			if(pA.getIdArea() == Constantes.ID_SOCIOS_CLAVE) {
				String header = "<h4>Socios clave <i onClick='sociosClave()' class='fas fa-eye' style='float:right';></i></h4>";
				String sociosClave = "";
				sociosClave = experimentoToHtml(pA.getExperimentos(), id, header, request);
//				sociosClave = sociosClave.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Socios clave</td>");
				tablaAreas = tablaAreas.concat("</tr>");
					
				mv.addObject(SOCIOS_CLAVE, sociosClave);
				
			}else if (pA.getIdArea() == Constantes.ID_ACTIVIDADES_CLAVE){
				String header = "<h4>Actividades clave <i onClick='actividadesClave()' class='fas fa-eye' style='float:right';></i></h4>";
				String actividadesClave = "";
				actividadesClave = (experimentoToHtml(pA.getExperimentos(), id, header, request));
//				actividadesClave = actividadesClave.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Actividades clave</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(ACTIVIDADES_CLAVE, actividadesClave);
				
			}else if (pA.getIdArea() == Constantes.ID_RECURSOS_CLAVE){
				String header = "<h4>Recursos clave <i onClick='recursosClave()' class='fas fa-eye' style='float:right';></i></h4>";
				String recursosClave = "";
				recursosClave = (experimentoToHtml(pA.getExperimentos(), id, header, request));
//				recursosClave = recursosClave.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Recursos clave</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(RECURSOS_CLAVE, recursosClave);
				
			}else if (pA.getIdArea() == Constantes.ID_PROPUESTA_VALOR){
				String header = "<h4>Propuesta de valor <i onClick='propuestaValor()' class='fas fa-eye' style='float:right';></i></h4>";
				String propuestaValor = "";
				propuestaValor = (experimentoToHtml(pA.getExperimentos(), id, header, request));
//				propuestaValor = propuestaValor.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Propuesta de valor</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(PROPUESTA_VALOR, propuestaValor);
				
			}else if (pA.getIdArea() == Constantes.ID_RELACION_CLIENTES){
				String header = "<h4>Relación con clientes <i onClick='reclacionClientes()' class='fas fa-eye' style='float:right';></i></h4>";
				String relacionClientes = "";
				relacionClientes = (experimentoToHtml(pA.getExperimentos(), id, header, request));
//				relacionClientes = relacionClientes.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Relación con clientes</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(RELACION_CLIENTES, relacionClientes);
				
			}else if (pA.getIdArea() == Constantes.ID_CANALES){
				
				String header = "<h4>Canales <i onClick='canales()' class='fas fa-eye' style='float:right';></i></h4>";
				String canales = "";
				canales = canales.concat(experimentoToHtml(pA.getExperimentos(), id, header, request));
//				canales = canales.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Canales</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(CANALES, canales);
				
			}else if (pA.getIdArea() == Constantes.ID_SEGMENTOS_CLIENTES){
				String header = "<h4>Segmentos de clientes <i onClick='segmentosClientes()' class='fas fa-eye' style='float:right';></i></h4>";
				String segmentosClientes = "";
				segmentosClientes = (experimentoToHtml(pA.getExperimentos(), id, header, request));
//				segmentosClientes = segmentosClientes.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Segmentos de clientes</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(SEGMENTOS_CLIENTES, segmentosClientes);
				
			}else if (pA.getIdArea() == Constantes.ID_ESTRUCTURA_COSTOS){
				String header = "<h4>Estructura de costos <i onClick='estructuraCostos()' class='fas fa-eye' style='float:right';></i></h4>";
				String estructuraCostos = "";
				estructuraCostos = (experimentoToHtml(pA.getExperimentos(), id, header, request));
//				estructuraCostos = estructuraCostos.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Estructura de costos</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(ESTRUCTURA_COSTOS, estructuraCostos);
				
			}else if (pA.getIdArea() == Constantes.ID_FUENTES_INGRESO){
				String header = "<h4>Fuentes de ingreso <i onClick='fuentesIngreso()' class='fas fa-eye' style='float:right';></i></h4>";
				String fuentesIngreso = "";
				fuentesIngreso = (experimentoToHtml(pA.getExperimentos(), id, header, request));
//				fuentesIngreso = fuentesIngreso.concat("</div>");
				
				tablaAreas = tablaAreas.concat("<tr value='"+pA.getId()+"'>");
				tablaAreas = tablaAreas.concat("<td>"+pA.getId()+"</td>");
				tablaAreas = tablaAreas.concat("<td>Fuentes de ingreso</td>");
				tablaAreas = tablaAreas.concat("</tr>");
				
				mv.addObject(FUENTES_INGRESO, fuentesIngreso);
			}
		}
		mv.addObject("tablaAreas", tablaAreas);
		
		
		
	}
	
	private String experimentoToHtml(List<Experimento> listHipotesis, String idProyecto, String html,  HttpServletRequest request) {
		html = html.concat("<ul>");
		for(Experimento h : listHipotesis) {
			html = html.concat("<li> <a href='"+request.getContextPath()+"/share/experimento/detalle/"+idProyecto+"?idExperimento="+h.getId()+"'><h2>"+h.getHipotesis()+"</h2></a></li>");
			
			
		}
		html = html.concat("</ul>");
		return html;
	}
}
