package com.tesis.lienzo.app.lienzanier.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tesis.lienzo.app.lienzanier.bean.Indicador;
import com.tesis.lienzo.app.lienzanier.feign.IndicadorFeignClient;
import com.tesis.lienzo.app.lienzanier.service.IndicadorService;

@Service
public class IndicadorServiceImpl implements IndicadorService{

	@Autowired
	IndicadorFeignClient indicadorFeign;

	@Override
	public List<Indicador> listar(String idProyecto) {
		// TODO Auto-generated method stub
		return indicadorFeign.listar(idProyecto);
	}

	@Override
	public void save(Indicador indicador) {
		// TODO Auto-generated method stub
		indicadorFeign.save(indicador);
	}

	@Override
	public Indicador getIndicador(String idIndicador) {
		// TODO Auto-generated method stub
		return indicadorFeign.getIndicador(idIndicador);
	}

	@Override
	public void saveMediciones(Indicador indicador) {
		// TODO Auto-generated method stub
		indicadorFeign.saveMediciones(indicador);
	}
	
	
	
	
}
