package com.tesis.lienzo.app.lienzanier.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.tesis.lienzo.app.lienzanier.bean.Metrica;

@FeignClient(name = "metrica", url = "${ws.lienzo}/metrica")
public interface MetricaFeignClient {
	
	@GetMapping("/getMetricaByProyecto")
	List<Metrica> getByProyecto(@RequestParam(value="id") String idProyecto);

	@PostMapping("/update")
	void update(@RequestBody Metrica metrica);

	@GetMapping("/delete")
	void delete(@RequestParam(value="id") String id);

	@PostMapping("/save")
	int save(@RequestBody Metrica metrica);

}
