package com.tesis.lienzo.app.lienzanier.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.tesis.lienzo.app.lienzanier.bean.Usuario;

@FeignClient(name = "usuario", url = "${ws.lienzo}/usuario")
public interface UsuarioFeignClient {
	
	@RequestMapping(method = RequestMethod.POST, value = "/login")
	Usuario login (Usuario usuario);
	
	@RequestMapping(method = RequestMethod.GET, value = "/getByEmail")
	List<Usuario>  getByEmail(@RequestParam(value="email") String email, @RequestParam(value="current") String current);

	@RequestMapping(method = RequestMethod.POST, value = "/loginG")
	Usuario loginGmail(Usuario usuario);

	@RequestMapping(method = RequestMethod.GET, value = "/validateUsuarioRegistrado")
	Boolean validateUsuarioRegistrado(@RequestParam(value="usuario") String usuario);

	@RequestMapping(method = RequestMethod.GET, value = "/validateEmailRegistrado")
	Boolean validateEmailRegistrado(@RequestParam(value="email") String usuario);

	@RequestMapping(method = RequestMethod.POST, value = "/save")
	void save(@RequestBody Usuario usuario);
	
	@RequestMapping(method = RequestMethod.POST, value = "/update")
	void update(Usuario usuario);
	
	@RequestMapping(method = RequestMethod.GET, value = "/getByIdProyecto")
	List<Usuario> getByIdProyecto(@RequestParam(value="idProyecto") String idProyecto);

	@RequestMapping(method = RequestMethod.POST, value = "/registerShare")
	Usuario registerShare(Usuario usuario);

	
}
