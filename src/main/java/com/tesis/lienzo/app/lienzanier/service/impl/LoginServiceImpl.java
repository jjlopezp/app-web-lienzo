package com.tesis.lienzo.app.lienzanier.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tesis.lienzo.app.lienzanier.bean.Usuario;
import com.tesis.lienzo.app.lienzanier.feign.UsuarioFeignClient;
import com.tesis.lienzo.app.lienzanier.feign.impl.UsuarioFeignClientImpl;
import com.tesis.lienzo.app.lienzanier.service.LoginService;

@Service
public class LoginServiceImpl implements LoginService{

	@Autowired
	UsuarioFeignClient usuarioFeignClient;
	
	@Override
	public Usuario login(Usuario usuario) {
		return usuarioFeignClient.login(usuario);
	}

	@Override
	public Usuario loginGmail(Usuario usuario) {
		return usuarioFeignClient.loginGmail(usuario);
	}

	@Override
	public Usuario registerShare(Usuario usuario) {
		return usuarioFeignClient.registerShare(usuario);
	}

}
