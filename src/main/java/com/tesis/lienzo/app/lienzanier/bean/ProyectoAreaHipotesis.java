package com.tesis.lienzo.app.lienzanier.bean;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProyectoAreaHipotesis {

	private int id;
	private int idProyectoArea;
	private int idHipotesis;
	private List<Hipotesis> hipotesis;
}
