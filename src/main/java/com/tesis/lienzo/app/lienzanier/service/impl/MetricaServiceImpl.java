package com.tesis.lienzo.app.lienzanier.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tesis.lienzo.app.lienzanier.bean.Metrica;
import com.tesis.lienzo.app.lienzanier.feign.MetricaFeignClient;
import com.tesis.lienzo.app.lienzanier.service.MetricaService;

@Service
public class MetricaServiceImpl implements MetricaService{
	
	@Autowired
	MetricaFeignClient metricaFeignClient;

	@Override
	public List<Metrica> getByProyecto(String idProyecto) {
		// TODO Auto-generated method stub
		
		return metricaFeignClient.getByProyecto(idProyecto);
	}

	@Override
	public void update(Metrica metrica) {
		// TODO Auto-generated method stub
		metricaFeignClient.update(metrica);
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub
		metricaFeignClient.delete(id);
	}

	@Override
	public void save(Metrica metrica) {
		// TODO Auto-generated method stub
		metricaFeignClient.save(metrica);
	}

}
