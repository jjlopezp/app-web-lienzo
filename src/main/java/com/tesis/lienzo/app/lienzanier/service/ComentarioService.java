package com.tesis.lienzo.app.lienzanier.service;

import java.util.List;

import com.tesis.lienzo.app.lienzanier.bean.Comentario;



public interface ComentarioService {

	List<Comentario> getComentariosByProyecto(String idProyecto);

	void updateEstadoComentario(String idComentario, String estado);

	void saveComentarioProyecto(Comentario comentario);

	void saveComentarioExperimento(Comentario comentario);

	List<Comentario> getComentariosByExperimento(String idExperimento);

	List<Comentario> getComentariosDetalleByProyecto(String idProyecto);

	void updateEstadoComentarioExperimento(String idComentario, String estado);


}
