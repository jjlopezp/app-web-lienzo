package com.tesis.lienzo.app.lienzanier.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.tesis.lienzo.app.lienzanier.bean.ProyectoArea;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HipotesisDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private int idExperimento;
	private String suposicion;
	private String publico;
	private int estado;
	private String descripcion;
	private int duracion;
	private int idProyecto;
	private String fechaRegistro;
	private List<ProyectoArea> proyectoAreas;
	private int idHipotesisPadre;
}
