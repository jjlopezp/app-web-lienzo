package com.tesis.lienzo.app.lienzanier.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tesis.lienzo.app.lienzanier.bean.Condicion;
import com.tesis.lienzo.app.lienzanier.bean.Dia;
import com.tesis.lienzo.app.lienzanier.bean.DiaIndicador;
import com.tesis.lienzo.app.lienzanier.bean.Experimento;
import com.tesis.lienzo.app.lienzanier.bean.Fecha;
import com.tesis.lienzo.app.lienzanier.bean.Indicador;
import com.tesis.lienzo.app.lienzanier.bean.MesIndicador;
import com.tesis.lienzo.app.lienzanier.bean.Metrica;
import com.tesis.lienzo.app.lienzanier.bean.PuntoMedicion;
import com.tesis.lienzo.app.lienzanier.bean.PuntoMedicionIndicador;
import com.tesis.lienzo.app.lienzanier.bean.Usuario;
import com.tesis.lienzo.app.lienzanier.service.IndicadorService;
import com.tesis.lienzo.app.lienzanier.service.MetricaService;
import com.tesis.lienzo.app.lienzanier.util.Constantes;

import lombok.Getter;

@Controller
@RequestMapping("/indicador")
public class IndicadorController {

	
	@Autowired
	IndicadorService indicadorService;
	
	@Autowired
	MetricaService metricaService;
	
	
	
	@RequestMapping("/listar")
	public ModelAndView listar(@RequestParam(value="id") String idProyecto, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView("indicadores");
		
		List<Indicador> indicadores = indicadorService.listar(idProyecto);
		
		String indicadoresHtml = "";
		for(Indicador ind : indicadores){
			indicadoresHtml = indicadoresHtml.concat("<li class='sgLi'>");
			indicadoresHtml = indicadoresHtml.concat("<a href='/indicador/agenda?id="+idProyecto+"&idIndicador="+ind.getId()+"'>");
			indicadoresHtml = indicadoresHtml.concat("<div class='box'>");
			indicadoresHtml = indicadoresHtml.concat("<h3>"+ind.getNombre()+"</h3>");
			indicadoresHtml = indicadoresHtml.concat("<p class='info-indicador'>");
			indicadoresHtml = indicadoresHtml.concat(ind.getDescripcion());
			indicadoresHtml = indicadoresHtml.concat("</p>");
			
			String indicadorConvertir = convertMetricas(ind.getTxt_indicador(), ind.getMetricas());
			
			indicadoresHtml = indicadoresHtml.concat("<p class='formula'>"+indicadorConvertir+"</p>");
			indicadoresHtml = indicadoresHtml.concat("<div>");
			indicadoresHtml = indicadoresHtml.concat("<i class='fas fa-trash fa-2x'></i>");
			indicadoresHtml = indicadoresHtml.concat("</div>");
			indicadoresHtml = indicadoresHtml.concat("</div>");
			indicadoresHtml = indicadoresHtml.concat("</a>");
			indicadoresHtml = indicadoresHtml.concat("</li>");
		
		}
		
		List<Metrica> metricas = metricaService.getByProyecto(idProyecto);
		String metricasHtml = "";
		for(Metrica me : metricas) {
			metricasHtml = metricasHtml.concat("<label>");			
			metricasHtml = metricasHtml.concat(" <input value='"+me.getId()+"' texto='"+me.getNombreCorto()+"' type='checkbox' class='option-input checkbox'/>");
			metricasHtml = metricasHtml.concat(me.getNombreCorto()+"&emsp;&emsp;&emsp;");
			metricasHtml = metricasHtml.concat("L"+me.getId());
			metricasHtml = metricasHtml.concat("</label>");

			
		}
		

		mv.addObject("indicadoresHtml", indicadoresHtml);
		mv.addObject("metricasHtml", metricasHtml);
		mv.addObject("idProyecto", idProyecto);
			return mv;
		
	}
	
	

	private String convertMetricas(String txt_indicador, List<Metrica> metricas) {
		// TODO Auto-generated method stub
		String txt = txt_indicador;
		for(Metrica me : metricas) {
			txt_indicador = txt_indicador.replace("L"+me.getId(), me.getNombreCorto());
		}
		return txt_indicador;
	}



	@PostMapping("/save")
	public @ResponseBody Map<String,Object> save(@RequestBody Indicador indicador, HttpServletRequest request){
		Map<String, Object> parametros = new HashMap<>();
		indicadorService.save(indicador);
		return parametros;
	}
	
	@PostMapping("/saveMediciones")
	public @ResponseBody Map<String,Object> saveMediciones(@RequestBody Indicador indicador, HttpServletRequest request){
		Map<String, Object> parametros = new HashMap<>();
		indicadorService.saveMediciones(indicador);
		return parametros;
	}
	
	@RequestMapping("/agenda")
	public ModelAndView agendaExperimento(@RequestParam(value="id") String idProyecto, @RequestParam(value="idIndicador", required = false) String idIndicador, HttpServletRequest request) throws ScriptException, JsonProcessingException {
		ModelAndView mv = new ModelAndView("agenda_indicador");
		Indicador indicador = indicadorService.getIndicador(idIndicador);
		String nombre = indicador.getNombre();
		String descripcion = indicador.getDescripcion();
		
		
		String metricas = "";
		int i=0;
		String formula = indicador.getTxt_indicador();
		String metricasTotal ="";
		
		
		
		
		
//		i=0;
//		for(Condicion co : experimento.getCondiciones()) {
//			condiciones = condiciones.concat("<li nombre='"+co.getTxtCondicion()+"' codigo='"+co.getId()+"'>");
//			condiciones = condiciones.concat("<h2>"+i+++"</h2>");
//			condiciones = condiciones.concat("<p>"+co.getDescripcion()+"</p>");
//			String condicionHTML = co.getTxtCondicion().replace("<", "&#60;");
//			condiciones = condiciones.concat("<h3>"+condicionHTML+"</h3>");		
//			condiciones = condiciones.concat("</li>");
//
//		}
		
//		fillComentarioDetalle(idExperimento, mv, request);
		
		String html = "";
		for(MesIndicador fe: indicador.getMeses()) {
			html = html.concat("<li>");
			html = html.concat("<div mes='"+fe.getMes()+"' class='timeline-month'>");
			html = html.concat(Constantes.ARREGLO_MESES[fe.getMes()]+", 2020");
			html = html.concat("<span>3 Entries</span>");
			html = html.concat("</div>");
			html = html.concat("<ul>");
			
			
			for(DiaIndicador dia: fe.getDias()) {
				html = html.concat("<li class='timeline-section li-dia'>");
				html = html.concat("<div semana='"+dia.getSemana()+"' dia='"+dia.getDia()+"' class='timeline-date'>");
				html = html.concat(dia.getDia() +", "+Constantes.ARREGLO_DIAS_SEMANAS[dia.getSemana()]);
				html = html.concat("</div>");
				html = html.concat("<ul class='row'>");
				
				for(PuntoMedicionIndicador pm: dia.getPuntos()) {
					html = html.concat("<li id='boxMetrica'>");
					html = html.concat("<div class='col-sm-4'>");
					html = html.concat("<div class='timeline-box'>");
					html = html.concat("<div class='box-tittle'>");
					html = html.concat("<i class='fa fa-asterisk text-success' aria-hidden='true'></i> Métrica");
					html = html.concat("</div>");

					html = html.concat("<div class='box-content'>");
					html = html.concat("<div class='box-item'><strong>"+pm.getMetrica()+" ("+pm.getIdentificador()+")</strong></div>");
					html = html.concat("<div class='box-item'><input idMetrica='"+pm.getIdMetrica()+"' indentificador='"+pm.getIdentificador()+"' value='"+pm.getValor()+"' class='' onkeypress='validarInput(event)'></div>");
					html = html.concat("</div>");

					html = html.concat("<div class='box-footer'>- </div>");
					html = html.concat("</div>");
					html = html.concat("</div>");
					html = html.concat("</li>");
					
					sumarParcial(indicador.getMetricas(), pm);
				}
				

				
				html = html.concat("</ul>");
				html = html.concat("</li>");
			}


			html = html.concat("</ul>");
			html = html.concat("</li>");
		}
		
		String resultado ="";
		String expresion = indicador.getTxt_indicador();
		for(Metrica me : indicador.getMetricas()) {
			formula = formula.replace("L"+me.getId(), me.getNombreCorto());
			
			metricas = metricas.concat("<li nombre='"+me.getNombreCorto()+"' codigo='"+me.getId()+"'>");
			metricas = metricas.concat("<h2>"+i+++"</h2>");
			metricas = metricas.concat("<h3>"+me.getNombreCorto()+"</h3>");
			metricas = metricas.concat("<p>L"+me.getId()+"</p>");
			metricas = metricas.concat("</li>");
			
			metricasTotal = metricasTotal.concat("<li nombre='"+me.getNombreCorto()+"' codigo='"+me.getId()+"'>");
			metricasTotal = metricasTotal.concat("<h3>"+me.getNombreCorto()+"</h3>");
			metricasTotal = metricasTotal.concat("<p id='sumaMetrica'>"+me.getSumaParcial()+"</p>");
			metricasTotal = metricasTotal.concat("</li>");

			expresion = expresion.replace("L"+me.getId(), String.valueOf(me.getSumaParcial()));
			
		}
		
		
		String formulaHtml = "<div class='formula' id='formulaExp' formulaExpresion='"+indicador.getTxt_indicador()+"'>";
		formulaHtml = formulaHtml.concat(formula);
		formulaHtml = formulaHtml.concat("</div>");
		
		ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
		ScriptEngine scriptEngine = scriptEngineManager.getEngineByName("JavaScript");
		
		Object ob = scriptEngine.eval(expresion);
		resultado = ob.toString();

		ObjectMapper objectMapper = new ObjectMapper();
		
		mv.addObject("nombre", nombre);
		mv.addObject("descripcion", descripcion);		
		mv.addObject("metricasTotal", metricasTotal);
		mv.addObject("metricas", metricas);
		mv.addObject("formula", formulaHtml);
		mv.addObject("resultado", resultado);
		mv.addObject("mediciones", html);
		mv.addObject("medicionesResumen", objectMapper.writeValueAsString(indicador.getMeses()));
		mv.addObject("metricasObject", objectMapper.writeValueAsString(indicador.getMetricas()));
//		mv.addObject("listCondiciones", experimento.getCondiciones());
//		mv.addObject("listMetricas", experimento.getMetricas());
		mv.addObject("idProyecto", idProyecto);
		mv.addObject("idIndicador", idIndicador);
		return mv;
	}



	private void sumarParcial(List<Metrica> metricas, PuntoMedicionIndicador pm) {
		// TODO Auto-generated method stub
		for(Metrica me : metricas) {
			if(me.getId() == pm.getIdMetrica()) {
				me.setSumaParcial(  me.getSumaParcial() + pm.getValor() );
			}
		}
	}



	
}
