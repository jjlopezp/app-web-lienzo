package com.tesis.lienzo.app.lienzanier.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsuarioProyecto {

	private Integer id;
	private Integer id_proyecto;
	private Integer id_usuario;
}
