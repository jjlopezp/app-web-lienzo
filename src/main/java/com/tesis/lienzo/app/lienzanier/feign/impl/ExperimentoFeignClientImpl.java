package com.tesis.lienzo.app.lienzanier.feign.impl;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.tesis.lienzo.app.lienzanier.bean.Experimento;
import com.tesis.lienzo.app.lienzanier.bean.ProyectoArea;
import com.tesis.lienzo.app.lienzanier.bean.PruebaHipotesis;
import com.tesis.lienzo.app.lienzanier.dto.ExperimentoDto;
import com.tesis.lienzo.app.lienzanier.feign.ExperimentoFeignClient;

@Component
public class ExperimentoFeignClientImpl implements ExperimentoFeignClient{

	@Override
	public List<PruebaHipotesis> getByProyecto() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Integer> save(ExperimentoDto experimentDto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Experimento> getExperimentosByProjecto(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<String> updateEstado(String idExperimento, String estado) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Experimento getDetalleExperimento(String idExperimento) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ProyectoArea> getExperimentoByProyectoAndEstado(String id, String estado) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveMediciones(Experimento experimento) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateMediciones(Experimento experimento) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void finalizarExperimento(String idHipotesis, String idProyecto, String idExperimento) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void guardarCambiosExperimento(ExperimentoDto experimentDto) {
		// TODO Auto-generated method stub
		
	}

}
