package com.tesis.lienzo.app.lienzanier.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tesis.lienzo.app.lienzanier.bean.Comentario;
import com.tesis.lienzo.app.lienzanier.bean.Condicion;
import com.tesis.lienzo.app.lienzanier.bean.Experimento;
import com.tesis.lienzo.app.lienzanier.bean.Hipotesis;
import com.tesis.lienzo.app.lienzanier.bean.Metrica;
import com.tesis.lienzo.app.lienzanier.bean.ProyectoArea;
import com.tesis.lienzo.app.lienzanier.bean.Usuario;
import com.tesis.lienzo.app.lienzanier.service.ComentarioService;
import com.tesis.lienzo.app.lienzanier.service.ExperimentoService;
import com.tesis.lienzo.app.lienzanier.service.LienzoService;
import com.tesis.lienzo.app.lienzanier.util.Constantes;

@Controller
public class WebController {
	
	
	
	@Autowired
	LienzoService lienzoService;
	
	@Autowired
	ComentarioService comentarioService;
	
	@Autowired
	ExperimentoService experimentoService;
	
	@RequestMapping("/")
	public String login2() {
		return "login";
	}

	@RequestMapping("/inicia")
	public String inicia_sesion() {
		return "inicia_sesion";
	}
	
	@RequestMapping("/hola")
	@ResponseBody
	public String login23() {
		return "WEB liena";
	}

	@RequestMapping("/login")
	public String login() {
		return "login";
	}
	
	@RequestMapping("/miperfil")
	public ModelAndView miperfil(@RequestParam(value="id") String idProyecto, HttpServletRequest request) {
		ModelAndView mv = new ModelAndView("edit_usuario");
		
		mv.addObject("idProyecto", idProyecto);
		return mv;
	}
	
	@RequestMapping("/registro/usuario")
	public String registroUsuario() {
		return "registro_usuario";
	}
	
	@RequestMapping("/proyectos/misproyectos")
	public String misproyectos(HttpServletRequest request) {
		
		Usuario usuario = ((Usuario) request.getSession().getAttribute("USUARIOSESION"));
		if(usuario==null) {
			return "redirect:/login";
		}else {
			return "redirect:/proyecto/listar";
		}
		
	}
	
	@RequestMapping("home")
	public String home(@RequestParam(value="id") String id, HttpServletRequest request) {
		Usuario usuario = ((Usuario) request.getSession().getAttribute("USUARIOSESION"));
		if(usuario==null) {
			return "redirect:/login";
		}else {
			return "redirect:/lienzo/home?id="+id;
		}
	}
	
	@RequestMapping("home/hipotesis")
	public String editHipotesis(@RequestParam(value="id") String id, @RequestParam(value="idPro") String idProyecto, HttpServletRequest request) {
		Usuario usuario = ((Usuario) request.getSession().getAttribute("USUARIOSESION"));
		if(usuario==null) {
			return "redirect:/login";
		}else {
			return "redirect:/lienzo/home/hipotesis?id="+id+"&idPro="+idProyecto;
		}
	}
	
	@RequestMapping("metrica")
	public String metrica(@RequestParam(value="id") String idProyecto, HttpServletRequest request) {
		Usuario usuario = ((Usuario) request.getSession().getAttribute("USUARIOSESION"));
		if(usuario==null) {
			return "redirect:/login";
		}else {
			return "redirect:/metrica/listar?id="+idProyecto;
		}
	}
	
	@RequestMapping("/experimento")
	public String crearExperimento(@RequestParam(value="id") String idProyecto, @RequestParam(value="idHipotesis") String idHipotesis, HttpServletRequest request) {
		return "redirect:/experimento/crear?id="+idProyecto+"&idHipotesis="+idHipotesis;
	}
	
	@RequestMapping("/historial")
	public String historialExperimento(@RequestParam(value="id") String idProyecto, HttpServletRequest request) {
		return "redirect:/experimento/historial?id="+idProyecto;
	}
	
	@RequestMapping("/detalleExperimento")
	public String historialExperimento(@RequestParam(value="id") String idProyecto, @RequestParam(value="idExperimento") String idExperimento, HttpServletRequest request) {
		return "redirect:/experimento/detalle?id="+idProyecto+"&idExperimento="+idExperimento;
	}
	
	@RequestMapping("home/experimento")
	public String homeExperimento(@RequestParam(value="id") String id, @RequestParam(value="estado") String estado, HttpServletRequest request) {
		Usuario usuario = ((Usuario) request.getSession().getAttribute("USUARIOSESION"));
		if(usuario==null) {
			return "redirect:/login";
		}else {
			return "redirect:/lienzo/experimento?id="+id+"&estado="+estado;
		}
	}
	
	@RequestMapping("/lienzo")
	public String blogLienzo() {
		return "blog_lienzo";
	}
	
	@RequestMapping("/leanStartup")
	public String leanStartup() {
		return "blog_startup";
	}
	
	@RequestMapping("/kpis")
	public String kpis() {
		return "blog_kpis";
	}
	
	@RequestMapping("/tutorial")
	public String tutorial() {
		return "como_usar";
	}
	
	
	
	@RequestMapping("/finalizar")
	public String finalizarExperimento(@RequestParam(value="id") String idProyecto, @RequestParam(value="idExperimento") String idExperimento, HttpServletRequest request) {
		return "redirect:/experimento/finalizar?id="+idProyecto+"&idExperimento="+idExperimento;
	}
	
	
}
